# AIRCONROLLER 

This solutions for control clearance, compressor, airaccumulator, power.

[hardware](hardware/) - altium project.    
[firmware](firmware/) - cmake gcc. (not actual generation)  
[doc](doc/) - documentation.   
[enclosure](enclosure/) - case for controller.   
[pinout](doc/pinout/)

![view](doc/photo/mainview.png)
![view](doc/photo/top.png)
![view](doc/photo/bot.png)
![view](doc/pinout/cover1.png)
![view](doc/pinout/cover2.png)