/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2019 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define PRESSURE_CONFIG_Pin GPIO_PIN_0
#define PRESSURE_CONFIG_GPIO_Port GPIOC
#define DOOROPEN_CONFIG_Pin GPIO_PIN_1
#define DOOROPEN_CONFIG_GPIO_Port GPIOC
#define DOOR_OPEN_Pin GPIO_PIN_2
#define DOOR_OPEN_GPIO_Port GPIOC
#define DOOR_OPEN_EXTI_IRQn EXTI2_IRQn
#define STOP_Pin GPIO_PIN_3
#define STOP_GPIO_Port GPIOC
#define STOP_EXTI_IRQn EXTI3_IRQn
#define ENABLE_Pin GPIO_PIN_0
#define ENABLE_GPIO_Port GPIOA
#define PRESSURE_Pin GPIO_PIN_1
#define PRESSURE_GPIO_Port GPIOA
#define PRESSURE_EXTI_IRQn EXTI1_IRQn
#define LCD_TX_Pin GPIO_PIN_2
#define LCD_TX_GPIO_Port GPIOA
#define LCD_RX_Pin GPIO_PIN_3
#define LCD_RX_GPIO_Port GPIOA
#define SENSOR_EN_Pin GPIO_PIN_4
#define SENSOR_EN_GPIO_Port GPIOA
#define OVER_CUR_SENSOR_Pin GPIO_PIN_5
#define OVER_CUR_SENSOR_GPIO_Port GPIOA
#define OVER_CUR_SENSOR_EXTI_IRQn EXTI9_5_IRQn
#define FL_IN_Pin GPIO_PIN_6
#define FL_IN_GPIO_Port GPIOA
#define SensorFR_Pin GPIO_PIN_7
#define SensorFR_GPIO_Port GPIOA
#define SensorFRC4_Pin GPIO_PIN_4
#define SensorFRC4_GPIO_Port GPIOC
#define SensorRL_Pin GPIO_PIN_5
#define SensorRL_GPIO_Port GPIOC
#define SensorRR_Pin GPIO_PIN_0
#define SensorRR_GPIO_Port GPIOB
#define ACCV_Pin GPIO_PIN_1
#define ACCV_GPIO_Port GPIOB
#define COMP_KEY_Pin GPIO_PIN_2
#define COMP_KEY_GPIO_Port GPIOB
#define FL_EX_Pin GPIO_PIN_10
#define FL_EX_GPIO_Port GPIOB
#define FR_IN_Pin GPIO_PIN_12
#define FR_IN_GPIO_Port GPIOB
#define FR_EX_Pin GPIO_PIN_13
#define FR_EX_GPIO_Port GPIOB
#define RL_IN_Pin GPIO_PIN_14
#define RL_IN_GPIO_Port GPIOB
#define RL_EX_Pin GPIO_PIN_15
#define RL_EX_GPIO_Port GPIOB
#define RR_IN_Pin GPIO_PIN_6
#define RR_IN_GPIO_Port GPIOC
#define RR_EX_Pin GPIO_PIN_7
#define RR_EX_GPIO_Port GPIOC
#define LED_Pin GPIO_PIN_9
#define LED_GPIO_Port GPIOA
#define SPEED_COMP_Pin GPIO_PIN_15
#define SPEED_COMP_GPIO_Port GPIOA
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
