#include "help.h"

#ifndef _BUFFER_H_
#define _BUFFER_H_

struct ring_buffer
{
	volatile uint8_t start;
	volatile uint8_t end;
	volatile uint8_t size;
	volatile uint8_t elem_size;
	volatile uint8_t max_pop_size;
    volatile uint8_t count;
    /*unsigned main buffer pointer*/
    uint8_t *element;
};
 
typedef struct ring_buffer buffer_type;

void buffer_init(buffer_type *name_buffer, int size, uint8_t *buffer, uint8_t element_size, int max_pop_size);
int buffer_is_full(buffer_type *buffer);
int buffer_is_empty(buffer_type *buffer);
int buffer_is_halffull(buffer_type *buffer);
int buffer_is_halfempty(buffer_type *buffer);
void buffer_pop_queue(buffer_type *buffer, uint8_t *element);
void buffer_flash_queue(buffer_type *buffer);
void buffer_push_queue(buffer_type *buffer, uint8_t *data);
void buffer_rewinds_queue(buffer_type *buffer, int count);


#endif
