#ifndef __DATA_CB_H_
#define __DATA_CB_H_
#include "pdata.h"


void cmd_valve_control(payload_t *payload);
void cmd_read_or_write_param(payload_t *payload);
void cmd_calibrate(payload_t* payload);
void cmd_do_clearance_or_manual_set(payload_t *payload);
void cmd_save_or_reset_user_data(payload_t *payload);

#endif
