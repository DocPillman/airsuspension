#include "data.h"
#include  "data_cb.h"
#include "led.h"


#define DATA_RECORD_COUNT sizeof(data_pool)/sizeof(data_record)

data_record data_pool[] = {

        {.addr = ADDR_CONTR_FL_VALVE,               .allow = wo,  .data = 0,                      .cb = &cmd_valve_control  },
        {.addr = ADDR_CONTR_FR_VALVE,               .allow = wo,  .data = 0,                      .cb = &cmd_valve_control  },
        {.addr = ADDR_CONTR_RL_VALVE,               .allow = wo,  .data = 0,                      .cb = &cmd_valve_control  },
        {.addr = ADDR_CONTR_RR_VALVE,               .allow = wo,  .data = 0,                      .cb = &cmd_valve_control  },
        {.addr = ADDR_CONTR_F_VALVE,                .allow = wo,  .data = 0,                      .cb = &cmd_valve_control  },
        {.addr = ADDR_CONTR_R_VALVE,                .allow = wo,  .data = 0,                      .cb = &cmd_valve_control  },
        {.addr = ADDR_CONTR_ALL_VALVE,              .allow = wo,  .data = 0,                      .cb = &cmd_valve_control  },

        {.addr = ADDR_PARAM_TIME_MANUAL_EX_IN,      .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },

        {.addr = ADDR_PARAM_IS_COMPR_EMPTY,         .allow = ro,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_COMPR_CONTROL,          .allow = rw,  .data = VALUE_COMP_OFF,         .cb = &cmd_read_or_write_param  },

        {.addr = ADDR_PARAM_SET_FL_DAMP,            .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_SET_FR_DAMP,            .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_SET_RR_DAMP,            .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_SET_RL_DAMP,            .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },

        {.addr = ADDR_PARAM_IS_SHORT_CIRCUIT_SENSOR,.allow = ro,  .data = 0,                      .cb = &cmd_read_or_write_param  },

        {.addr = ADDR_PARAM_MEASURED_IN_PERS_FL,    .allow = ro,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_MEASURED_IN_PERS_FR,    .allow = ro,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_MEASURED_IN_PERS_RR,    .allow = ro,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_MEASURED_IN_PERS_RL,    .allow = ro,  .data = 0,                      .cb = &cmd_read_or_write_param  },

        {.addr = ADDR_PARAM_VALVE_TIME_ON,          .allow = rw,  .data = VALUE_VAL_ONTIME0,      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_VALVE_TIME_OFF,         .allow = rw,  .data = VALUE_VAL_OFFTIME0,     .cb = &cmd_read_or_write_param  },

        {.addr = ADDR_PARAM_DEPTH_MODE,             .allow = rw,  .data = VALUE_DEPTH_MALL,       .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_CONTROL_DEPTH,                .allow = wo,  .data = 0,                      .cb = &cmd_do_clearance_or_manual_set },

        {.addr = ADDR_PARAM_SAVE_DAMPS_VALUE,       .allow = rw,  .data = 0,                      .cb = &cmd_calibrate  },

        {.addr = ADDR_PARAM_MIN_SET_FL,             .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_MIN_SET_FR,             .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_MIN_SET_RR,             .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_MIN_SET_RL,             .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },

        {.addr = ADDR_PARAM_MAX_SET_FL,             .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_MAX_SET_RR,             .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_MAX_SET_FR,             .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_MAX_SET_RL,             .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },

        {.addr = ADDR_PARAM_MED_SET_FL,             .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_MED_SET_FR,             .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_MED_SET_RR,             .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_MED_SET_RL,             .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },

        {.addr = ADDR_PARAM_DIRCHG_FL,              .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_DIRCHG_FR,              .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_DIRCHG_RR,              .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_DIRCHG_RL,              .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },

        {.addr = ADDR_PARAM_MEASURE_FL,             .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_MEASURE_FR,             .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_MEASURE_RR,             .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_MEASURE_RL,             .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },

        {.addr = ADDR_PARAM_INTERNAL_TEMP,          .allow = ro,  .data = 0,                      .cb = &cmd_read_or_write_param  },

        {.addr = ADDR_PARAM_VOLTAGE,                .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_VOLT_SYS_THRS,          .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },

        {.addr = ADDR_PARAM_IS_DOOR_OPEN,           .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_CONTROL_DOOR,           .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },

        {.addr = ADDR_PARAM_STOP_SIG_STATE,         .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },
        {.addr = ADDR_PARAM_STOP_SIG_CONTR,         .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },

        {.addr = ADDR_PARAM_POWER_STATE,            .allow = ro,  .data = 0,                      .cb = &cmd_read_or_write_param  },

        {.addr = ADDR_PARAM_STATUS,                 .allow = rw,  .data = 0,                      .cb = &cmd_read_or_write_param  },

        {.addr = ADDR_CONTROL_SAVERESET,            .allow = wo,  .data = 0,                      .cb = &cmd_save_or_reset_user_data },
};


data_record* data_pool_at(uint16_t addr)
{
    for(uint8_t i = 0; i < DATA_RECORD_COUNT; i++)
    {
        if(data_pool[i].addr == addr)
            return &data_pool[i];
    }
    return NULL;
}


bool data_read(uint16_t addr, uint32_t* data, bool check_permission)
{
    data_record * record = data_pool_at(addr);

    if(record == NULL)
        return false;

    if(check_permission)
    {
        if(record->allow == wo)
            return false;
    }
    *data = record->data;
    return true;
}

bool data_write(uint16_t addr, uint32_t data, bool check_permission)
{
    data_record * record = data_pool_at(addr);

    if(record == NULL)
        return false;

    if(check_permission)
    {
        if(record->allow == ro)
            return false;
    }
    record->data = data;
    return true;
}


void* data_get_callback(uint16_t addr)
{
    data_record* record = data_pool_at(addr);
    return  record->cb;

}

