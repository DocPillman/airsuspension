#ifndef __CLEARANCE_H_
#define __CLEARANCE_H_

typedef enum
{
    manual_mode,
    auto_mode,

}clearance_mode_t;


clearance_mode_t clearance_get_mode(void);
void clearance_set(void);
void clearance_control_task(void);

#endif
