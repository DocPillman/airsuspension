#include "valve.h"
#include "stm32f1xx_hal.h"
#include "main.h"


void valve_fl_set(valve_cmd_t cmd)
{
    if(cmd == in)
    {
        HAL_GPIO_WritePin(FL_IN_GPIO_Port,FL_IN_Pin,GPIO_PIN_SET);
        HAL_GPIO_WritePin(FL_EX_GPIO_Port,FL_EX_Pin,GPIO_PIN_RESET);
    }
    else if(cmd == ex)
    {
        HAL_GPIO_WritePin(FL_EX_GPIO_Port,FL_EX_Pin,GPIO_PIN_SET);
        HAL_GPIO_WritePin(FL_IN_GPIO_Port,FL_IN_Pin,GPIO_PIN_RESET);
    }
    else if(cmd == block)
    {
        HAL_GPIO_WritePin(FL_IN_GPIO_Port,FL_IN_Pin,GPIO_PIN_RESET);
        HAL_GPIO_WritePin(FL_EX_GPIO_Port,FL_EX_Pin,GPIO_PIN_RESET);
    }
}

void valve_fr_set(valve_cmd_t cmd)
{
    if(cmd == in)
    {
        HAL_GPIO_WritePin(FR_IN_GPIO_Port,FR_IN_Pin,GPIO_PIN_SET);
        HAL_GPIO_WritePin(FR_EX_GPIO_Port,FR_EX_Pin,GPIO_PIN_RESET);
    }
    else if(cmd == ex)
    {
        HAL_GPIO_WritePin(FR_EX_GPIO_Port,FR_EX_Pin,GPIO_PIN_SET);
        HAL_GPIO_WritePin(FR_IN_GPIO_Port,FR_IN_Pin,GPIO_PIN_RESET);
    }
    else if(cmd == block)
    {
        HAL_GPIO_WritePin(FR_IN_GPIO_Port,FR_IN_Pin,GPIO_PIN_RESET);
        HAL_GPIO_WritePin(FR_EX_GPIO_Port,FR_EX_Pin,GPIO_PIN_RESET);
    }
}

void valve_rr_set(valve_cmd_t cmd)
{
    if(cmd == in)
    {
        HAL_GPIO_WritePin(RR_IN_GPIO_Port,RR_IN_Pin,GPIO_PIN_SET);
        HAL_GPIO_WritePin(RR_EX_GPIO_Port,RR_EX_Pin,GPIO_PIN_RESET);
    }
    else if(cmd == ex)
    {
        HAL_GPIO_WritePin(RR_EX_GPIO_Port,RR_EX_Pin,GPIO_PIN_SET);
        HAL_GPIO_WritePin(RR_IN_GPIO_Port,RR_IN_Pin,GPIO_PIN_RESET);
    }
    else if(cmd == block)
    {
        HAL_GPIO_WritePin(RR_IN_GPIO_Port,RR_IN_Pin,GPIO_PIN_RESET);
        HAL_GPIO_WritePin(RR_EX_GPIO_Port,RR_EX_Pin,GPIO_PIN_RESET);
    }
}

void valve_rl_set(valve_cmd_t cmd)
{
    if(cmd == in)
    {
        HAL_GPIO_WritePin(RL_IN_GPIO_Port,RL_IN_Pin,GPIO_PIN_SET);
        HAL_GPIO_WritePin(RL_EX_GPIO_Port,RL_EX_Pin,GPIO_PIN_RESET);
    }
    else if(cmd == ex)
    {
        HAL_GPIO_WritePin(RL_EX_GPIO_Port,RL_EX_Pin,GPIO_PIN_SET);
        HAL_GPIO_WritePin(RL_IN_GPIO_Port,RL_IN_Pin,GPIO_PIN_RESET);
    }
    else if(cmd == block)
    {
        HAL_GPIO_WritePin(RL_IN_GPIO_Port,RL_IN_Pin,GPIO_PIN_RESET);
        HAL_GPIO_WritePin(RL_EX_GPIO_Port,RL_EX_Pin,GPIO_PIN_RESET);
    }
}

void valves_set(valves_t *valves)
{
    valve_fl_set(valves->fl.cmd);
    valve_fr_set(valves->fr.cmd);
    valve_rr_set(valves->rr.cmd);
    valve_rl_set(valves->rl.cmd);
}

void valves_off(void)
{
    valve_cmd_t val = block;
    valve_fr_set(val);
    valve_rr_set(val);
    valve_rl_set(val);
    valve_fr_set(val);
}


