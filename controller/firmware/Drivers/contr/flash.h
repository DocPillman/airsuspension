#ifndef __FLASH_H_
#define __FLASH_H_

#include "stdbool.h"
#include "stdio.h"
#include "stdint.h"

bool flash_erase(void);
bool flash_write(uint32_t *data , uint16_t len);
bool flash_read(uint32_t * data, uint16_t len);

#endif
