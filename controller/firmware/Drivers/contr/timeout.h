#ifndef __TIMEOUT_H_
#define __TIMEOUT_H_

#include "stdio.h"
#include "stdbool.h"
#include "stdint.h"

#define TIMEOUT_DISABLE 0

typedef struct
{
    uint32_t  start_time;
    uint32_t timeout;
}timeout_t;

bool is_timeout(timeout_t *instance);
void timeout_set(timeout_t *instance, uint32_t timeout_arg);
bool timeout_is_enabled(timeout_t * instance);

#endif

