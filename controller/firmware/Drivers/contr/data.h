

#ifndef __DATA_H_
#define __DATA_H_

#include "pdata.h"
#include "stdbool.h"
#include "addr_map.h"
#include "stdint.h"


typedef void (*record_cb)(payload_t* payload);

typedef enum
{
    ro,
    rw,
    wo,

}permission_t;

typedef struct
{
    uint16_t addr;
    permission_t allow;
    uint32_t data;
    record_cb cb;

}data_record;

bool data_read(uint16_t addr, uint32_t* data, bool check_permission);
bool data_write(uint16_t addr, uint32_t data, bool check_permission);

void data_set_callback(uint16_t addr, void* cb);
void* data_get_callback(uint16_t addr);

#endif
