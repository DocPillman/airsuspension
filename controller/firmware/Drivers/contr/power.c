#include "power.h"
#include "stm32f1xx_hal.h"
#include "main.h"
#include "data.h"



void power_update_state(void)
{
    int8_t power_state = 0;
    for(uint8_t i = 0; i < 100; i++)
    {
        if(HAL_GPIO_ReadPin(ENABLE_GPIO_Port,ENABLE_Pin))
            power_state++;
        else
            power_state--;
    }
    if(power_state >= 70)
        data_write(ADDR_PARAM_POWER_STATE,VALUE_POWER_ON,false);
    else
        data_write(ADDR_PARAM_POWER_STATE,VALUE_POWER_SLEEP,false);
}

bool power_is_off(void)
{
    uint32_t state;
    data_read(ADDR_PARAM_POWER_STATE,&state,false);
    return state == VALUE_POWER_SLEEP ? true : false ;
}
