#ifndef __STOP_H_
#define __STOP_H_

#include "stdbool.h"

void stop_update_state(void);

bool stop_is_pushed(void);
bool stop_ctrl_is_on(void);

#endif
