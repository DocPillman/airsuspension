
#ifndef __VALVE_CMD_H_
#define __VALVE_CMD_H_

#include "valve.h"

typedef enum
{
    v_manual,
    v_auto,

}valve_mode_t;

void valve_cmd_tim_handler(void);


void valve_set_mode(valve_mode_t valve_mode);
valve_mode_t valve_get_mode(void);


void valve_cmd_manual_fl(valve_cmd_t cmd);
void valve_cmd_manual_fr(valve_cmd_t cmd);
void valve_cmd_manual_rr(valve_cmd_t cmd);
void valve_cmd_manual_rl(valve_cmd_t cmd);
void valve_cmd_manual_r(valve_cmd_t cmd);
void valve_cmd_manual_f(valve_cmd_t cmd);
void valve_cmd_manual_all(valve_cmd_t cmd);


void valve_cmd_auto_fl(valve_cmd_t cmd);
void valve_cmd_auto_fr(valve_cmd_t cmd);
void valve_cmd_auto_rr(valve_cmd_t cmd);
void valve_cmd_auto_rl(valve_cmd_t cmd);
void valve_cmd_auto_block_all(void);

#endif
