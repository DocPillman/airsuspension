#include <main.h>
#include "sensor.h"
#include "data.h"
#include "calibrate.h"
#include "help.h"
#include "math.h"
#include "addr_map.h"
#include "stm32f1xx_hal.h"



static  uint32_t absi(int32_t a){return a < 0 ? -a : a ;}

uint32_t sensor_fl_height(void)
{
    uint32_t data;
    data_read(ADDR_PARAM_MEASURE_FL,&data,false);
    return data;
}

uint32_t sensor_fr_height(void)
{
    uint32_t data;
    data_read(ADDR_PARAM_MEASURE_FR,&data,false);
    return data;
}

uint32_t sensor_rr_height(void)
{
    uint32_t data;
    data_read(ADDR_PARAM_MEASURE_RR,&data,false);
    return data;
}

uint32_t sensor_rl_height(void)
{
    uint32_t data;
    data_read(ADDR_PARAM_MEASURE_RL,&data,false);
    return data;
}

uint32_t convert_sensor_data_to_persent(uint32_t max, uint32_t min, uint8_t dir_change, uint32_t converted_data)
{
    uint32_t range = absi(max - min);
    
    uint32_t in_percent = 100 * converted_data / range ;
    
    if(in_percent > 100)
        return 0;
    
    if(dir_change == VALUE_DIRCHG_MAXDOWN_MINUP)
        in_percent = 100 - in_percent;
    
    return in_percent;
}

void sensor_user_info_update(void)
{
    uint32_t max,min,measured,dir_change,in_percent;
    if(!calibrate_is_done())
    {
        data_write(ADDR_PARAM_MEASURED_IN_PERS_FL,VALUE_PERSENT_UNKOWN,false);
        data_write(ADDR_PARAM_MEASURED_IN_PERS_FR,VALUE_PERSENT_UNKOWN,false);
        data_write(ADDR_PARAM_MEASURED_IN_PERS_RR,VALUE_PERSENT_UNKOWN,false);
        data_write(ADDR_PARAM_MEASURED_IN_PERS_RL,VALUE_PERSENT_UNKOWN,false);
        return;
    }
    data_read(ADDR_PARAM_MAX_SET_FL,&max,false);
    data_read(ADDR_PARAM_MAX_SET_FL,&min,false);
    data_read(ADDR_PARAM_MEASURE_FL,&measured,false);
    data_read(ADDR_PARAM_DIRCHG_FL,&dir_change,false);
    in_percent = convert_sensor_data_to_persent(max,min,dir_change,measured); 
    data_write(ADDR_PARAM_MEASURED_IN_PERS_FL,in_percent,false);
    
    data_read(ADDR_PARAM_MAX_SET_FR,&max,false);
    data_read(ADDR_PARAM_MAX_SET_FR,&min,false);
    data_read(ADDR_PARAM_MEASURE_FR,&measured,false);
    data_read(ADDR_PARAM_DIRCHG_FR,&dir_change,false);
    in_percent = convert_sensor_data_to_persent(max,min,dir_change,measured); 
    data_write(ADDR_PARAM_MEASURED_IN_PERS_FR,in_percent,false);
    
    data_read(ADDR_PARAM_MAX_SET_RR,&max,false);
    data_read(ADDR_PARAM_MAX_SET_RR,&min,false);
    data_read(ADDR_PARAM_MEASURE_RR,&measured,false);
    data_read(ADDR_PARAM_DIRCHG_RR,&dir_change,false);
    in_percent = convert_sensor_data_to_persent(max,min,dir_change,measured); 
    data_write(ADDR_PARAM_MEASURED_IN_PERS_RR,in_percent,false);
    
    data_read(ADDR_PARAM_MAX_SET_RL,&max,false);
    data_read(ADDR_PARAM_MAX_SET_RL,&min,false);
    data_read(ADDR_PARAM_MEASURE_RL,&measured,false);
    data_read(ADDR_PARAM_DIRCHG_RL,&dir_change,false);
    in_percent = convert_sensor_data_to_persent(max,min,dir_change,measured); 
    data_write(ADDR_PARAM_MEASURED_IN_PERS_RL,in_percent,false);
   
}


