#include "chks.h"



uint8_t chks_is_ok (uint8_t* packet)
{
    packet_t *pct = (packet_t *)packet;

    if(packet == NULL)
        return 0;

    if (pct->chks != chks(packet))
        return 0;

    return 1;
}

uint8_t chks(uint8_t* packet)
{
    uint8_t i,chks = 0;
    if (packet == NULL)
        return 0;

    for(i = 0; i < PACKET_SIZE - 1; i++)
    {
        chks += packet[i];
    }
    chks = -chks;
    return chks;

}
