#include "buffer.h"
#include "stdbool.h"
#include "string.h"


/*
 *   Initialize a circular buffer of type buffer_type
 */
void buffer_init(buffer_type *name_buffer, int size, uint8_t *buffer, uint8_t element_size, int max_pop_size)
{
    name_buffer->size = size;
    name_buffer->start = 0;
    name_buffer->count = 0;
    name_buffer->end = 0;
    name_buffer->elem_size = element_size;
    name_buffer->max_pop_size = max_pop_size;
    name_buffer->element = buffer;
}

/*
 *   flushes the buffer
 *   *buffer : pointer to ring buffer
 */
void buffer_flash_queue(buffer_type *buffer)
{
    buffer->start = buffer->end;  //the tail goes up to the head and buffer becomes empty
    buffer->count = 0;
}

/*
 * checks if the buffer is half full (empty)
 * status of ring buffer (=1 if half full =0 otherwise)
 */ 
int buffer_is_halffull(buffer_type *buffer)
{
  return buffer->count >= buffer->size - buffer->max_pop_size ? 1 : 0;
}

/*
 * is_half_empty
 * checks if the buffer is less than half
 * status of ring buffer (=1 if half empty =0 otherwise)
 */
int buffer_is_halfempty(buffer_type *buffer)
{
  return (buffer->count <= buffer->max_pop_size) ? 1 : 0;
}

/*
 * full
 * indicates if the given buffer is full or not
 * status of ring buffer (=1 if full =0 otherwise)
 */
int buffer_is_full(buffer_type *buffer)
{
  return buffer->count == buffer->size ? 1 : 0;
}

/*
 *  empty
 *  indicates if the given buffer is empty or not
 *  status of ring buffer (=1 if empty =0 otherwise)
 */
int buffer_is_empty(buffer_type *buffer)
{
  return buffer->count == 0 ? 1 : 0;
}

/*
 *  push_buffer_queue
 *  pushes a new item onto the circular buffer (queues it)
 *  data : value to be Q'ed in ring buffer.
 */
void buffer_push_queue(buffer_type *buffer, uint8_t *data)
{
    if (buffer_is_full(buffer))
    {
      return;
    }
    else
    {
        buffer->count+=buffer->elem_size;
        memcpy(&buffer->element[buffer->end],data, buffer->elem_size);
        buffer->end+=buffer->elem_size;
        buffer->end %= buffer->size;
    }

}
 

void  buffer_pop_queue(buffer_type *buffer, uint8_t *element)
{

    if (buffer_is_empty(buffer))
    {
      return;
    }
    else {
        /* First in First Out*/
      memcpy(element, &buffer->element[buffer->start], buffer->elem_size);
      buffer->start = buffer->start + buffer->elem_size;
      buffer->count-=buffer->elem_size;
      buffer->start %= buffer->size;
    }

}

/*
 * rewinds_buffer_queue
 * rewinds the circular buffer
 * count : number of bytes to rewind 
 */ 
void buffer_rewinds_queue(buffer_type *buffer, int count)
{   
    int buf_end = buffer->end;
    if(buffer->start - count >= 0)
      {
          buffer->start = buffer->start - count;
          if(buf_end > buffer->start) 
              buffer->count = buf_end - buffer->start;
          else 
              buffer->count = (buffer->size-buffer->start)+buf_end;
      }
    else
      {
          buffer->start = buffer->size - (count - buffer->start);
          buffer->count = (buffer->size - buffer->start)+ buf_end;
      }
}


