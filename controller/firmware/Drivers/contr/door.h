#ifndef __DOOR_H_
#define __DOOR_H_

#include "stdbool.h"

void door_update_state(void);

bool door_ctrl_is_on(void);
bool door_is_open(void);

#endif
