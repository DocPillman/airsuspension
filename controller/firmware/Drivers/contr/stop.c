#include "stop.h"
#include "stm32f1xx_hal.h"
#include "main.h"
#include "data.h"



void stop_update_state(void)
{
    int8_t stop_state = 0;
    for(uint32_t i = 0; i < 100; i++)
    {
        if(HAL_GPIO_ReadPin(STOP_GPIO_Port,STOP_Pin))
            stop_state++;
        else
            stop_state--;
    }
    if(stop_state >= 70)
        data_write(ADDR_PARAM_STOP_SIG_STATE,VALUE_STOP_IS_PUSHED,false);
    else
        data_write(ADDR_PARAM_STOP_SIG_STATE,VALUE_STOP_DONOT_PUSH,false);
}

bool stop_is_pushed(void)
{
    uint32_t state;
    data_read(ADDR_PARAM_STOP_SIG_STATE,&state,false);
    return  state == VALUE_STOP_IS_PUSHED ? true : false;
}

bool stop_ctrl_is_on(void)
{
    uint32_t state;
    data_read(ADDR_PARAM_STOP_SIG_CONTR,&state,false);
    return state == VALUE_STOP_ENABLE ? true : false ;
}
