#include "proto.h"
#include "uart.h"
#include "stm32f1xx_hal.h"
#include "status.h"
#include "data.h"
#include "led.h"
#include "timeout.h"

proto_t proto;

timeout_t reinit_proto_timeout;
timeout_t uart_busy_timeout;

void proto_init(void)
{
    proto.state = idle;

    buffer_init(&proto._request_buffer,REQUEST_BUFFER_SIZE,proto.request_buffer,
                                               PAYLOAD_SIZE,REQUEST_BUFFER_SIZE/2);
    buffer_init(&proto._response_buffer,RESPONSE_BUFFER_SIZE,proto.response_buffer,
                                              PAYLOAD_SIZE,RESPONSE_BUFFER_SIZE/2);
    buffer_flash_queue(&proto._request_buffer);
    buffer_flash_queue(&proto._response_buffer);
    proto.state = idle;
}



void proto_request(payload_t *payload)
{
	__disable_irq();
    buffer_push_queue(&proto._request_buffer,(uint8_t * )payload);
    __enable_irq();
}


void proto_response( uint8_t command_type, uint16_t address, uint32_t data )
{
    payload_t payload;
    payload.type_command = command_type;
    payload.address = address;
    payload.data = data;

    __disable_irq();
    buffer_push_queue(&proto._response_buffer,(uint8_t*)&payload);
    __enable_irq();
}

void proto_error_report(payload_t* payload, uint32_t error)
{
	uint8_t response_type = 0;
    if(payload->type_command == CMD_TYPE_READ)
        response_type = CMD_TYPE_READ_ERROR;
    else if(payload->type_command == CMD_TYPE_WRITE)
        response_type = CMD_TYPE_WRITE_ERROR;

    proto_response(response_type,payload->address,payload->data);
    status_set(error);
}

void proto_request_parser(payload_t *payload)
{

    uint8_t success = 0 ;
    record_cb cb = (record_cb)data_get_callback(payload->address);

    if(cb != NULL)
        success = 1;

    if(payload->type_command == CMD_TYPE_READ ||
       payload->type_command == CMD_TYPE_WRITE)
        success = 1;

    if(success != 1)
        proto_error_report(payload,VALUE_STATUS_ERROR_COMMUNICATION);
    else{
        status_clear(VALUE_STATUS_ERROR_COMMUNICATION);
        cb(payload);
        }


}


void proto_buf_worker(void)
{
    payload_t payload;
    if(!buffer_is_empty(&proto._request_buffer))
    {
        __disable_irq();
        buffer_pop_queue(&proto._request_buffer,(uint8_t * )&payload);
        __enable_irq();
        proto_request_parser(&payload);
    }
    if(!(buffer_is_empty(&proto._response_buffer)) && (idle == uart_get_tx_state()))
    {
        __disable_irq();
        buffer_pop_queue(&proto._response_buffer,(uint8_t*)&payload);
        __enable_irq();
        uart_transmit(&payload);
    }
}


void proto_task(void)
{
    proto_buf_worker();
    
    if( uart_get_rx_state() == busy && !timeout_is_enabled(&uart_busy_timeout))
        timeout_set(&uart_busy_timeout,TIME_GETTING_PACKET);
    else if(uart_get_rx_state() == busy && is_timeout(&uart_busy_timeout))
		proto.state = error;
	else if(uart_get_rx_state() != busy)
		timeout_set(&uart_busy_timeout,TIMEOUT_DISABLE);


	if(uart_get_rx_state() == error || proto.state == error)
    {
        timeout_set(&reinit_proto_timeout,TIME_PROTO_REBOOT);
		proto.state = error;
		while(!is_timeout(&reinit_proto_timeout))
		{
			HAL_Delay(1);
		}
		uart_init();
		proto_init();
        timeout_set(&uart_busy_timeout,TIMEOUT_DISABLE);
        timeout_set(&reinit_proto_timeout,TIMEOUT_DISABLE);
    }
}


