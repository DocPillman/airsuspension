#include "stdbool.h"
#ifndef __CALIBRATE_H_
#define __CALIBRATE_H_


#define DEPTH_HISTR 50U

void calibrate_save_measure_to_max(void);
void calibrate_save_measure_to_med(void);
void calibrate_save_measure_to_min(void);

bool calibrate_saved_data_is_ok(void);
void calibrate_determining_directions(void);

bool calibrate_is_done(void);
void calibrate_clear(void);


#endif
