#ifndef  __PDATA_H_
#define  __PDATA_H_

#include "stdio.h"
#include "stdint.h"


#define PAYLOAD_SIZE  sizeof(payload_t)
#define PACKET_SIZE   sizeof(packet_t)


#pragma pack (1)

typedef struct
{
    uint8_t type_command;
    uint16_t address;
    uint32_t data;
}payload_t;

typedef struct
{
    uint8_t id;
    payload_t payload;
    uint8_t chks;

}packet_t;

#pragma pack ()

#endif
