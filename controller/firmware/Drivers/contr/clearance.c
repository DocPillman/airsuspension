#include "clearance.h"
#include "valve_cmd.h"
#include "calibrate.h"
#include "sensor_pwr.h"
#include "sensor.h"
#include "power.h"
#include "data.h"
#include "stm32f1xx_hal.h"
#include "addr_map.h"
#include "help.h"
#include "door.h"
#include "battery.h"
#include "status.h"
#include "timeout.h"

#define CLEARANCE_TIME_SET 56000U
#define RWP_SENSOR_RELOAD_TIMEOUT 1000

typedef enum
{
    above,
    below,
    fine
}dump_state_t;


uint8_t clearance_do;

static  uint32_t absi(int32_t a){return a < 0 ? -a : a ;}

clearance_mode_t clearance_get_mode(void)
{
    uint32_t depth;
    data_read(ADDR_PARAM_DEPTH_MODE,&depth,false);
    clearance_mode_t mode ;
    if(depth == VALUE_DEPTH_MIN ||
       depth == VALUE_DEPTH_MED ||
       depth == VALUE_DEPTH_MAX )
        mode = auto_mode;
    
    if(depth == VALUE_DEPTH_MF ||
       depth == VALUE_DEPTH_MR ||
       depth == VALUE_DEPTH_MALL)
        mode = manual_mode;
    
    return mode;
}


uint32_t clearance_get_fl_setting(void)
{
    uint32_t depth;
    data_read(ADDR_PARAM_DEPTH_MODE,&depth,false);
    uint32_t set;
    switch (depth) {
        case VALUE_DEPTH_MIN :
            data_read(ADDR_PARAM_MIN_SET_FL,&set,false);
            break;
        case VALUE_DEPTH_MED :
            data_read(ADDR_PARAM_MED_SET_FL,&set,false);
            break;
        case VALUE_DEPTH_MAX :
            data_read(ADDR_PARAM_MAX_SET_FL,&set,false);
            break;
        default:
            break;
    }
    return set;
}


uint32_t clearance_get_fr_setting(void)
{
    uint32_t depth;
    data_read(ADDR_PARAM_DEPTH_MODE,&depth,false);
    uint32_t set;
    switch (depth) {
        case VALUE_DEPTH_MIN :
            data_read(ADDR_PARAM_MIN_SET_FR,&set,false);
            break;
        case VALUE_DEPTH_MED :
            data_read(ADDR_PARAM_MED_SET_FR,&set,false);
            break;
        case VALUE_DEPTH_MAX :
            data_read(ADDR_PARAM_MAX_SET_FR,&set,false);
            break;
        default:
            break;
    }
    return set;
}


uint32_t clearance_get_rr_setting(void)
{
    uint32_t depth;
    data_read(ADDR_PARAM_DEPTH_MODE,&depth,false);
    uint32_t set;
    switch (depth) {
        case VALUE_DEPTH_MIN :
            data_read(ADDR_PARAM_MIN_SET_RR,&set,false);
            break;
        case VALUE_DEPTH_MED :
            data_read(ADDR_PARAM_MED_SET_RR,&set,false);
            break;
        case VALUE_DEPTH_MAX :
            data_read(ADDR_PARAM_MAX_SET_RR,&set,false);
            break;
        default:
            break;
    }
    return set;
}


uint32_t clearance_get_rl_setting(void)
{
    uint32_t depth;
    data_read(ADDR_PARAM_DEPTH_MODE,&depth,false);
    uint32_t set;
    switch (depth) {
        case VALUE_DEPTH_MIN :
            data_read(ADDR_PARAM_MIN_SET_RL,&set,false);
            break;
        case VALUE_DEPTH_MED :
            data_read(ADDR_PARAM_MED_SET_RL,&set,false);
            break;
        case VALUE_DEPTH_MAX :
            data_read(ADDR_PARAM_MAX_SET_RL,&set,false);
            break;
        default:
            break;
    }
    return set;
}

void clearance_apply_current_setting(void)
{
    uint32_t fl,fr,rr,rl;
    fl = clearance_get_fl_setting();
    fr = clearance_get_fr_setting();
    rr = clearance_get_rr_setting();
    rl = clearance_get_rl_setting();
    data_write(ADDR_PARAM_SET_FL_DAMP,fl,false);
    data_write(ADDR_PARAM_SET_FR_DAMP,fr,false);
    data_write(ADDR_PARAM_SET_RR_DAMP,rr,false);
    data_write(ADDR_PARAM_SET_RL_DAMP,rl,false);
}

dump_state_t compare_set_height_with_measured(uint32_t set, uint32_t measured , uint32_t dir_changing)
{
	dump_state_t state;
	int32_t delta = set - measured;

	if(absi(delta) < DEPTH_HISTR)
		state = fine;
	else if((delta < 0 && dir_changing == VALUE_DIRCHG_MAXUP_MINDOWN) || (delta > 0 && dir_changing == VALUE_DIRCHG_MAXDOWN_MINUP))
		state = above;
	else
		state = below;
	return  state;
}


dump_state_t clearance_get_state_fl_dump(void)
{
	uint32_t target,measured,dir_changing;
    data_read(ADDR_PARAM_SET_FL_DAMP,&target,false);
    data_read(ADDR_PARAM_DIRCHG_FL,&dir_changing,false);
	measured = sensor_fl_height();
	return compare_set_height_with_measured(target,measured,dir_changing);
}

dump_state_t clearance_get_state_fr_dump(void)
{
	uint32_t target,measured,dir_changing;
    data_read(ADDR_PARAM_SET_FR_DAMP,&target,false);
    data_read(ADDR_PARAM_DIRCHG_FR,&dir_changing,false);
	measured = sensor_fr_height();
	return compare_set_height_with_measured(target,measured,dir_changing);
}

dump_state_t clearance_get_state_rr_dump(void)
{
	uint32_t target,measured,dir_changing;
    data_read(ADDR_PARAM_SET_RR_DAMP,&target,false);
    data_read(ADDR_PARAM_DIRCHG_RR,&dir_changing,false);
	measured = sensor_rr_height();
	return compare_set_height_with_measured(target,measured,dir_changing);
}

dump_state_t clearance_get_state_rl_dump(void)
{
	uint32_t target,measured,dir_changing;
    data_read(ADDR_PARAM_SET_RL_DAMP,&target,false);
    data_read(ADDR_PARAM_DIRCHG_RL,&dir_changing,false);
	measured = sensor_rl_height();
	return compare_set_height_with_measured(target,measured,dir_changing);
}

void clearance_set(void)
{
    clearance_do = 1;
}

bool clearance_is_ok(void)
{
    clearance_apply_current_setting();

	if(clearance_get_state_fl_dump() != fine)
		return false;
	if(clearance_get_state_fr_dump() != fine)
		return false;
	if(clearance_get_state_rr_dump() != fine)
		return false;
	if(clearance_get_state_rl_dump() != fine)
		return false;

	return true;
}

void clearance_set_fl_dump(void)
{
    dump_state_t state = clearance_get_state_fl_dump();
    valve_cmd_t cmd  = block;

    if(state == above)
        cmd = ex;
    else if (state == below)
        cmd = in;

    valve_cmd_auto_fl(cmd);
}

void clearance_set_fr_dump(void)
{
    dump_state_t state = clearance_get_state_fr_dump();
    valve_cmd_t cmd  = block;

    if(state == above)
        cmd = ex;
    else if (state == below)
        cmd = in;

    valve_cmd_auto_fr(cmd);
}

void clearance_set_rr_dump(void)
{
    dump_state_t state = clearance_get_state_rr_dump();
    valve_cmd_t cmd  = block;

    if(state == above)
        cmd = ex;
    else if (state == below)
        cmd = in;

    valve_cmd_auto_rr(cmd);
}

void clearance_set_rl_dump(void)
{
    dump_state_t state = clearance_get_state_rl_dump();
    valve_cmd_t cmd  = block;

    if(state == above)
        cmd = ex;
    else if (state == below)
        cmd = in;

    valve_cmd_auto_rl(cmd);
}

void clearance_set_all_dumps(void)
{
    clearance_apply_current_setting();
    clearance_set_fl_dump();
    clearance_set_fr_dump();
    clearance_set_rl_dump();
    clearance_set_rr_dump();
}

bool clearance_can_set(void)
{
    bool bat_block, pwr_block, door_block, sensor_short_circuit, can_set;
    bat_block = pwr_block = door_block = sensor_short_circuit = can_set = false;

    if(door_ctrl_is_on() && door_is_open())
        door_block = true;

    if(battery_ctrl_is_on() && !battery_is_ok())
        bat_block =  true;

    pwr_block = power_is_off();

    sensor_short_circuit = sensor_power_in_short_circuit();

    if(!pwr_block && !bat_block && !door_block && !sensor_short_circuit)
        can_set = true;

    return can_set;
}

void clearance_do_set(void)
{

    uint32_t time_on,time_off;
    valve_mode_t valve_mode;
    data_read(ADDR_PARAM_VALVE_TIME_ON,&time_on,false);
    data_read(ADDR_PARAM_VALVE_TIME_OFF,&time_off,false);
    valve_mode = valve_get_mode();

    if(valve_mode != v_auto)
        valve_set_mode(v_auto);

    clearance_set_all_dumps();
    HAL_Delay(time_on);
    valve_cmd_auto_block_all();
    HAL_Delay(time_off);

}



void clearance_control_task(void)
{
    if(clearance_do)
    {
        if(calibrate_is_done())
        {
            status_clear(VALUE_STATUS_CLEARANCE_SET_TIMEOUT);
            status_clear(VALUE_STATUS_ERROR_CALIBRATE);
            timeout_t ctimeout;
            timeout_set(&ctimeout,CLEARANCE_TIME_SET);

            while (!clearance_is_ok() && clearance_can_set())
            {
                clearance_do_set();
                if(clearance_get_mode() == manual_mode)
                {
                  clearance_do = 0;
                    return;
                }
                if(is_timeout(&ctimeout))
                {
                    status_set(VALUE_STATUS_CLEARANCE_SET_TIMEOUT);
                    clearance_do = 0;
                    return;
                }
            }

        }
        clearance_do = 0;
    }
    if(clearance_get_mode() == manual_mode)
        status_clear(VALUE_STATUS_ERROR_CALIBRATE);

    if(sensor_power_in_short_circuit())
    {
        sensor_power_remote(off);
        timeout_t power_reload;
        status_set(VALUE_STATUS_ERROR_SHORT_CIRCUIT_SENS);
        timeout_set(&power_reload,RWP_SENSOR_RELOAD_TIMEOUT);
        HAL_Delay(RWP_SENSOR_RELOAD_TIMEOUT);
        sensor_power_remote(on);
        HAL_Delay(RWP_SENSOR_RELOAD_TIMEOUT);
    } else
        status_clear(VALUE_STATUS_ERROR_SHORT_CIRCUIT_SENS);

}
