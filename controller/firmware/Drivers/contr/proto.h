#ifndef __PROTO_H_
#define __PROTO_H_

#include "buffer.h"
#include "pdata.h"


#define TIME_GETTING_PACKET 100
#define TIME_PROTO_REBOOT 1000

#define MASTER_ID 0x02
#define SLAVE_ID  0x01

#define CMD_TYPE_READ 0x03
#define CMD_TYPE_WRITE 0x05

#define CMD_TYPE_READ_OK 0x43
#define CMD_TYPE_WRITE_OK 0x45

#define CMD_TYPE_READ_ERROR 0x33
#define CMD_TYPE_WRITE_ERROR 0x35

#define REQUEST_BUFFER_SIZE PAYLOAD_SIZE * 20
#define RESPONSE_BUFFER_SIZE PAYLOAD_SIZE * 20


typedef enum
{
    idle,
    busy,
    error,
    
}state_t;

typedef struct
{

    buffer_type _request_buffer;
    buffer_type _response_buffer;

    uint8_t request_buffer[REQUEST_BUFFER_SIZE];
    uint8_t response_buffer[RESPONSE_BUFFER_SIZE];

    state_t state;
    

}proto_t;


void proto_init(void);
void proto_task(void); // must call every ms

void proto_request(payload_t *payload);
void proto_response(uint8_t type_command, uint16_t address, uint32_t data);
void proto_error_report(payload_t* payload, uint32_t error);

#endif

