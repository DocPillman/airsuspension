#include "help.h"
#include "compressor.h"
#include "battery.h"
#include "power.h"
#include "data.h"
#include "main.h"
#include "addr_map.h"
#include "air_accumulator.h"
#include "stm32f1xx_hal.h"
#include "status.h"
#include "timeout.h"
#include "led.h"

#define MAX_TIME_COMPRESSOR_WORKING 56000U // ms
#define TIME_PAUSE_COMPRESSOR 16000U //ms


void compressor_remote(ctrl_t ctrl)
{
	if(ctrl == on)
        HAL_GPIO_WritePin(COMP_KEY_GPIO_Port,COMP_KEY_Pin,GPIO_PIN_SET);
	else if(ctrl == off)
        HAL_GPIO_WritePin(COMP_KEY_GPIO_Port,COMP_KEY_Pin,GPIO_PIN_RESET);
}

bool compressor_can_turn_on(void)
{
	bool pwr_block, bat_block, compressor_allow;
	pwr_block = bat_block = compressor_allow = false;

	if(battery_ctrl_is_on() && !battery_is_ok())
		bat_block = true;

	pwr_block = power_is_off();

	if(!pwr_block && !bat_block)
		compressor_allow = true;

	return compressor_allow;
}

bool compressor_ctrl_is_on(void)
{
    uint32_t val;
    data_read(ADDR_PARAM_COMPR_CONTROL,&val,false);
    return val == VALUE_COMP_ON ? true : false ;
}

 
void compressor_control_task(void)
{
	timeout_t timeout = {.start_time = 0, .timeout = 0};
    timeout_set(&timeout,MAX_TIME_COMPRESSOR_WORKING);

	while(compressor_ctrl_is_on() && compressor_can_turn_on() && air_accumulator_is_empty())
	{
		compressor_remote(on);
        status_clear(VALUE_STATUS_COMPRESSOR_TIMEOUT);
		if(is_timeout(&timeout))
		{
			compressor_remote(off);
 			status_set(VALUE_STATUS_COMPRESSOR_TIMEOUT);
			timeout_set(&timeout,TIME_PAUSE_COMPRESSOR);
			while (!is_timeout(&timeout))
			{
                HAL_Delay(1);
			}

            timeout_set(&timeout,MAX_TIME_COMPRESSOR_WORKING);
		}
	}
	compressor_remote(off);
}

