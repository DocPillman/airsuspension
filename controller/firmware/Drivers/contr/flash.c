#include "flash.h"
#include "stm32f1xx_hal.h"
#include "main.h"
#include "stdlib.h"
#include "string.h"

typedef enum
{
    idle,
    writing,
    reading,
    erasing,
    error,

}flash_state_t;

#define ADDR_FLASH_PAGE_30    ((uint32_t)0x08007800) /* Base @ of Page 30, 1 Kbytes */
#define ADDR_FLASH_PAGE_31    ((uint32_t)0x08007C00) /* Base @ of Page 31, 1 Kbytes */
#define FLASH_USER_START_ADDR   ADDR_FLASH_PAGE_30   /* Start @ of user Flash area */
#define FLASH_USER_END_ADDR     ADDR_FLASH_PAGE_30 + FLASH_PAGE_SIZE   /* End @ of user Flash area */


flash_state_t flash_state = idle;


bool flash_erase(void)
{
    if(flash_state != idle)
        return false;
    
    flash_state = erasing;
    uint32_t PAGEError = 0;
    FLASH_EraseInitTypeDef EraseInitStruct;
    EraseInitStruct.TypeErase   = FLASH_TYPEERASE_PAGES;
    EraseInitStruct.PageAddress = FLASH_USER_START_ADDR;
    EraseInitStruct.NbPages     = (FLASH_USER_END_ADDR - FLASH_USER_START_ADDR) / FLASH_PAGE_SIZE;
    HAL_FLASH_Unlock();
    
    if(HAL_FLASHEx_Erase(&EraseInitStruct, &PAGEError) != HAL_OK)
    {
        Error_Handler();
        flash_state = error;
        return false;
    }
    flash_state = idle;
    return true;
}


bool flash_write(uint32_t *data , uint16_t len)
{
    uint32_t record = 0;
    HAL_StatusTypeDef status = HAL_ERROR;
    volatile uint32_t address = FLASH_USER_START_ADDR;

    if(!flash_erase() || data == NULL)
    {
        Error_Handler();
        flash_state = error;
        return false;
    }

    flash_state = writing;

    for (uint16_t i = 0; i < len ; i++)
    {
        while(status != HAL_OK)
        {
            if(address > FLASH_USER_END_ADDR)
            {
                Error_Handler();
                flash_state = error;
                return false;
            }

            status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD,address,*data);

            if(status == HAL_ERROR || status == HAL_TIMEOUT )
            {
                Error_Handler();
                flash_state = error;
                return false;
            }
        }
        data++;
        address += sizeof(record);
        status = HAL_ERROR;
    }
    flash_state = idle;
    return true;
}


bool flash_read(uint32_t * data, uint16_t len)
{
    uint32_t record = 0;
    __IO uint32_t address = FLASH_USER_START_ADDR;

    if(flash_state != idle)
        return false;

    flash_state = reading;
    for(uint16_t i = 0; i < len; i++)
    {
        record = *(__IO uint32_t *)address;
        data[i] = record;
        address += sizeof(record);
    }
    flash_state = idle;
    return true;

}
