#include "stdbool.h"

#ifndef __BATTERY_H_
#define __BATTERY_H_



bool battery_is_ok(void);

bool battery_ctrl_is_on(void);
void battery_user_info_update(void);

#endif
