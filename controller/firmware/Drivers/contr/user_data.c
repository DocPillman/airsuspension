
#include "user_data.h"
#include "data.h"
#include "calibrate.h"
#include "flash.h"


#pragma pack(1)

typedef struct
{
    uint32_t addr;
    uint32_t data;

}user_data_record_t;

#pragma pack()

#define  USER_DATA_COUNT sizeof(user_data_list_default)/sizeof(user_data_record_t)

static const user_data_record_t user_data_list_default[] = {

        { .addr = ADDR_PARAM_TIME_MANUAL_EX_IN ,        .data = 2                       },
        { .addr = ADDR_PARAM_COMPR_CONTROL,             .data = VALUE_COMP_OFF          },
        { .addr = ADDR_PARAM_VALVE_TIME_ON,             .data = VALUE_VAL_ONTIME5       },
        { .addr = ADDR_PARAM_VALVE_TIME_OFF,            .data = VALUE_VAL_OFFTIME17     },
        { .addr = ADDR_PARAM_DEPTH_MODE,                .data = VALUE_DEPTH_MALL        },
        { .addr = ADDR_PARAM_SAVE_DAMPS_VALUE,          .data = VALUE_DONT_SAVE_DAMPS   },
        { .addr = ADDR_PARAM_MIN_SET_FL,                .data = 0                       },
        { .addr = ADDR_PARAM_MIN_SET_FR,                .data = 0                       },
        { .addr = ADDR_PARAM_MIN_SET_RR,                .data = 0                       },
        { .addr = ADDR_PARAM_MIN_SET_RL,                .data = 0                       },
        { .addr = ADDR_PARAM_MAX_SET_FL,                .data = 0                       },
        { .addr = ADDR_PARAM_MAX_SET_FR,                .data = 0                       },
        { .addr = ADDR_PARAM_MAX_SET_RR,                .data = 0                       },
        { .addr = ADDR_PARAM_MAX_SET_RL,                .data = 0                       },
        { .addr = ADDR_PARAM_MED_SET_FL,                .data = 0                       },
        { .addr = ADDR_PARAM_MED_SET_FR,                .data = 0                       },
        { .addr = ADDR_PARAM_MED_SET_RR,                .data = 0                       },
        { .addr = ADDR_PARAM_MED_SET_RL,                .data = 0                       },
        { .addr = ADDR_PARAM_DIRCHG_FL,                 .data = 0                       },
        { .addr = ADDR_PARAM_DIRCHG_FR,                 .data = 0                       },
        { .addr = ADDR_PARAM_DIRCHG_RR,                 .data = 0                       },
        { .addr = ADDR_PARAM_DIRCHG_RL,                 .data = 0                       },
        { .addr = ADDR_PARAM_VOLT_SYS_THRS,             .data = VALUE_TRSH_VOL_DISABLE  },
        { .addr = ADDR_PARAM_CONTROL_DOOR,              .data = VALUE_DOOR_CONTR_OFF    },
        { .addr = ADDR_PARAM_STOP_SIG_CONTR,            .data = VALUE_STOP_DISABLE      },
};


void user_data_set_default(void)
{
    uint32_t  data,addr;
    for (uint8_t  i = 0; i < USER_DATA_COUNT; ++i)
    {
        data = user_data_list_default[i].data;
        addr = user_data_list_default[i].addr;
        data_write(addr,data,false);
    }
}


bool user_data_save_to_flash(void)
{
    user_data_record_t record[USER_DATA_COUNT];
    uint32_t data, addr;
    for (uint8_t i = 0; i < USER_DATA_COUNT; i++)
    {
           addr = user_data_list_default[i].addr;
           data_read(addr,&data,false);
           record[i].addr = addr;
           record[i].data = data;
    }
    if(flash_write((uint32_t*)&record, sizeof(record) / sizeof(uint32_t)))
        return true;

    return false;
}

bool user_data_read_from_flash(void)
{
    user_data_record_t read_data[USER_DATA_COUNT];
    uint32_t data, addr;

    if(!flash_read((uint32_t*)&read_data, sizeof(read_data)/sizeof(uint32_t)))
        return false;


    for (uint8_t i = 0; i < USER_DATA_COUNT; i++)
    {
        addr = read_data[i].addr;
        data = read_data[i].data;
        data_write(addr,data,false);
    }
    return true;
}



