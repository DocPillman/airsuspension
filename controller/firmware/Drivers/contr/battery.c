#include "battery.h"
#include "data.h"
#include "measuments.h"
#include "help.h"
#include "addr_map.h"
#include "status.h"

float int_to_float(uint32_t b)
{
    return (float)   b * 10000 / 27306666;
}

float convert_set_to_voltage_threshold(void)
{
    float  vol = 0;
    uint32_t set;
    data_read(ADDR_PARAM_VOLT_SYS_THRS,&set,false);
    switch (set) {
        case VALUE_TRSH_VOL_10V :
            vol = 10.0f;
            break;
        case VALUE_TRSH_VOL_11V :
            vol = 11.0f;
            break;
        case VALUE_TRSH_VOL_12V :
            vol = 12.0f;
            break;
        default:
            break;
    }
    return vol;
}


bool battery_is_ok(void)
{
    uint32_t measured_int = 0;
    data_read(ADDR_PARAM_VOLTAGE,&measured_int,false);
    float measured = int_to_float(measured_int);
    float set = convert_set_to_voltage_threshold();
    return measured > set ? true : false;
}

bool battery_ctrl_is_on(void)
{
    uint32_t set;
    data_read(ADDR_PARAM_VOLT_SYS_THRS,&set, false);
    return set > 0 ? true : false;
}


void battery_user_info_update(void)
{
    if(battery_is_ok())
        status_clear(VALUE_STATUS_ERROR_LOW_POWER);
    else
        status_set(VALUE_STATUS_ERROR_LOW_POWER);
}


