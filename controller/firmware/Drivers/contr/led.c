#include "led.h"
#include "help.h"
#include "stm32f1xx_hal.h"
#include "main.h"


void led (ctrl_t ctrl)
{
	if(ctrl == on)
		HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin,GPIO_PIN_SET);
	else if(ctrl == off)
		HAL_GPIO_WritePin(LED_GPIO_Port,LED_Pin,GPIO_PIN_RESET);
}

