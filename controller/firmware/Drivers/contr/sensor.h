#include "help.h"
#ifndef __SENSOR_H_
#define __SENSOR_H_

uint32_t sensor_fl_height(void);
uint32_t sensor_fr_height(void);
uint32_t sensor_rr_height(void);
uint32_t sensor_rl_height(void);

void sensor_user_info_update(void);


#endif
