#include "monitor.h"
#include "stm32f1xx_hal.h"
#include "stdbool.h"
#include "addr_map.h"
#include "data.h"
#include <stdio.h>
#include <string.h>


//printf("\033[2J"); /* Clear the entire screen. printf("\e[1;1H\e[2J");*/ 
//printf("\033[0;0f"); /* Move cursor to the top left hand corner 

typedef enum
{
    value,
    value_list,
    status,
    byte_fileld,
    int_to_float
    
}type_value_t;

typedef struct{
    type_value_t type;
    uint32_t value;
    void * data_info;
}value_t;

typedef struct{
    char name[10];
    value_t value;
    uint16_t address;
}param_t;


typedef struct
{
    uint32_t value;
    char text[10];
}value_item_t;


typedef struct
{
    value_item_t * list_of_value;
    uint8_t amount_item;
    
}list_of_value_t;


/************************************************************************************************************************
 *  CALIBRATE
 */

value_item_t list_calibrate_value[] = {
                                        {.value = VALUE_DONT_SAVE_DAMPS, .text = "dont save " },
                                        {.value = VALUE_SAVE_MIN_DAMPS,  .text = "min saved " },
                                        {.value = VALUE_SAVE_MED_DAMPS,  .text = "med saved " },
                                        {.value = VALUE_SAVE_MAX_DAMPS,  .text = "OK        " },
};

list_of_value_t calibrate_param = { .list_of_value = list_calibrate_value, .amount_item = sizeof(list_calibrate_value)/sizeof(value_item_t) };  

/************************************************************************************************************************
 *  POWER 
 */

value_item_t list_battery_ctrl[] = {
                                        {.value = VALUE_TRSH_VOL_DISABLE, .text = "disable   " },
                                        {.value = VALUE_TRSH_VOL_10V,     .text = "10.0V     " },
                                        {.value = VALUE_TRSH_VOL_11V,     .text = "11.0V     " },
                                        {.value = VALUE_TRSH_VOL_12V,     .text = "12.0V     " },

};

list_of_value_t battery_param = { .list_of_value = list_battery_ctrl, .amount_item = sizeof(list_battery_ctrl)/sizeof(value_item_t) }; 

/************************************************************************************************************************
 *  COMPRESSOR 
 */

value_item_t list_compessor_ctrl[] = {
                                        {.value = VALUE_COMP_OFF,         .text = "off       " },
                                        {.value = VALUE_COMP_ON ,         .text = "on        " },


};

list_of_value_t compressor_param = { .list_of_value = list_compessor_ctrl, .amount_item = sizeof(list_compessor_ctrl)/sizeof(value_item_t) }; 


/************************************************************************************************************************
 *  DOOR 
 */

value_item_t list_door_ctrl[] = {
                                        {.value = VALUE_DOOR_CONTR_OFF,   .text = "off       " },
                                        {.value = VALUE_DOOR_CONTR_ON ,   .text = "on        " },


};

list_of_value_t door_param = { .list_of_value = list_door_ctrl, .amount_item = sizeof(list_door_ctrl)/sizeof(value_item_t) }; 


/************************************************************************************************************************
 *  BRAKE 
 */

value_item_t list_braker_ctrl[] = {
                                        {.value = VALUE_STOP_ENABLE   ,   .text = "enable    " },
                                        {.value = VALUE_STOP_DISABLE ,    .text = "disable   " },


};

list_of_value_t breaker_param = { .list_of_value = list_braker_ctrl, .amount_item = sizeof(list_braker_ctrl)/sizeof(value_item_t) }; 

/************************************************************************************************************************/

param_t list_monitoring_param[] = {
    
        { .name = "calibrate ",  .value = { .type = value_list , .value = 0, .data_info = &calibrate_param       }, .address = ADDR_PARAM_SAVE_DAMPS_VALUE },
        { .name = "time on   ",  .value = { .type = value      , .value = 0, .data_info = NULL                   }, .address = ADDR_PARAM_VALVE_TIME_ON    },
        { .name = "time off  ",  .value = { .type = value      , .value = 0, .data_info = NULL                   }, .address = ADDR_PARAM_VALVE_TIME_OFF   },
        { .name = "battery   ",  .value = { .type = value_list , .value = 0, .data_info = &battery_param         }, .address = ADDR_PARAM_VOLT_SYS_THRS    },
        { .name = "compressor",  .value = { .type = value_list , .value = 0, .data_info = &compressor_param      }, .address = ADDR_PARAM_COMPR_CONTROL    },
        { .name = "door      ",  .value = { .type = value_list , .value = 0, .data_info = &door_param            }, .address = ADDR_PARAM_CONTROL_DOOR     },
        { .name = "brake     ",  .value = { .type = value_list , .value = 0, .data_info = &breaker_param         }, .address = ADDR_PARAM_STOP_SIG_CONTR   },  
    };

    
#define LIST_COUNTER  sizeof(list_monitoring_param) / sizeof(param_t)

uint32_t frame_pause = 100;


char * get_string_of_value(list_of_value_t* list,uint32_t readed_value)
{
    for(uint8_t i = 0; i < list->amount_item; i++)
    {
        if(value == list->list_of_value[i].value)
            return list->list_of_value[i].text;
    }
    return "error    ";   
}


void monitor_task(void)
{
    printf("%c[1;1f%c[J", 27, 27);
    for(uint8_t i = 0; i < LIST_COUNTER; i++)
    {   
        printf("%s",list_monitoring_param[i].name);
        uint32_t raw_data;
        uint16_t addr = list_monitoring_param[i].address;
        data_read(addr,&raw_data,false);
        uint8_t value_type = list_monitoring_param[i].value.type;
        switch (value_type)
        {
            case value:        printf(" = %d\n\r",raw_data);break;
            case value_list :  printf(" = %s\n\r",get_string_of_value(list_monitoring_param[i].value.data_info,raw_data));break;
            default:
                break;
        }
    
    }     
    HAL_Delay(frame_pause);
      
}
