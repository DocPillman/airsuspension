#ifndef __POWER_H_
#define __POWER_H_

#include "stdbool.h"

void power_update_state(void);
bool power_is_off(void);

#endif
