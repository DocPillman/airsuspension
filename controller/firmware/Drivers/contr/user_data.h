#ifndef __USER_DATA_H
#define __USER_DATA_H

#include "stdio.h"
#include "stdbool.h"


void user_data_set_default(void);
bool user_data_save_to_flash(void);
bool user_data_read_from_flash(void);



#endif
