#include "status.h"
#include "data.h"




uint32_t status_get(void)
{
    uint32_t  status;
    data_read(ADDR_PARAM_STATUS, &status, false);
    return status;
}


void status_set(uint32_t status_field)
{
    uint32_t  fields;
    data_read(ADDR_PARAM_STATUS, &fields, false);
    data_write(ADDR_PARAM_STATUS,fields|status_field, false);
}

void status_clear(uint32_t status_field)
{
    uint32_t  fields;
    data_read(ADDR_PARAM_STATUS, &fields, false);
    fields &= ~status_field;
    data_write(ADDR_PARAM_STATUS,fields, false);
}

