#include "sensor_pwr.h"
#include "stm32f1xx_hal.h"
#include "main.h"
#include "stdbool.h"
#include "data.h"



void sensor_power_remote(ctrl_t cmd)
{
    if(cmd == on)
        HAL_GPIO_WritePin(SENSOR_EN_GPIO_Port,SENSOR_EN_Pin,GPIO_PIN_SET);
    else if(cmd == off)
        HAL_GPIO_WritePin(SENSOR_EN_GPIO_Port,SENSOR_EN_Pin,GPIO_PIN_RESET);
}



void sensor_power_state_update(void)
{
    int8_t short_circuit = 0;
    for(uint8_t i = 0; i < 100; i++)
    {
        if(HAL_GPIO_ReadPin(OVER_CUR_SENSOR_GPIO_Port,OVER_CUR_SENSOR_Pin) == GPIO_PIN_RESET)
            short_circuit++;
        else
            short_circuit--;
    }
    if(short_circuit >= 70)
        data_write(ADDR_PARAM_IS_SHORT_CIRCUIT_SENSOR,VALUE_PARAM_WAS_SHORT_CIRCUIT,false);
    else
        data_write(ADDR_PARAM_IS_SHORT_CIRCUIT_SENSOR,VALUE_PARAM_SHORT_CIRCUIT_NOT_DETECT,false);
}


bool sensor_power_in_short_circuit(void)
{
    uint32_t state;
    data_read(ADDR_PARAM_IS_SHORT_CIRCUIT_SENSOR,&state,false);
    return state == VALUE_PARAM_WAS_SHORT_CIRCUIT ? true : false ;
}


