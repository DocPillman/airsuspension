#include "stdbool.h"
#ifndef  __AIR_ACCUMULATOR_H_
#define  __AIR_ACCUMULATOR_H_


void air_accumulator_update_state(void);
bool air_accumulator_is_empty(void);

#endif
