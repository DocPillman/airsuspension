#include "help.h"
#ifndef __STATUS_H_
#define __STATUS_H_


void status_clear(uint32_t status_field);
void status_set(uint32_t status_field);
uint32_t status_get(void);


#endif
