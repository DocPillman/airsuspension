#include "valve_cmd.h"
#include "data.h"


valves_t valves;
valve_mode_t valve_mode;



void valves_time_decrease_and_block(valves_t *valves)
{
    if(valves->fl.time > 0)
        valves->fl.time--;
    else
        valves->fl.cmd = block;

    if(valves->fr.time > 0)
        valves->fr.time--;
    else
        valves->fr.cmd = block;

    if(valves->rr.time > 0)
        valves->rr.time--;
    else
        valves->rr.cmd = block;

    if(valves->rl.time > 0)
        valves->rl.time--;
    else
        valves->rl.cmd = block;
}

/*
 *  this function must call every ms
 */
void valve_cmd_tim_handler(void)
{
    if(valve_mode == v_manual)
    {
        valves_set(&valves);
        valves_time_decrease_and_block(&valves);
    }
    if(valve_mode == v_auto)
    {
        valves_set(&valves);
    }
}

void valve_set_mode(valve_mode_t mode)
{
    valves_off();
    valve_mode = mode;
}

valve_mode_t valve_get_mode(void)
{
    return valve_mode;
}

void valve_set_cmd_and_time(valve_ctrl_t *valve_instance, valve_cmd_t cmd, uint16_t time)
{
    valve_instance->cmd = cmd;
    valve_instance->time = time;
}

/*
 * this group of function for manual set valve during ADDR_PARAM_TIME_MANUAL_EX_IN
 * after this time valve was block automaticaly in manual valve_mode
 */
void valve_cmd_manual_fl(valve_cmd_t cmd)
{
    uint32_t time;
    data_read(ADDR_PARAM_TIME_MANUAL_EX_IN,&time,false) ;
    valve_set_cmd_and_time(&valves.fl,cmd,time * 50U);
}

void valve_cmd_manual_fr(valve_cmd_t cmd)
{
    uint32_t time;
    data_read(ADDR_PARAM_TIME_MANUAL_EX_IN, &time, false) ;
    valve_set_cmd_and_time(&valves.fr,cmd,time * 50U);
}

void valve_cmd_manual_rr(valve_cmd_t cmd)
{
    uint32_t time;
    data_read(ADDR_PARAM_TIME_MANUAL_EX_IN,&time,false) ;
    valve_set_cmd_and_time(&valves.rr,cmd,time * 50U);
}

void valve_cmd_manual_rl(valve_cmd_t cmd)
{
    uint32_t time;
    data_read(ADDR_PARAM_TIME_MANUAL_EX_IN, &time, false) ;
    valve_set_cmd_and_time(&valves.rl,cmd,time * 50U);
}

void valve_cmd_manual_r(valve_cmd_t cmd)
{
	valve_cmd_manual_rl(cmd);
	valve_cmd_manual_rr(cmd);
}

void valve_cmd_manual_f(valve_cmd_t cmd)
{
    valve_cmd_manual_fl(cmd);
    valve_cmd_manual_fr(cmd);
}

void valve_cmd_manual_all(valve_cmd_t cmd)
{
	valve_cmd_manual_rl(cmd);
	valve_cmd_manual_rr(cmd);
	valve_cmd_manual_fl(cmd);
    valve_cmd_manual_fr(cmd);
}


/*
 * this group of function for set valve without block your must block in your using
 * in auto valve_mode
 */

void valve_cmd_auto_fl(valve_cmd_t cmd)
{
    valves.fl.cmd = cmd;
}

void valve_cmd_auto_fr(valve_cmd_t cmd)
{
    valves.fr.cmd = cmd;
}

void valve_cmd_auto_rr(valve_cmd_t cmd)
{
    valves.rr.cmd = cmd;
}

void valve_cmd_auto_rl(valve_cmd_t cmd)
{
    valves.rl.cmd = cmd;
}

void valve_cmd_auto_block_all(void)
{
    valves.fl.cmd = block;
    valves.fr.cmd = block;
    valves.rr.cmd = block;
    valves.rl.cmd = block;
}
