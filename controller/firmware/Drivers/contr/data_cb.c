#include "data_cb.h"
#include "proto.h"
#include "valve_cmd.h"
#include "calibrate.h"
#include "data.h"
#include "clearance.h"
#include "user_data.h"
#include "status.h"
#include "led.h"
#include "stm32f1xx_hal.h"
#include "sensor_pwr.h"

void cmd_read_or_write_param(payload_t *payload)
{
    bool ok;
    uint32_t data;


    if(payload->type_command == CMD_TYPE_WRITE)
        ok = data_write(payload->address,payload->data, true);
    else if(payload->type_command == CMD_TYPE_READ)
        ok = data_read(payload->address,&data, true);
    else
        ok = false;

    if(ok && payload->type_command == CMD_TYPE_READ)
        proto_response(CMD_TYPE_READ_OK,payload->address,data);
    else if(ok && payload->type_command == CMD_TYPE_WRITE)
        proto_response(CMD_TYPE_WRITE_OK,payload->address,payload->data);
    else
        proto_error_report(payload,VALUE_STATUS_ERROR_COMMUNICATION);
}


void cmd_valve_control(payload_t *payload)
{
    valve_mode_t valve_mode = valve_get_mode();
    valve_cmd_t valve_cmd = (valve_cmd_t)payload->data;


    if((payload->data == ex || payload->data == in) && payload->type_command == CMD_TYPE_WRITE)
    {
        if(valve_mode != v_manual)
            valve_set_mode(v_manual);

        switch (payload->address)
        {
            case ADDR_CONTR_FL_VALVE: valve_cmd_manual_fl(valve_cmd); break;
            case ADDR_CONTR_FR_VALVE: valve_cmd_manual_fr(valve_cmd); break;
            case ADDR_CONTR_RR_VALVE: valve_cmd_manual_rr(valve_cmd); break;
            case ADDR_CONTR_RL_VALVE: valve_cmd_manual_rl(valve_cmd); break;
            case ADDR_CONTR_F_VALVE: valve_cmd_manual_f(valve_cmd); break;
            case ADDR_CONTR_R_VALVE: valve_cmd_manual_r(valve_cmd); break;
            case ADDR_CONTR_ALL_VALVE: valve_cmd_manual_all(valve_cmd); break;
        }
        proto_response(CMD_TYPE_WRITE_OK,payload->address,payload->data);
        return;
    }
    proto_error_report(payload,VALUE_STATUS_ERROR_COMMUNICATION);
}


void cmd_do_clearance_or_manual_set(payload_t *payload)
{

    uint32_t depth = 0;
    clearance_mode_t mode;
    valve_cmd_t cmd = block;

    if(payload->data == VALUE_CTRL_SET_OR_UP)
        cmd = (valve_cmd_t) ACTION_VALVE_EX;
    else if(payload->data == VALUE_CTRL_DOWN)
        cmd = (valve_cmd_t) ACTION_VALVE_IN;

    if(payload->type_command == CMD_TYPE_WRITE)
    {
        mode = clearance_get_mode();
        if(mode == auto_mode)
        {
            if(calibrate_is_done())
            {
                clearance_set();
            }else
                {
                   proto_error_report(payload,VALUE_STATUS_ERROR_CALIBRATE);
                   return;
                }

        }
        else if (mode == manual_mode)
        {
            if(valve_get_mode() != v_manual)
                valve_set_mode(v_manual);

            data_read(ADDR_PARAM_DEPTH_MODE,&depth,false);
            switch (depth)
            {
                case VALUE_DEPTH_MALL: valve_cmd_manual_all(cmd); break;
                case VALUE_DEPTH_MF: valve_cmd_manual_f(cmd); break;
                case VALUE_DEPTH_MR: valve_cmd_manual_r(cmd); break;
                default:
                    break;
            }
            status_clear(VALUE_STATUS_ERROR_CALIBRATE);
        }
        proto_response(CMD_TYPE_WRITE_OK,payload->address,payload->data);
        return;
    }
    proto_error_report(payload,VALUE_STATUS_ERROR_COMMUNICATION);
}


void cmd_calibrate(payload_t* payload)
{

    if(sensor_power_in_short_circuit())
    {
        status_set(VALUE_STATUS_ERROR_SHORT_CIRCUIT_SENS);
        proto_error_report(payload,VALUE_STATUS_ERROR_SHORT_CIRCUIT_SENS);
        return;
    }

    if(payload->type_command == CMD_TYPE_READ)
        cmd_read_or_write_param(payload);
    else if(payload->type_command == CMD_TYPE_WRITE)
    {
        if(payload->data == VALUE_SAVE_MIN_DAMPS)
            calibrate_save_measure_to_min();
        else if(payload->data == VALUE_SAVE_MED_DAMPS)
            calibrate_save_measure_to_med();
        else if(payload->data == VALUE_SAVE_MAX_DAMPS)
        {
            calibrate_save_measure_to_max();
            if(calibrate_saved_data_is_ok())
            {
                calibrate_determining_directions();
                status_clear(VALUE_STATUS_ERROR_CALIBRATE);
            }else {
                calibrate_clear();
                proto_error_report(payload, VALUE_STATUS_ERROR_CALIBRATE);
                return;
            }
        }
        cmd_read_or_write_param(payload);
    }
}

void cmd_save_or_reset_user_data(payload_t *payload)
{
    bool success = false;
    if(payload->type_command == CMD_TYPE_WRITE &&
      (payload->data == VALUE_SAVE_SETTING || payload->data == VALUE_RESET_SETTING))
    {
        if(payload->data == VALUE_RESET_SETTING)
            user_data_set_default();

        success = user_data_save_to_flash();
    }
    if(!success)
        proto_error_report(payload,VALUE_STATUS_ERROR_SAVE_USER_DATA);
    else
        proto_response(CMD_TYPE_WRITE_OK,payload->address,payload->data);

        HAL_Delay(300);
        NVIC_SystemReset();
}
