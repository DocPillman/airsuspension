#include "help.h"
#ifndef __VALVE_H_
#define __VALVE_H_

typedef enum
{
    in,
    ex,
    block
}valve_cmd_t;

typedef struct
{
    valve_cmd_t cmd;
    uint16_t time;

}valve_ctrl_t;

typedef struct
{
    valve_ctrl_t fr;
    valve_ctrl_t fl;
    valve_ctrl_t rr;
    valve_ctrl_t rl;

}valves_t;

void valve_fr_set(valve_cmd_t cmd);
void valve_fl_set(valve_cmd_t cmd);
void valve_rr_set(valve_cmd_t cmd);
void valve_rl_set(valve_cmd_t cmd);

void valves_set(valves_t *valves);
void valves_off(void);



#endif
