#include "timeout.h"
#include "stdio.h"
#include "stm32f1xx_hal.h"
#include "stdbool.h"


void timeout_set(timeout_t *instance, uint32_t timeout_arg)
{
    instance->start_time = HAL_GetTick();
    instance->timeout = timeout_arg;
}

bool is_timeout(timeout_t *instance)
{
    return  instance->timeout > 0 && (HAL_GetTick() - instance->start_time) > instance->timeout;
}


bool timeout_is_enabled(timeout_t * instance)
{
    return instance->timeout > 0 ? true : false ;
}

