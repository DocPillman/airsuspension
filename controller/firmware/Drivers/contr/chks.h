#ifndef __CHKS_H_
#define __CHKS_H_

#include "pdata.h"

uint8_t chks_is_ok (uint8_t* packet);
uint8_t chks(uint8_t* packet);

#endif
