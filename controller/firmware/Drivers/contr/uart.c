#include "uart.h"
#include "stm32f1xx_hal.h"
#include "string.h"

extern UART_HandleTypeDef huart2;


com_t tx = {.buffer = { 0 }, .counter = 0, .state = idle};
com_t rx = {.buffer = { 0 }, .counter = 0, .state = idle};


void uart_init(void)
{
    memset(&tx.buffer,0,PACKET_SIZE);
    memset(&rx.buffer,0,PACKET_SIZE);
    tx.counter = rx.counter = 0; 
    tx.state = rx.state = idle;

    HAL_UART_Receive_IT(&huart2, &rx.buffer[rx.counter++],1);
}


void uart_rx_isr_handler(void)
{
    packet_t * packet;
    rx.state = busy;
    if(rx.counter == PACKET_SIZE)
    {
        rx.counter = 0;

        packet = (packet_t *) rx.buffer;

        if(chks_is_ok(rx.buffer) && (packet->id == MASTER_ID))
        {
            proto_request(&packet->payload);
            rx.state = idle;

        } else {
            if (!chks_is_ok(rx.buffer))
            {
                rx.state = error;
                __HAL_UART_DISABLE_IT(&huart2,UART_IT_RXNE);
            }
        }
    }
    if(HAL_UART_Receive_IT(&huart2, &rx.buffer[rx.counter++], 1) != HAL_OK)
    {
        rx.state = error;
    }

}


void uart_tx_isr_handler(void)
{
    if(tx.counter < PACKET_SIZE)
        HAL_UART_Transmit_IT(&huart2, &tx.buffer[tx.counter++], 1);
    else
        {
            tx.state = idle;
            __HAL_UART_DISABLE_IT(&huart2,UART_IT_TXE);
            
        }
}



void uart_transmit(payload_t * payload)
{

    packet_t packet;
    if((tx.state != idle) || (payload == NULL))
        return;

    packet.id = SLAVE_ID;
    memcpy(&packet.payload,payload,PAYLOAD_SIZE);
    packet.chks = chks((uint8_t*)&packet);
    memcpy(tx.buffer,&packet,PACKET_SIZE);
    tx.counter = 0;
    tx.state = busy;                             
    HAL_UART_Transmit_IT(&huart2, &tx.buffer[tx.counter++], 1);
}


uint8_t uart_get_tx_state(void)
{
    return tx.state;
}

uint8_t uart_get_rx_state(void)
{
    return rx.state;
}

