#include "air_accumulator.h"
#include "stm32f1xx_hal.h"
#include "addr_map.h"
#include "main.h"
#include "data.h"
#include "help.h"
#include "stdbool.h"

void air_accumulator_update_state(void)
{
    uint8_t sensor_type = HAL_GPIO_ReadPin(PRESSURE_CONFIG_GPIO_Port,PRESSURE_CONFIG_Pin);
    int8_t pressure_sensor_state  = 0;
    for(uint8_t i = 0; i < 100; i++)
    {
        if(HAL_GPIO_ReadPin(PRESSURE_GPIO_Port,PRESSURE_Pin))
            pressure_sensor_state++;
        else
            pressure_sensor_state--;
    }
    if((sensor_type == 1 && pressure_sensor_state < 70) ||
       (sensor_type == 0 && pressure_sensor_state >= 70))
        data_write(ADDR_PARAM_IS_COMPR_EMPTY,1,false);
    else
        data_write(ADDR_PARAM_IS_COMPR_EMPTY,0,false);
}


bool air_accumulator_is_empty(void)
{
    uint32_t val;
    data_read(ADDR_PARAM_IS_COMPR_EMPTY,&val,false);
    return  val == 1 ? true : false;
}
