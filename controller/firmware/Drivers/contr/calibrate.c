#include "calibrate.h"
#include "sensor.h"
#include "addr_map.h"
#include "data.h"



void calibrate_save_measure_to_max(void)
{
    uint32_t fl,fr,rr,rl;
    
    fl = sensor_fl_height();
    fr = sensor_fr_height();
    rr = sensor_rr_height();
    rl = sensor_rl_height();
    
    data_write(ADDR_PARAM_MAX_SET_FL, fl, false);
    data_write(ADDR_PARAM_MAX_SET_FR, fr, false);
    data_write(ADDR_PARAM_MAX_SET_RR, rr, false);
    data_write(ADDR_PARAM_MAX_SET_RL, rl, false);
}

void calibrate_save_measure_to_med(void)
{
    uint32_t fl,fr,rr,rl;

    fl = sensor_fl_height();
    fr = sensor_fr_height();
    rr = sensor_rr_height();
    rl = sensor_rl_height();

    data_write(ADDR_PARAM_MED_SET_FL, fl, false);
    data_write(ADDR_PARAM_MED_SET_FR, fr, false);
    data_write(ADDR_PARAM_MED_SET_RR, rr, false);
    data_write(ADDR_PARAM_MED_SET_RL, rl, false);
}

void calibrate_save_measure_to_min(void)
{
    uint32_t fl,fr,rr,rl;

    fl = sensor_fl_height();
    fr = sensor_fr_height();
    rr = sensor_rr_height();
    rl = sensor_rl_height();

    data_write(ADDR_PARAM_MIN_SET_FL, fl, false);
    data_write(ADDR_PARAM_MIN_SET_FR, fr, false);
    data_write(ADDR_PARAM_MIN_SET_RR, rr, false);
    data_write(ADDR_PARAM_MIN_SET_RL, rl, false);
}

bool calibrate_is_done(void)
{
    uint32_t data;
    data_read(ADDR_PARAM_SAVE_DAMPS_VALUE, &data, false);
    return data == 3 ? true : false;
}


void calibrate_clear(void)
{
    data_write(ADDR_PARAM_SAVE_DAMPS_VALUE,VALUE_DONT_SAVE_DAMPS,false);
}


bool calibrate_saved_data_is_ok(void)
{
    uint8_t state = 0;
    int32_t  dif = 0;
    uint32_t max,min;
   
    data_read(ADDR_PARAM_MAX_SET_FL, &max, false);
    data_read(ADDR_PARAM_MIN_SET_FL, &min, false);
    dif =  max - min;
    if((dif > 0 && dif < DEPTH_HISTR)||
       (dif < 0 &&  -dif < DEPTH_HISTR))
        state++;

    data_read(ADDR_PARAM_MAX_SET_FR, &max, false);
    data_read(ADDR_PARAM_MIN_SET_FR, &min, false);
    dif =  max - min;
    if((dif > 0 && dif < DEPTH_HISTR)||
       (dif < 0  && -dif < DEPTH_HISTR))
        state++;

    data_read(ADDR_PARAM_MAX_SET_RR, &max, false);
    data_read(ADDR_PARAM_MIN_SET_RR, &min, false);
    dif =  max - min;
    if((dif > 0 && dif < DEPTH_HISTR)||
       (dif < 0  && -dif < DEPTH_HISTR))
        state++;

    data_read(ADDR_PARAM_MAX_SET_RL, &max, false);
    data_read(ADDR_PARAM_MIN_SET_RL, &min, false);
    dif =  max - min;
    if((dif > 0 && dif < DEPTH_HISTR)||
       (dif < 0  && -dif < DEPTH_HISTR))
        state++;

    return state == 0 ? true : false;
}



void calibrate_determining_directions(void)
{

    int32_t dif;
    uint32_t max,min;

    data_read(ADDR_PARAM_MAX_SET_FL, &max, false);
    data_read(ADDR_PARAM_MIN_SET_FL, &min, false);
    dif =  max - min;
    if(dif > 0)
        data_write(ADDR_PARAM_DIRCHG_FL,VALUE_DIRCHG_MAXUP_MINDOWN,false);
    else
        data_write(ADDR_PARAM_DIRCHG_FL,VALUE_DIRCHG_MAXDOWN_MINUP,false);

    data_read(ADDR_PARAM_MAX_SET_FR, &max, false);
    data_read(ADDR_PARAM_MIN_SET_FR, &min, false);
    dif =  max - min;
    if(dif > 0)
        data_write(ADDR_PARAM_DIRCHG_FR,VALUE_DIRCHG_MAXUP_MINDOWN,false);
    else
        data_write(ADDR_PARAM_DIRCHG_FR,VALUE_DIRCHG_MAXDOWN_MINUP,false);

    data_read(ADDR_PARAM_MAX_SET_RR, &max, false);
    data_read(ADDR_PARAM_MIN_SET_RR, &min, false);
    dif =  max - min;
    if(dif > 0)
        data_write(ADDR_PARAM_DIRCHG_RR,VALUE_DIRCHG_MAXUP_MINDOWN,false);
    else
        data_write(ADDR_PARAM_DIRCHG_RR,VALUE_DIRCHG_MAXDOWN_MINUP,false);

    data_read(ADDR_PARAM_MAX_SET_RL, &max, false);
    data_read(ADDR_PARAM_MIN_SET_RL, &min, false);
    dif =  max - min;
    if(dif > 0)
        data_write(ADDR_PARAM_DIRCHG_RL,VALUE_DIRCHG_MAXUP_MINDOWN,false);
    else
        data_write(ADDR_PARAM_DIRCHG_RL,VALUE_DIRCHG_MAXDOWN_MINUP,false);
}
