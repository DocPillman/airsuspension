#ifndef __SENSOR_PWR_H_
#define __SENSOR_PWR_H_

#include "help.h"
#include "stdbool.h"


bool sensor_power_in_short_circuit(void);
void sensor_power_state_update(void);
void sensor_power_remote(ctrl_t cmd);


#endif