#include "measuments.h"
#include "stm32f1xx_hal.h"
#include "main.h"
#include "help.h"
#include "sensor_pwr.h"
#include "data.h"
#include "string.h"
#include "sensor.h"


#define VDD_APPLI                      ((uint32_t) 3290)
#define RANGE_12BITS                   ((uint32_t) 4095) 
#define COMPUTATION_DIGITAL_12BITS_TO_VOLTAGE(ADC_DATA)  ((ADC_DATA) * VDD_APPLI / RANGE_12BITS)
	

extern ADC_HandleTypeDef hadc1;
extern DMA_HandleTypeDef hdma_adc1;

__IO uint16_t  adc_dma_array[6];
uint32_t median_value[6];
uint32_t  counter = 0;

uint32_t float_to_int(float a)
{
	return (uint32_t)a * 27306666 / 10000;
}


void measument_start(void)
{

    sensor_power_remote(on);
	if (HAL_ADCEx_Calibration_Start(&hadc1) != HAL_OK)
		Error_Handler();
	if( HAL_ADC_Start_DMA(&hadc1,(uint32_t *)adc_dma_array,6) != HAL_OK)
		Error_Handler();
	if( HAL_ADC_Start(&hadc1) != HAL_OK)
		Error_Handler();
}


void measument_stop(void)
{

	HAL_ADC_Stop(&hadc1);
	HAL_ADC_Stop_DMA(&hadc1);
	HAL_ADC_MspDeInit(&hadc1);

}

void measument_adc_cmplt_callback(void)
{
    counter++;
	float bat_voltage;
	for(uint8_t i = 0; i < 6; i++)
		median_value[i] += adc_dma_array[i];

	if(counter == 32)
	{
		data_write(ADDR_PARAM_MEASURE_FL, median_value[3] >> 5,false);
		data_write(ADDR_PARAM_MEASURE_FR, median_value[0] >> 5,false);
		data_write(ADDR_PARAM_MEASURE_RL, median_value[4] >> 5,false);
		data_write(ADDR_PARAM_MEASURE_RR, median_value[1] >> 5,false);
        data_write(ADDR_PARAM_INTERNAL_TEMP, median_value[6] >> 5, false);
		bat_voltage = (float)COMPUTATION_DIGITAL_12BITS_TO_VOLTAGE(median_value[2] >> 5) * 1000.0f;
		data_write(ADDR_PARAM_VOLTAGE,float_to_int(bat_voltage),false);
		memset(median_value,0, sizeof(median_value));
		counter=0;
	}

}


void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* AdcHandle)
{
	measument_adc_cmplt_callback();
}







