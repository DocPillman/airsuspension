#include "door.h"
#include "help.h"
#include "stm32f1xx_hal.h"
#include "main.h"
#include "data.h"

#define  TIME_LAMP_START 1000
#define  TIME_DOOR_SENSOR_RISING_FAILING 100


void door_update_state(void)
{
    uint8_t connection_type = HAL_GPIO_ReadPin(DOOROPEN_CONFIG_GPIO_Port,DOOROPEN_CONFIG_Pin);
    int16_t door_state = 0;
    uint32_t rise_fail_count;

    if(connection_type == 0)
        rise_fail_count = TIME_LAMP_START;
    else
        rise_fail_count = TIME_DOOR_SENSOR_RISING_FAILING;

    for(uint32_t i = 0; i < rise_fail_count; i++)
    {
        if(HAL_GPIO_ReadPin(DOOR_OPEN_GPIO_Port,DOOR_OPEN_Pin))
            door_state++;
        else
            door_state--;
    }
    if(door_state >= rise_fail_count / 100U * 70U)
        data_write(ADDR_PARAM_IS_DOOR_OPEN,1,false);
    else
        data_write(ADDR_PARAM_IS_DOOR_OPEN,0,false);
}

bool door_is_open(void)
{
    uint32_t val;
    data_read(ADDR_PARAM_IS_DOOR_OPEN,&val,false);
    return val == 1 ? true : false;
}

bool door_ctrl_is_on(void)
{
    uint32_t val;
    data_read(ADDR_PARAM_CONTROL_DOOR,&val,false);
    return val ==  VALUE_DOOR_CONTR_ON ? true : false ;
}
