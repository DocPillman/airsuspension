# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "ASM"
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_ASM
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/startup/startup_stm32l462xx.s" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/startup/startup_stm32l462xx.s.obj"
  )
set(CMAKE_ASM_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_ASM
  "ARM_MATH_CM4"
  "ARM_MATH_MATRIX_CHECK"
  "ARM_MATH_ROUNDING"
  "STM32L462xx"
  "USE_HAL_DRIVER"
  "__FPU_PRESENT=1"
  "__packed=__attribute__((__packed__))"
  "__weak=__attribute__((weak))"
  )

# The include file search paths:
set(CMAKE_ASM_TARGET_INCLUDE_PATH
  "../Core/Inc"
  "../USB_DEVICE/App"
  "../USB_DEVICE/Target"
  "../Drivers/STM32L4xx_HAL_Driver/Inc"
  "../Drivers/STM32L4xx_HAL_Driver/Inc/Legacy"
  "../Middlewares/Third_Party/FreeRTOS/Source/include"
  "../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS"
  "../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F"
  "../Middlewares/ST/STM32_USB_Device_Library/Core/Inc"
  "../Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc"
  "../Drivers/CMSIS/Device/ST/STM32L4xx/Include"
  "../Drivers/CMSIS/Include"
  )
set(CMAKE_DEPENDS_CHECK_C
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Core/Src/adc.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Core/Src/adc.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Core/Src/can.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Core/Src/can.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Core/Src/dma.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Core/Src/dma.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Core/Src/freertos.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Core/Src/freertos.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Core/Src/gpio.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Core/Src/gpio.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Core/Src/main.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Core/Src/main.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Core/Src/stm32l4xx_hal_msp.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Core/Src/stm32l4xx_hal_msp.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Core/Src/stm32l4xx_hal_timebase_tim.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Core/Src/stm32l4xx_hal_timebase_tim.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Core/Src/stm32l4xx_it.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Core/Src/stm32l4xx_it.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Core/Src/syscalls.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Core/Src/syscalls.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Core/Src/system_stm32l4xx.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Core/Src/system_stm32l4xx.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Core/Src/tim.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Core/Src/tim.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_adc.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_adc.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_adc_ex.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_adc_ex.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_can.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_can.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_cortex.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_cortex.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma_ex.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_dma_ex.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_exti.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_exti.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ex.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ex.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ramfunc.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_flash_ramfunc.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_gpio.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_gpio.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c_ex.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_i2c_ex.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pcd.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pcd.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pcd_ex.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pcd_ex.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr_ex.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_pwr_ex.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc_ex.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_rcc_ex.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim_ex.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_hal_tim_ex.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_ll_usb.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Drivers/STM32L4xx_HAL_Driver/Src/stm32l4xx_ll_usb.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Src/usbd_cdc.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Src/usbd_cdc.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Middlewares/ST/STM32_USB_Device_Library/Core/Src/usbd_core.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Middlewares/ST/STM32_USB_Device_Library/Core/Src/usbd_core.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Middlewares/ST/STM32_USB_Device_Library/Core/Src/usbd_ctlreq.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Middlewares/ST/STM32_USB_Device_Library/Core/Src/usbd_ctlreq.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Middlewares/ST/STM32_USB_Device_Library/Core/Src/usbd_ioreq.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Middlewares/ST/STM32_USB_Device_Library/Core/Src/usbd_ioreq.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS/cmsis_os.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS/cmsis_os.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Middlewares/Third_Party/FreeRTOS/Source/croutine.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/croutine.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Middlewares/Third_Party/FreeRTOS/Source/event_groups.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/event_groups.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Middlewares/Third_Party/FreeRTOS/Source/list.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/list.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F/port.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F/port.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Middlewares/Third_Party/FreeRTOS/Source/portable/MemMang/heap_4.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/portable/MemMang/heap_4.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Middlewares/Third_Party/FreeRTOS/Source/queue.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/queue.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Middlewares/Third_Party/FreeRTOS/Source/stream_buffer.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/stream_buffer.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Middlewares/Third_Party/FreeRTOS/Source/tasks.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/tasks.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/Middlewares/Third_Party/FreeRTOS/Source/timers.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/Middlewares/Third_Party/FreeRTOS/Source/timers.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/USB_DEVICE/App/usb_device.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/USB_DEVICE/App/usb_device.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/USB_DEVICE/App/usbd_cdc_if.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/USB_DEVICE/App/usbd_cdc_if.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/USB_DEVICE/App/usbd_desc.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/USB_DEVICE/App/usbd_desc.c.obj"
  "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/USB_DEVICE/Target/usbd_conf.c" "/Users/docpillman/projects/my_projects/airsuspension/controller/firmware/cmake-build-debug/CMakeFiles/firmware.elf.dir/USB_DEVICE/Target/usbd_conf.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "ARM_MATH_CM4"
  "ARM_MATH_MATRIX_CHECK"
  "ARM_MATH_ROUNDING"
  "STM32L462xx"
  "USE_HAL_DRIVER"
  "__FPU_PRESENT=1"
  "__packed=__attribute__((__packed__))"
  "__weak=__attribute__((weak))"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../Core/Inc"
  "../USB_DEVICE/App"
  "../USB_DEVICE/Target"
  "../Drivers/STM32L4xx_HAL_Driver/Inc"
  "../Drivers/STM32L4xx_HAL_Driver/Inc/Legacy"
  "../Middlewares/Third_Party/FreeRTOS/Source/include"
  "../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS"
  "../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F"
  "../Middlewares/ST/STM32_USB_Device_Library/Core/Inc"
  "../Middlewares/ST/STM32_USB_Device_Library/Class/CDC/Inc"
  "../Drivers/CMSIS/Device/ST/STM32L4xx/Include"
  "../Drivers/CMSIS/Include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
