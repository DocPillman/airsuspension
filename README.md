

# AirSuspension controller Kit


This project universal air suspension 

This kit inculde :


* [controller](controller/) - solutons for control air accumulator, clearance, compressor.   
* [display controller](display controller/) - HMI for remote air suspension.    
* [sensor](sensor/) - sensor which measument level of clearance.   
* [valve](valve) -  air valve.        
* [wifi display controller](wifi display controller/) - HMI with wifi module.   

![view](controller/doc/photo/mainview.png) 
![view](display controller/doc/photo/top_view.png)
![view][valve/doc/photo/main_view.png)

