#ifndef _CLEARANCE_INCLUDED_
#define _CLEARANCE_INCLUDED_

#include "addr_map.h"
#include "help.h"
#include <stdbool.h>
#include "data_cmd.h"



typedef enum
{
    manual_mode,
    auto_mode,
}mode_type_t;


mode_type_t clearance_get_mode(void);

void clearance_switch_auto_manual(void);
void clearance_auto_up(void);
void clearance_auto_down(void);
void clearance_switch_manual_param(void);
void clearance_set_or_manual_up(void);
void clearance_manual_down(void);




#endif