#include "button.h"


uint16_t time_press;
bool push_status;
bool menu_pressed = 0;
button_t last_push;



void button_handler(void)
{
    if(BTN_DOWN_GPIO == 0x00)
        last_push = btn_down;
    else if(BTN_UP_GPIO == 0x00)
        last_push = btn_up;
    else if(BTN_RIGHT_GPIO == 0x00)
        last_push = btn_right;
    else if(BTN_MENU_GPIO == 0x00)
    {
        menu_pressed = true;
        time_press  = tim_get_tic();
        push_status = false;
        return;
    }
    push_status = true;
}

void button_timer_handler (void)
{
    if(menu_pressed)
    {
        if(BTN_MENU_GPIO == 0x00)
        {
            if(tim_get_time_mc(time_press) >= LONG_PRESS_TIME_TIC)
            {
                menu_pressed = false;
                last_push = btn_long_menu;
                push_status = true;
            }
        } else{
            menu_pressed = false;
            push_status = true;
            last_push = btn_menu;
        }

    }
}



bool button_is_pushed()
{
    return push_status;
}


button_t button_last_push(void)
{
    return  last_push;
}

void button_clear_info(void)
{
    push_status = false;
}