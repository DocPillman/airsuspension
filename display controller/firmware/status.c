#include "status.h"
#include "stdbool.h"
#include "data_cmd.h"
#include "stdint.h"

uint32_t error_list[] = {
        VALUE_STATUS_ERROR_SHORT_CUR_SENS,
        VALUE_STATUS_ERROR_COMMUNICATION,
        VALUE_STATUS_ERROR_CALIBRATE,
        VALUE_STATUS_ERROR_SAVE_USER_DATA,
        VALUE_STATUS_ERROR_LOW_POWER,
        VALUE_STATUS_COMPRESSOR_TIMEOUT,
        VALUE_STATUS_CLEARANCE_SET_TIMEOUT,
};

#define LIST_ERROR_SIZE  sizeof(error_list)/sizeof(error_list[0])

uint8_t  error_counter = 0;

uint32_t status_get_next_error(void)
{

    uint32_t status = get_data(ADDR_PARAM_STATUS);
    uint32_t checking_error = error_list[error_counter];;
    error_counter++;               
    if((status & checking_error) > 0)
         return checking_error;
           
    if(error_counter == LIST_ERROR_SIZE)
        error_counter = 0;
        
    return 0;
}
