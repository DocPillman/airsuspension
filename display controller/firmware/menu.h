#ifndef _MENU_INCLUDED_
#define _MENU_INCLUDED_


#include "help.h"
#include "lcd.h"
#include "addr_map.h"
#include <mega8.h>
#include <alcd.h>
#include <stdio.h>
#include <string.h>
#include "proto.h"
#include "debug.h"

#define DECLARE_ITEM( name )	extern  flash menu_item_t name
#define MAKE_ITEM			 flash menu_item_t

#define NOTUSED 0xFF

#define INCR_DECR_WHEN_SELECTED 0xFE
#define SHOW_CHOOSEN_VALUE 0xFD
#define NOT_SELECT 0x00

#define  FRAME_ROW 16
#define  FRAME_COLUMN 2
#define  FRAME_SIZE FRAME_ROW * FRAME_COLUMN

#define  SYMBOL_CURSOR '>' 
#define  SYMBOL_SET    'o' 

#define  ITEM_TEXT_SIZE 14

typedef struct menu_item_t
{
    flash struct menu_item_t *next;
    flash struct menu_item_t *prev;
    flash struct menu_item_t *parent;
    flash struct menu_item_t *child;
    void (*select_callback)(void);
    void (*long_press_callback)(void);
    void (*incr_decr_callback)(void);
    uint8_t item_value;
    flash char *text;  
    
}menu_item_t;

typedef struct
{

	uint8_t  cursor_ptr;
	uint8_t  readed_data;
	uint8_t  counter;
	MAKE_ITEM *item ;
	uint8_t select_state;
	char frame[FRAME_SIZE];
	flash char *message_box;
	bool exit;

}menu_t;

void menu_init(void);
void menu_exit(void);
bool menu_is_off(void);

void menu_navigate(uint8_t btn);

void menu_enter_data(uint32_t data);
void menu_enter_setect_state(uint8_t state);
uint32_t menu_get_item_value(void);

void menu_set_message_box(flash char* messadge);
flash char * menu_get_message_box(void);
void menu_clear_message_box(void);

void menu_update(void);
char* menu_get_frame(void);


#endif
