#ifndef _LCD_INCLUDED_
#define _LCD_INCLUDED_


#include <mega8.h>
#include <alcd.h>
#include <stdio.h>
#include <string.h>
#include <delay.h>

#include "menu.h"
#include "data.h"
#include "addr_map.h"
#include "help.h"



#define LCD_ROW 16
#define LCD_COLUMN 2
#define LCD_SIZE LCD_ROW * LCD_COLUMN

#define LCD_BACKLIGHT_PORT  PORTB
#define LCD_BACKLIGHT_CTRL_PIN  PORTB2

void lcd_show(char *frame);
void lcd_backlight(ctrl_t ctrl);
void lcd_off(void);

#endif