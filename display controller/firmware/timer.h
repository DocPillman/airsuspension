#ifndef _TIMER_INCLUDED_
#define _TIMER_INCLUDED_


#include "help.h"
#include <mega8.h>

void tim_init(void);
void tim_handler(void);

uint32_t tim_get_time_mc(uint32_t prev);
uint16_t tim_get_tic(void);



#endif