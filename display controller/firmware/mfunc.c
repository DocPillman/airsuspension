#include "mfunc.h"



flash char message_box_save_min[] = {"  min position       saved      "};
flash char message_box_save_med[] = {"  med position       saved      "};
flash char message_box_save_max[] = {"  max position       saved      "};


void main_3_check(void)
{
    uint32_t data = get_data(ADDR_PARAM_VOLT_SYS_THRS);
    menu_enter_setect_state(SHOW_CHOOSEN_VALUE);
    menu_enter_data(data);
}

void main_4_check(void)
{
    uint32_t data = get_data(ADDR_PARAM_COMPR_CONTROL);
    menu_enter_setect_state(SHOW_CHOOSEN_VALUE);
    menu_enter_data(data);

}

void main_5_check(void)
{
    uint32_t data = get_data(ADDR_PARAM_CONTROL_DOOR);
    menu_enter_setect_state(SHOW_CHOOSEN_VALUE);
    menu_enter_data(data);
}

void main_6_check(void)
{
    uint32_t data = get_data(ADDR_PARAM_STOP_SIG_CONTR);
    menu_enter_setect_state(SHOW_CHOOSEN_VALUE);
    menu_enter_data(data);
}


void main1_x_long_press(void)
{
    menu_exit();
}


void chlid_1x_encr_decr(void)
{
    uint16_t addr = menu_get_item_value();
    button_t button = button_last_push();
    uint32_t data  = 0;

    if(button == btn_down)
        data = ACTION_VALVE_EX;
    else if(button == btn_up)
        data = ACTION_VALVE_IN;

    set_data(addr,data);
}




void child_1x_long_press(void)
{

    
    uint32_t data = get_data(ADDR_PARAM_SAVE_DAMPS_VALUE);
    switch (data)
    {
        case VALUE_DONT_SAVE_DAMPS:
            set_data(ADDR_PARAM_SAVE_DAMPS_VALUE,VALUE_SAVE_MIN_DAMPS);
            menu_set_message_box(message_box_save_min);
            break;
        case VALUE_SAVE_MIN_DAMPS:
            set_data(ADDR_PARAM_SAVE_DAMPS_VALUE,VALUE_SAVE_MED_DAMPS);
            menu_set_message_box(message_box_save_med);
            break;
        case VALUE_SAVE_MED_DAMPS:
            set_data(ADDR_PARAM_SAVE_DAMPS_VALUE,VALUE_SAVE_MAX_DAMPS);
            menu_set_message_box(message_box_save_max);
            break;
        case VALUE_SAVE_MAX_DAMPS:
            return;
    }
}



void chlid_1x_select(void)
{
    uint16_t data = menu_get_item_value();
    menu_enter_setect_state(INCR_DECR_WHEN_SELECTED);
    menu_enter_data(data);
}





void child_21_check(void)
{
    uint32_t data = get_data(ADDR_PARAM_VALVE_TIME_ON);
    menu_enter_setect_state(SHOW_CHOOSEN_VALUE);
    menu_enter_data(data);
}

void child_22_check(void)
{
    uint32_t data = get_data(ADDR_PARAM_VALVE_TIME_OFF);
    menu_enter_setect_state(SHOW_CHOOSEN_VALUE);
    menu_enter_data(data);
}

void  child_21x_select(void)
{
    uint32_t value = menu_get_item_value();
    set_data(ADDR_PARAM_VALVE_TIME_ON,value);
    value = data(ADDR_PARAM_VALVE_TIME_ON);
    menu_enter_data(value);
}

void child_22x_select(void)
{
    uint32_t value = menu_get_item_value();
    set_data(ADDR_PARAM_VALVE_TIME_OFF,value);
    value = data(ADDR_PARAM_VALVE_TIME_OFF);
    menu_enter_data(value);
}


void  child_3x_select(void)
{
    uint32_t value = menu_get_item_value();
    set_data(ADDR_PARAM_VOLT_SYS_THRS,value);
    value = data(ADDR_PARAM_VOLT_SYS_THRS);
    menu_enter_data(value);
}

void  child_4x_select(void)
{
    uint32_t value = menu_get_item_value();
    set_data(ADDR_PARAM_COMPR_CONTROL,value);
    value = data(ADDR_PARAM_COMPR_CONTROL);
    menu_enter_data(value);
}

void  child_5x_select(void)
{
    uint32_t value = menu_get_item_value();
    set_data(ADDR_PARAM_CONTROL_DOOR,value);
    value = data(ADDR_PARAM_CONTROL_DOOR);
    menu_enter_data(value);
}

void  child_6x_select(void)
{

    uint32_t value = menu_get_item_value();
    set_data(ADDR_PARAM_STOP_SIG_CONTR,value);
    value = data(ADDR_PARAM_STOP_SIG_CONTR);
    menu_enter_data(value);
}

void  child_7x_select(void)
{
    uint32_t value = menu_get_item_value();
    set_data(ADDR_CONTROL_SAVERESET,value); 
    menu_enter_setect_state(NOT_SELECT);
    menu_enter_data(value);
}
