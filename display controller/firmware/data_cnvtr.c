#include "data_cnvtr.h"


flash char err_str[] = {"err "};
flash char unkown_str[] = {"xxx"};
flash char depth_min_str[] = {"min>"};
flash char depth_med_str[] = {"med>"};
flash char depth_max_str[] = {"max>"};
flash char depth_m_r_str[] = {"rear"};
flash char depth_m_f_str[] = {"fron"};
flash char depth_m_all_str[] = {"all "};

float int_to_float(uint32_t b)
{
    return (float) b * 10000 / 27306666;
}
 
void convert_percent_to_str(uint32_t data, uint8_t *str)
{
    uint8_t count;
    if(data >100 || data == VALUE_PERSENT_UNKOWN)
    {
      memcpyf(str,unkown_str,3);
    } else {
        str[0] = data/100;
        str[1] = (data-str[0]*100)/10;
        str[2] = data-str[0]*100-str[1]*10;
        for(count = 0 ; count < 3; count++)
        {
            str[count]+=0x30;
        }
    }
}

void convert_voltage_data_to_str(uint32_t data,uint8_t *str)
{    
    float voltage = int_to_float(data);
    uint8_t voltage_int;
    uint8_t count;
    if(voltage > 25.0f || voltage < 7.0f)
    {
      memcpyf(str,err_str,4);
    } else {
        
        voltage *= 10.0f;
        voltage_int  = (uint8_t) voltage;
        str[0] =  voltage_int / 100;
        str[1] = (voltage_int - str[0] * 100) / 10;
        str[3] =  voltage_int - str[0] * 100 - str[1] * 10;
        for(count = 0; count < 4; count++)
        {
            str[count]+=0x30;
        }
        str[2]='.';
    }
}

void convert_depth_mode_to_str( uint32_t data, uint8_t *str)
{

    if(data == VALUE_DEPTH_MIN)
        memcpyf(str,depth_min_str,4);
    else if(data == VALUE_DEPTH_MED)
        memcpyf(str,depth_med_str,4);
    else if(data == VALUE_DEPTH_MAX)
        memcpyf(str,depth_max_str,4);
    else if(data == VALUE_DEPTH_MALL)
        memcpyf(str,depth_m_all_str,4);
    else if(data == VALUE_DEPTH_MR)
        memcpyf(str,depth_m_r_str,4);
    else if(data == VALUE_DEPTH_MF)
        memcpyf(str,depth_m_f_str,4);

}




