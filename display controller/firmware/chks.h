#ifndef _CHKS_INCLUDED_
#define _CHKS_INCLUDED_

#include "help.h"
#include "proto.h"

uint8_t chks_is_ok (uint8_t* packet);

uint8_t chks(uint8_t* packet);


#endif