#ifndef _MFUNC_INCLUDED_
#define _MFUNC_INCLUDED_

#include "menu.h"
#include "data_cmd.h"
#include "button.h"

void main_3_check(void);
void main_4_check(void);
void main_5_check(void);
void main_6_check(void);
void main1_x_long_press(void);


void chlid_1x_encr_decr(void);
void child_1x_long_press(void);
void chlid_1x_select(void);

void child_21_check(void);
void child_22_check(void);
void child_21x_select(void);
void child_22x_select(void);
void child_3x_select(void);
void child_4x_select(void);
void child_5x_select(void);
void child_6x_select(void);
void child_7x_select(void);


#endif
