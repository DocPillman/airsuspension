#include "menu.h"
#include "button.h"
#include "mfunc.h"

menu_t menu;

DECLARE_ITEM( main1 );  
DECLARE_ITEM( main2 );
DECLARE_ITEM( main3 );
DECLARE_ITEM( main4 );
DECLARE_ITEM( main5 );
DECLARE_ITEM( main6 );
DECLARE_ITEM( main7 );

   DECLARE_ITEM( child11 );
   DECLARE_ITEM( child12 );  
   DECLARE_ITEM( child13 );
   DECLARE_ITEM( child14 );
   DECLARE_ITEM( child15 );   
   DECLARE_ITEM( child16 );
   DECLARE_ITEM( child17 );

   DECLARE_ITEM( child21 );
         DECLARE_ITEM( child211 );
         DECLARE_ITEM( child212 );
         DECLARE_ITEM( child213 );
         DECLARE_ITEM( child214 );
         DECLARE_ITEM( child215 );

   DECLARE_ITEM( child22 );
         DECLARE_ITEM( child221 );
         DECLARE_ITEM( child222 );
         DECLARE_ITEM( child223 );
         DECLARE_ITEM( child224 );

   DECLARE_ITEM( child31 );
   DECLARE_ITEM( child32 );
   DECLARE_ITEM( child33 );
   DECLARE_ITEM( child34 );

   DECLARE_ITEM( child41 );
   DECLARE_ITEM( child42 );

   
   DECLARE_ITEM( child51 );
   DECLARE_ITEM( child52 );


   DECLARE_ITEM( child61 );
   DECLARE_ITEM( child62 ); 
   
   DECLARE_ITEM( child71 );
   DECLARE_ITEM( child72 );

//                      next,       prev,    parent,    child,        Select,           LongPressed,        /incr/decr/,         	item_value,              text
MAKE_ITEM main1 =    {  &main2,     &main7,   NULL,    &child11,       NULL,        &main1_x_long_press,        NULL,                NOTUSED,           "calibrate     " };
MAKE_ITEM main2 =    {  &main3,     &main1,   NULL,    &child21,       NULL,        &main1_x_long_press,        NULL,                NOTUSED,           "time ON/OFF   " };
MAKE_ITEM main3=     {  &main4,     &main2,   NULL,    &child31,    &main_3_check,  &main1_x_long_press,        NULL,                NOTUSED,           "battery ctrl  " };
MAKE_ITEM main4=     {  &main5,     &main3,   NULL,    &child41,    &main_4_check,  &main1_x_long_press,        NULL,                NOTUSED,           "compressor    " };
MAKE_ITEM main5=     {  &main6,     &main4,   NULL,    &child51,    &main_5_check,  &main1_x_long_press,        NULL,                NOTUSED,           "door control  " };
MAKE_ITEM main6=     {  &main7,     &main5,   NULL,    &child61,    &main_6_check,  &main1_x_long_press,        NULL,                NOTUSED,           "brake control " };
MAKE_ITEM main7=     {  &main1,     &main6,   NULL,    &child71,       NULL,        &main1_x_long_press,        NULL,                NOTUSED,           "save/reset    " };

MAKE_ITEM child11 =  { &child12,   &child17,  &main1,    NULL,	  &chlid_1x_select,	&child_1x_long_press, &chlid_1x_encr_decr, ADDR_CONTR_ALL_VALVE,    "ALL UP-DOWN   " };
MAKE_ITEM child12 =  { &child13,   &child11,  &main1,    NULL,    &chlid_1x_select, &child_1x_long_press, &chlid_1x_encr_decr, ADDR_CONTR_F_VALVE,      "F UP-DOWN     " };
MAKE_ITEM child13 =  { &child14,   &child12,  &main1,    NULL,    &chlid_1x_select, &child_1x_long_press, &chlid_1x_encr_decr, ADDR_CONTR_R_VALVE,      "R UP-DOWN     " };
MAKE_ITEM child14 =  { &child15,   &child13,  &main1,    NULL,    &chlid_1x_select, &child_1x_long_press, &chlid_1x_encr_decr, ADDR_CONTR_FL_VALVE,     "FL UP-DOWN    " };
MAKE_ITEM child15 =  { &child16,   &child14,  &main1,    NULL,    &chlid_1x_select, &child_1x_long_press, &chlid_1x_encr_decr, ADDR_CONTR_FR_VALVE,     "FR UP-DOWN    " };
MAKE_ITEM child16 =  { &child17,   &child15,  &main1,    NULL,    &chlid_1x_select, &child_1x_long_press, &chlid_1x_encr_decr, ADDR_CONTR_RL_VALVE,     "RL UP-DOWN    " };
MAKE_ITEM child17 =  { &child11,   &child16,  &main1,    NULL,    &chlid_1x_select, &child_1x_long_press, &chlid_1x_encr_decr, ADDR_CONTR_RR_VALVE,     "RR UP-DOWN    " };

MAKE_ITEM child21 =  { &child22,   &child22,  &main2,  &child211,   &child_21_check,        NULL,               NULL,                NOTUSED,           "time ON valve " };
MAKE_ITEM child22 =  { &child21,   &child21,  &main2,  &child221,   &child_22_check,        NULL,               NULL,                NOTUSED,           "time OFF valve" };

MAKE_ITEM child211 = { &child212, &child215, &child21,   NULL,    &child_21x_select,        NULL,               NULL,           VALUE_VAL_ONTIME0,      "continuously  " };
MAKE_ITEM child212 = { &child213, &child211, &child21,   NULL,    &child_21x_select,        NULL,               NULL,           VALUE_VAL_ONTIME5,      "5 ms          " };
MAKE_ITEM child213 = { &child214, &child212, &child21,   NULL,    &child_21x_select,        NULL,               NULL,           VALUE_VAL_ONTIME7,      "7 ms          " };
MAKE_ITEM child214 = { &child215, &child213, &child21,   NULL,    &child_21x_select,        NULL,               NULL,           VALUE_VAL_ONTIME10,     "10 ms         " };
MAKE_ITEM child215 = { &child211, &child214, &child21,   NULL,    &child_21x_select,        NULL,               NULL,           VALUE_VAL_ONTIME12,     "12 ms         " };

MAKE_ITEM child221 = { &child222, &child224, &child22,   NULL,    &child_22x_select,        NULL,               NULL,            VALUE_VAL_OFFTIME0,    "0 ms          " };
MAKE_ITEM child222 = { &child223, &child221, &child22,   NULL,    &child_22x_select,        NULL,               NULL,           VALUE_VAL_OFFTIME17,    "17 ms         " };
MAKE_ITEM child223 = { &child224, &child222, &child22,   NULL,    &child_22x_select,        NULL,               NULL,           VALUE_VAL_OFFTIME50,    "50 ms         " };
MAKE_ITEM child224 = { &child221, &child223, &child22,   NULL,    &child_22x_select,        NULL,               NULL,           VALUE_VAL_OFFTIME100,   "100 ms        " };

MAKE_ITEM child31 =  { &child32,  &child34,   &main3,    NULL,    &child_3x_select,         NULL,               NULL,           VALUE_TRSH_VOL_DISABLE, "off           " };
MAKE_ITEM child32 =  { &child33,  &child31,   &main3,    NULL,    &child_3x_select,         NULL,               NULL,             VALUE_TRSH_VOL_10V,   "10.0 V        " };
MAKE_ITEM child33 =  { &child34,  &child32,   &main3,    NULL,    &child_3x_select,         NULL,               NULL,             VALUE_TRSH_VOL_11V,   "11.0 V        " };
MAKE_ITEM child34 =  { &child31,  &child33,   &main3,    NULL,    &child_3x_select,         NULL,               NULL,             VALUE_TRSH_VOL_12V,   "12.0 V        " };

MAKE_ITEM child41 =  { &child42,  &child42,   &main4,    NULL,    &child_4x_select,         NULL,               NULL,              VALUE_COMP_OFF,      "compr ctrl OFF" };
MAKE_ITEM child42=   { &child41,  &child41,   &main4,    NULL,    &child_4x_select,         NULL,               NULL,              VALUE_COMP_ON,       "compr ctrl ON " };

MAKE_ITEM child51 =  { &child52,   &child51,  &main5,    NULL,    &child_5x_select,         NULL,               NULL,            VALUE_DOOR_CONTR_OFF,  "door ctrl OFF " };
MAKE_ITEM child52 =  { &child51,   &child52,  &main5,    NULL,    &child_5x_select,         NULL,               NULL,            VALUE_DOOR_CONTR_ON,   "door ctrl ON  " };

MAKE_ITEM child61 =  { &child62,   &child62,  &main6,    NULL,    &child_6x_select,         NULL,               NULL,             VALUE_STOP_DISABLE,   "brake ctrl OFF" };
MAKE_ITEM child62 =  { &child61,   &child61,  &main6,    NULL,    &child_6x_select,         NULL,               NULL,             VALUE_STOP_ENABLE,    "brake ctrl ON " };

MAKE_ITEM child71 =  { &child72,   &child72,  &main7,    NULL,    &child_7x_select,         NULL,               NULL,             VALUE_SAVE_SETTING,   "save          " };
MAKE_ITEM child72 =  { &child71,   &child71,  &main7,    NULL,    &child_7x_select,         NULL,               NULL,            VALUE_RESET_SETTING,   "reset         " };


void menu_init(void)
{
    menu.exit = false;
    menu.item = &main1;
    menu.select_state = NOT_SELECT;
    menu.readed_data = NOTUSED;
}

void menu_enter_setect_state(uint8_t state)
{
    menu.select_state = state;
}

void menu_update(void){

    MAKE_ITEM *next_item = menu.item->next;

    memset(menu.frame,' ',FRAME_SIZE);
    menu.frame[0]= SYMBOL_CURSOR;


    if(menu.select_state == INCR_DECR_WHEN_SELECTED || menu.select_state == SHOW_CHOOSEN_VALUE)
    {
        if(menu.readed_data == menu.item->item_value)
            menu.frame[FRAME_ROW - 1] = SYMBOL_SET;

        if(menu.readed_data == next_item->item_value)
            menu.frame[FRAME_SIZE - 1] = SYMBOL_SET;
    }
    memcpyf(&menu.frame[1],menu.item->text,ITEM_TEXT_SIZE);
    memcpyf(&menu.frame[FRAME_ROW + 1],next_item->text,ITEM_TEXT_SIZE);
}

char *menu_get_frame(void)
{
	return menu.frame;
}


void menu_enter_data(uint32_t data)
{
	menu.readed_data = data;
}

uint32_t menu_get_item_value(void)
{
	return menu.item->item_value;
}


bool menu_item_select(void)
{
	if( menu.item->select_callback != NULL )
	{            
		((void(*)(void))menu.item->select_callback)();
		return true;
	};
	return false;
}

bool menu_item_incr_decr(void)
{
	if( menu.item->incr_decr_callback != NULL )
	{
		((void(*)(void))menu.item->incr_decr_callback)();
		return true;
	};               	
	return false;
}

bool menu_item_long_press(void)
{
	if( menu.item->long_press_callback != NULL )
	{  
		((void(*)(void))menu.item->long_press_callback)();
		return true;
	};               	
	return true;
}

void menu_navigate(uint8_t btn)
{
	switch( btn )
	{ 
		case btn_empty:
			return;
			break;

		case btn_down:
		    if(menu.select_state == NOT_SELECT || menu.select_state == SHOW_CHOOSEN_VALUE)
            {
                if( menu.item->next != NULL )
                    menu.item = menu.item->next;

            }else if(menu.select_state == INCR_DECR_WHEN_SELECTED)
            {
                menu_item_incr_decr();
            }

            break;

		case btn_up:
            if(menu.select_state == NOT_SELECT || menu.select_state == SHOW_CHOOSEN_VALUE)
            {
                if( menu.item->prev != NULL )
                    menu.item = menu.item->prev;

            }else if(menu.select_state == INCR_DECR_WHEN_SELECTED)
            {
                menu_item_incr_decr();
            }

            break;

		case btn_right: // enter
            if( menu.select_state == INCR_DECR_WHEN_SELECTED)
            {
                menu.select_state = NOT_SELECT;
                return;
            }

            menu_item_select();

			if( menu.item->child != NULL )
                menu.item = menu.item->child;


			break;

        case btn_menu:
            if(menu.select_state != NOT_SELECT)
                menu.select_state = NOT_SELECT;

			if( menu.item->parent != NULL )
				menu.item = menu.item->parent;

			break;

        case btn_long_menu:
            if(menu_item_long_press())
              return;
			break;

	};
}

void menu_set_message_box(flash char* messadge)
{
    menu.message_box = messadge;
}

void menu_clear_message_box(void)
{
	menu.message_box = NULL;
}

flash char* menu_get_message_box(void)
{
    return menu.message_box;
}

void menu_exit(void)
{
	menu.exit = true;
}

bool menu_is_off(void)
{
	return menu.exit;
}
