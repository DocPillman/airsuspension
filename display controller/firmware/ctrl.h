#ifndef _CTRL_INCLUDED_
#define _CTRL_INCLUDED_


#include "help.h"
#include "view.h"


typedef enum
{
    start,
    sleep,
    menu,
    main,
    verror,


}ctrl_state_t;

void ctrl_task(void);
void ctrl_restart(void);

#endif