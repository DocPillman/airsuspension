#ifndef  _PDATA_INCLUDED_
#define  _PDATA_INCLUDED_

#include "help.h"


#define PAYLOAD_SIZE  sizeof(payload_t)
#define PACKET_SIZE   sizeof(packet_t)

typedef struct
{
    uint8_t type_command;
    uint16_t address;
    uint32_t value;
}payload_t;



typedef struct
{
    uint8_t id;
    payload_t payload;
    uint8_t chks;

}packet_t;


#endif