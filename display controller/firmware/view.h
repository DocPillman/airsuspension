#ifndef _VIEW_INCLUDED_
#define _VIEW_INCLUDED_

#include "help.h"
#include "poll.h"
#include "lcd.h"
#include "data_cnvtr.h"
#include "timer.h"
#include "button.h"
#include "clearance.h"



void view_main(void);
void view_error(uint32_t error_code);
void view_menu(void);


#endif