#include "ctrl.h"
#include "delay.h"
#include "status.h"
#include "timer.h"
#include "data_cmd.h"
#include "lcd.h"
#include <mega8.h>

#define RESTART_PAUSE 1000
#define SHOW_ERROR_INTERVAL 5000
#define POLL_STATUS_TIMEOUT_IN_SLEEP 200

ctrl_state_t state = main;

uint16_t show_error_timeout = 0;
bool postpone_show = false;




bool master_device_is_off(void)
{
    uint32_t data = get_data(ADDR_PARAM_POWER_STATE);
    return data == VALUE_POWER_SLEEP ? true : false ;
}

void ctrl_task(void)
{
    uint32_t current_error = 0;
    uint16_t status_timeout = 0;

    if(master_device_is_off())
    {
        state = sleep;
        status_timeout = tim_get_tic();
    }


    switch(state)
    {
        case main: view_main(); break;
        case menu: view_menu(); break;
        case sleep:
            lcd_off();
            while (tim_get_time_mc(status_timeout) < POLL_STATUS_TIMEOUT_IN_SLEEP)
            {
                state = main;
                return;
            }
            break;
    }

    if(menu_is_off())
        state = main;
    else if(!master_device_is_off())
        state = menu;
        
    if(state == main && !postpone_show)
    {
        current_error = status_get_next_error();
        if(current_error != 0)
        {
            view_error(current_error);
            show_error_timeout = tim_get_tic();
            postpone_show = true;
        }
    }
    if(postpone_show && tim_get_time_mc(show_error_timeout) > SHOW_ERROR_INTERVAL)
    {
        postpone_show = false;
    }

}

void ctrl_restart(void)
{
    delay_ms(RESTART_PAUSE);
    WDTCR=0x18;
    WDTCR=0x08;
    #asm("wdr")
    while(1);
}

