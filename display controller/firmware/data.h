#ifndef _DATA_INCLUDED_
#define _DATA_INCLUDED_

#include "addr_map.h"
#include "help.h"
#include <stdbool.h>


typedef struct
{
    uint32_t value;
    uint8_t download : 1;
    uint8_t upload : 1;
}data_t;


uint32_t data(uint16_t addr);

bool data_is_download(uint16_t addr);
bool data_is_upload(uint16_t addr);

void data_clear(uint16_t addr);

void data_download_cb(uint16_t addr, uint32_t val);
void data_upload_cb( uint16_t addr, uint32_t val);




#endif