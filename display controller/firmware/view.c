#include "view.h"
#include "addr_map.h"
#include "ctrl.h"


flash char communication_error_frame[] =   {"_COMMUNICATION______ERROR_______"};
flash char main_template_frame[] =         {"FLxxxUxx.xvFRxxxRLxxx[xxxx]RRxxx"};
flash char error_sensor_frame[] =          {"sensor error                    "};
flash char calibrate_save_error_frame[] =  {"calibrate or      save error    "};
flash char low_power_frame[] =             {"battery low                     "};
flash char compressor_timeout_frame[] =    {"compressor          timeout     "};
flash char clearance_set_timeout_frame[] = {"clearance set       timeout     "};

void make_main_frame(char *frame)
{
    uint32_t param;
    
    memcpyf(frame,&main_template_frame,LCD_SIZE);

    param = data(ADDR_PARAM_VOLTAGE);
    convert_voltage_data_to_str(param,&frame[6]);

    param = data(ADDR_PARAM_MEASURED_IN_PERS_FL);
    convert_percent_to_str(param,&frame[2]);

    param = data(ADDR_PARAM_MEASURED_IN_PERS_FR);
    convert_percent_to_str(param,&frame[13]);

    param = data(ADDR_PARAM_MEASURED_IN_PERS_RL);
    convert_percent_to_str(param,&frame[18]);

    param = data(ADDR_PARAM_MEASURED_IN_PERS_RR);
    convert_percent_to_str(param,&frame[29]);

    param = data(ADDR_PARAM_DEPTH_MODE);
    convert_depth_mode_to_str(param,&frame[22]);

}

void show_message_box(char *message)
{
    uint8_t i;
    lcd_show(message);
    for(i = 0; i < 5; i++)
    {
        lcd_backlight(off);
        delay_ms(200);
        lcd_backlight(on);
        delay_ms(200);
    }
}

void view_main(void)
{
    char frame[LCD_SIZE];
    uint16_t time;
    button_t btn;
    mode_type_t mode;
    lcd_backlight(on);
    polling_start();
    time = tim_get_tic();
    make_main_frame(frame);
    lcd_show(frame);

    while (!poll_is_finished)
    {
        if(tim_get_time_mc(time) > POLLING_TIME)
        {
            ctrl_restart();
            return;
        }
    }
    if(button_is_pushed())
    {
        btn = button_last_push();
        mode = clearance_get_mode();

        switch (btn)
        {
            case btn_long_menu:
                menu_init();
                break;
            case btn_menu:
                clearance_switch_auto_manual();
                break;
            case btn_right:
                if(mode == auto_mode)
                    clearance_set_or_manual_up();
                else if(mode == manual_mode)
                    clearance_switch_manual_param();
                break;
            case btn_up:
                if(mode == auto_mode)
                    clearance_auto_up();
                else if(mode == manual_mode)
                    clearance_set_or_manual_up();
                break;
            case btn_down:
                if(mode == auto_mode)
                    clearance_auto_down();
                else if(mode == manual_mode)
                    clearance_manual_down();
                break;
            default:
                break;
        }
        button_clear_info();
    }
}

void view_error(uint32_t error_code)
{
    uint8_t i;
    uint8_t frame[LCD_SIZE];
    flash char * error_frame;
    switch (error_code)
    {
        case VALUE_STATUS_ERROR_SHORT_CUR_SENS : error_frame = error_sensor_frame; break;
        case VALUE_STATUS_ERROR_COMMUNICATION  : error_frame = communication_error_frame; break;
        case VALUE_STATUS_ERROR_CALIBRATE      : error_frame = calibrate_save_error_frame; break;
        case VALUE_STATUS_ERROR_SAVE_USER_DATA : error_frame = calibrate_save_error_frame; break;
        case VALUE_STATUS_ERROR_LOW_POWER      : error_frame = low_power_frame; break;
        case VALUE_STATUS_COMPRESSOR_TIMEOUT   : error_frame = compressor_timeout_frame; break;
        case VALUE_STATUS_CLEARANCE_SET_TIMEOUT: error_frame = clearance_set_timeout_frame; break;
        default:
            break;
    }
    memcpyf(frame,error_frame,LCD_SIZE);
    show_message_box(frame);
}

void view_menu(void)
{
    button_t btn;
    char *frame;
    flash char *message;
    char message_frame[LCD_SIZE];
    lcd_backlight(on);
    if(button_is_pushed())
    {
        btn = button_last_push();
        menu_navigate(btn);
        button_clear_info();
    }
    menu_update();
    frame = menu_get_frame();
    lcd_show(frame);
    if(menu_get_message_box() != NULL )
    {
        message = menu_get_message_box();
        memcpyf(message_frame,message,LCD_SIZE);
        show_message_box(message_frame);
        menu_clear_message_box();
    }
    delay_ms(300);
}



