
#include <mega8.h>
#include <alcd.h>
#include <stdio.h>
#include <string.h>
#include <delay.h>
#include "timer.h"
#include "button.h"
#include "view.h"
#include "ctrl.h"


void mcu_init(void);
void gpio_init(void);



interrupt [EXT_INT1] void ext_int1_isr(void)
{
    button_handler();
}


interrupt [USART_RXC] void usart_rx_isr(void)
{
    uart_rx_isr_handler();
}


interrupt [USART_DRE] void usart_tx_isr(void)
{
    uart_tx_isr_handler();
}


interrupt [TIM0_OVF] void timer0_ovf_isr(void)
{
    TCNT0=0x06; // reload
    tim_handler();
    button_timer_handler();
}

interrupt [TIM2_OVF] void timer2_ovf_isr(void)
{
    TCNT2=0x06;
    proto_tim_handler();
}

//-------------------------------------------------------------------------------------//


void main(void)
{
    mcu_init();
    gpio_init();
    tim_init();
    uart_init();
    proto_init();
    lcd_init(16);
    menu_init();
    menu_exit();
    #asm("sei")
    
    while (1)
    {
        ctrl_task();
    }

}


void gpio_init (void)
{
    PORTB = 0x03;
    DDRB = 0x3C;
    PORTC = 0x00;
    DDRC = 0x00;
    PORTD = 0xF4;
    DDRD = 0x00;
}

void mcu_init(void)
{

    GICR|=0x80;
    MCUCR=0x08;
    GIFR=0x80;

    ACSR=0x80;
    SFIOR=0x00;
    ADCSRA=0x00;
    SPCR=0x00;
    TWCR=0x00;
}