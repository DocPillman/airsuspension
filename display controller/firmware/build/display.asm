
;CodeVisionAVR C Compiler V2.05.0 Professional
;(C) Copyright 1998-2010 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com

;Chip type                : ATmega8
;Program type             : Application
;Clock frequency          : 16,000000 MHz
;Memory model             : Small
;Optimize for             : Size
;(s)printf features       : int, width
;(s)scanf features        : int, width
;External RAM size        : 0
;Data Stack size          : 256 byte(s)
;Heap size                : 0 byte(s)
;Promote 'char' to 'int'  : No
;'char' is unsigned       : Yes
;8 bit enums              : Yes
;global 'const' stored in FLASH: No
;Enhanced core instructions    : On
;Smart register allocation     : On
;Automatic register allocation : On

	#pragma AVRPART ADMIN PART_NAME ATmega8
	#pragma AVRPART MEMORY PROG_FLASH 8192
	#pragma AVRPART MEMORY EEPROM 512
	#pragma AVRPART MEMORY INT_SRAM SIZE 1119
	#pragma AVRPART MEMORY INT_SRAM START_ADDR 0x60

	.LISTMAC
	.EQU UDRE=0x5
	.EQU RXC=0x7
	.EQU USR=0xB
	.EQU UDR=0xC
	.EQU SPSR=0xE
	.EQU SPDR=0xF
	.EQU EERE=0x0
	.EQU EEWE=0x1
	.EQU EEMWE=0x2
	.EQU EECR=0x1C
	.EQU EEDR=0x1D
	.EQU EEARL=0x1E
	.EQU EEARH=0x1F
	.EQU WDTCR=0x21
	.EQU MCUCR=0x35
	.EQU GICR=0x3B
	.EQU SPL=0x3D
	.EQU SPH=0x3E
	.EQU SREG=0x3F

	.DEF R0X0=R0
	.DEF R0X1=R1
	.DEF R0X2=R2
	.DEF R0X3=R3
	.DEF R0X4=R4
	.DEF R0X5=R5
	.DEF R0X6=R6
	.DEF R0X7=R7
	.DEF R0X8=R8
	.DEF R0X9=R9
	.DEF R0XA=R10
	.DEF R0XB=R11
	.DEF R0XC=R12
	.DEF R0XD=R13
	.DEF R0XE=R14
	.DEF R0XF=R15
	.DEF R0X10=R16
	.DEF R0X11=R17
	.DEF R0X12=R18
	.DEF R0X13=R19
	.DEF R0X14=R20
	.DEF R0X15=R21
	.DEF R0X16=R22
	.DEF R0X17=R23
	.DEF R0X18=R24
	.DEF R0X19=R25
	.DEF R0X1A=R26
	.DEF R0X1B=R27
	.DEF R0X1C=R28
	.DEF R0X1D=R29
	.DEF R0X1E=R30
	.DEF R0X1F=R31

	.EQU __SRAM_START=0x0060
	.EQU __SRAM_END=0x045F
	.EQU __DSTACK_SIZE=0x0100
	.EQU __HEAP_SIZE=0x0000
	.EQU __CLEAR_SRAM_SIZE=__SRAM_END-__SRAM_START+1

	.MACRO __CPD1N
	CPI  R30,LOW(@0)
	LDI  R26,HIGH(@0)
	CPC  R31,R26
	LDI  R26,BYTE3(@0)
	CPC  R22,R26
	LDI  R26,BYTE4(@0)
	CPC  R23,R26
	.ENDM

	.MACRO __CPD2N
	CPI  R26,LOW(@0)
	LDI  R30,HIGH(@0)
	CPC  R27,R30
	LDI  R30,BYTE3(@0)
	CPC  R24,R30
	LDI  R30,BYTE4(@0)
	CPC  R25,R30
	.ENDM

	.MACRO __CPWRR
	CP   R@0,R@2
	CPC  R@1,R@3
	.ENDM

	.MACRO __CPWRN
	CPI  R@0,LOW(@2)
	LDI  R30,HIGH(@2)
	CPC  R@1,R30
	.ENDM

	.MACRO __ADDB1MN
	SUBI R30,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDB2MN
	SUBI R26,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDW1MN
	SUBI R30,LOW(-@0-(@1))
	SBCI R31,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW2MN
	SUBI R26,LOW(-@0-(@1))
	SBCI R27,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	SBCI R22,BYTE3(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1N
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	SBCI R22,BYTE3(-@0)
	SBCI R23,BYTE4(-@0)
	.ENDM

	.MACRO __ADDD2N
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	SBCI R24,BYTE3(-@0)
	SBCI R25,BYTE4(-@0)
	.ENDM

	.MACRO __SUBD1N
	SUBI R30,LOW(@0)
	SBCI R31,HIGH(@0)
	SBCI R22,BYTE3(@0)
	SBCI R23,BYTE4(@0)
	.ENDM

	.MACRO __SUBD2N
	SUBI R26,LOW(@0)
	SBCI R27,HIGH(@0)
	SBCI R24,BYTE3(@0)
	SBCI R25,BYTE4(@0)
	.ENDM

	.MACRO __ANDBMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ANDWMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ANDI R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ANDD1N
	ANDI R30,LOW(@0)
	ANDI R31,HIGH(@0)
	ANDI R22,BYTE3(@0)
	ANDI R23,BYTE4(@0)
	.ENDM

	.MACRO __ANDD2N
	ANDI R26,LOW(@0)
	ANDI R27,HIGH(@0)
	ANDI R24,BYTE3(@0)
	ANDI R25,BYTE4(@0)
	.ENDM

	.MACRO __ORBMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ORWMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ORI  R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ORD1N
	ORI  R30,LOW(@0)
	ORI  R31,HIGH(@0)
	ORI  R22,BYTE3(@0)
	ORI  R23,BYTE4(@0)
	.ENDM

	.MACRO __ORD2N
	ORI  R26,LOW(@0)
	ORI  R27,HIGH(@0)
	ORI  R24,BYTE3(@0)
	ORI  R25,BYTE4(@0)
	.ENDM

	.MACRO __DELAY_USB
	LDI  R24,LOW(@0)
__DELAY_USB_LOOP:
	DEC  R24
	BRNE __DELAY_USB_LOOP
	.ENDM

	.MACRO __DELAY_USW
	LDI  R24,LOW(@0)
	LDI  R25,HIGH(@0)
__DELAY_USW_LOOP:
	SBIW R24,1
	BRNE __DELAY_USW_LOOP
	.ENDM

	.MACRO __GETD1S
	LDD  R30,Y+@0
	LDD  R31,Y+@0+1
	LDD  R22,Y+@0+2
	LDD  R23,Y+@0+3
	.ENDM

	.MACRO __GETD2S
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	LDD  R24,Y+@0+2
	LDD  R25,Y+@0+3
	.ENDM

	.MACRO __PUTD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R31
	STD  Y+@0+2,R22
	STD  Y+@0+3,R23
	.ENDM

	.MACRO __PUTD2S
	STD  Y+@0,R26
	STD  Y+@0+1,R27
	STD  Y+@0+2,R24
	STD  Y+@0+3,R25
	.ENDM

	.MACRO __PUTDZ2
	STD  Z+@0,R26
	STD  Z+@0+1,R27
	STD  Z+@0+2,R24
	STD  Z+@0+3,R25
	.ENDM

	.MACRO __CLRD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R30
	STD  Y+@0+2,R30
	STD  Y+@0+3,R30
	.ENDM

	.MACRO __POINTB1MN
	LDI  R30,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW1MN
	LDI  R30,LOW(@0+(@1))
	LDI  R31,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTD1M
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __POINTW1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	.ENDM

	.MACRO __POINTD1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	LDI  R22,BYTE3(2*@0+(@1))
	LDI  R23,BYTE4(2*@0+(@1))
	.ENDM

	.MACRO __POINTB2MN
	LDI  R26,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW2MN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTBRM
	LDI  R@0,LOW(@1)
	.ENDM

	.MACRO __POINTWRM
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __POINTBRMN
	LDI  R@0,LOW(@1+(@2))
	.ENDM

	.MACRO __POINTWRMN
	LDI  R@0,LOW(@2+(@3))
	LDI  R@1,HIGH(@2+(@3))
	.ENDM

	.MACRO __POINTWRFN
	LDI  R@0,LOW(@2*2+(@3))
	LDI  R@1,HIGH(@2*2+(@3))
	.ENDM

	.MACRO __GETD1N
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __GETD2N
	LDI  R26,LOW(@0)
	LDI  R27,HIGH(@0)
	LDI  R24,BYTE3(@0)
	LDI  R25,BYTE4(@0)
	.ENDM

	.MACRO __GETB1MN
	LDS  R30,@0+(@1)
	.ENDM

	.MACRO __GETB1HMN
	LDS  R31,@0+(@1)
	.ENDM

	.MACRO __GETW1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	.ENDM

	.MACRO __GETD1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	LDS  R22,@0+(@1)+2
	LDS  R23,@0+(@1)+3
	.ENDM

	.MACRO __GETBRMN
	LDS  R@0,@1+(@2)
	.ENDM

	.MACRO __GETWRMN
	LDS  R@0,@2+(@3)
	LDS  R@1,@2+(@3)+1
	.ENDM

	.MACRO __GETWRZ
	LDD  R@0,Z+@2
	LDD  R@1,Z+@2+1
	.ENDM

	.MACRO __GETD2Z
	LDD  R26,Z+@0
	LDD  R27,Z+@0+1
	LDD  R24,Z+@0+2
	LDD  R25,Z+@0+3
	.ENDM

	.MACRO __GETB2MN
	LDS  R26,@0+(@1)
	.ENDM

	.MACRO __GETW2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	.ENDM

	.MACRO __GETD2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	LDS  R24,@0+(@1)+2
	LDS  R25,@0+(@1)+3
	.ENDM

	.MACRO __PUTB1MN
	STS  @0+(@1),R30
	.ENDM

	.MACRO __PUTW1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	.ENDM

	.MACRO __PUTD1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	STS  @0+(@1)+2,R22
	STS  @0+(@1)+3,R23
	.ENDM

	.MACRO __PUTB1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMWRB
	.ENDM

	.MACRO __PUTW1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMWRW
	.ENDM

	.MACRO __PUTD1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMWRD
	.ENDM

	.MACRO __PUTBR0MN
	STS  @0+(@1),R0
	.ENDM

	.MACRO __PUTBMRN
	STS  @0+(@1),R@2
	.ENDM

	.MACRO __PUTWMRN
	STS  @0+(@1),R@2
	STS  @0+(@1)+1,R@3
	.ENDM

	.MACRO __PUTBZR
	STD  Z+@1,R@0
	.ENDM

	.MACRO __PUTWZR
	STD  Z+@2,R@0
	STD  Z+@2+1,R@1
	.ENDM

	.MACRO __GETW1R
	MOV  R30,R@0
	MOV  R31,R@1
	.ENDM

	.MACRO __GETW2R
	MOV  R26,R@0
	MOV  R27,R@1
	.ENDM

	.MACRO __GETWRN
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __PUTW1R
	MOV  R@0,R30
	MOV  R@1,R31
	.ENDM

	.MACRO __PUTW2R
	MOV  R@0,R26
	MOV  R@1,R27
	.ENDM

	.MACRO __ADDWRN
	SUBI R@0,LOW(-@2)
	SBCI R@1,HIGH(-@2)
	.ENDM

	.MACRO __ADDWRR
	ADD  R@0,R@2
	ADC  R@1,R@3
	.ENDM

	.MACRO __SUBWRN
	SUBI R@0,LOW(@2)
	SBCI R@1,HIGH(@2)
	.ENDM

	.MACRO __SUBWRR
	SUB  R@0,R@2
	SBC  R@1,R@3
	.ENDM

	.MACRO __ANDWRN
	ANDI R@0,LOW(@2)
	ANDI R@1,HIGH(@2)
	.ENDM

	.MACRO __ANDWRR
	AND  R@0,R@2
	AND  R@1,R@3
	.ENDM

	.MACRO __ORWRN
	ORI  R@0,LOW(@2)
	ORI  R@1,HIGH(@2)
	.ENDM

	.MACRO __ORWRR
	OR   R@0,R@2
	OR   R@1,R@3
	.ENDM

	.MACRO __EORWRR
	EOR  R@0,R@2
	EOR  R@1,R@3
	.ENDM

	.MACRO __GETWRS
	LDD  R@0,Y+@2
	LDD  R@1,Y+@2+1
	.ENDM

	.MACRO __PUTBSR
	STD  Y+@1,R@0
	.ENDM

	.MACRO __PUTWSR
	STD  Y+@2,R@0
	STD  Y+@2+1,R@1
	.ENDM

	.MACRO __MOVEWRR
	MOV  R@0,R@2
	MOV  R@1,R@3
	.ENDM

	.MACRO __INWR
	IN   R@0,@2
	IN   R@1,@2+1
	.ENDM

	.MACRO __OUTWR
	OUT  @2+1,R@1
	OUT  @2,R@0
	.ENDM

	.MACRO __CALL1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	ICALL
	.ENDM

	.MACRO __CALL1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	RCALL __GETW1PF
	ICALL
	.ENDM

	.MACRO __CALL2EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMRDW
	ICALL
	.ENDM

	.MACRO __GETW1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X
	.ENDM

	.MACRO __GETD1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X+
	LD   R22,X
	.ENDM

	.MACRO __NBST
	BST  R@0,@1
	IN   R30,SREG
	LDI  R31,0x40
	EOR  R30,R31
	OUT  SREG,R30
	.ENDM


	.MACRO __PUTB1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RNS
	MOVW R26,R@0
	ADIW R26,@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	RCALL __PUTDP1
	.ENDM


	.MACRO __GETB1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R30,Z
	.ENDM

	.MACRO __GETB1HSX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	.ENDM

	.MACRO __GETW1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R31,Z
	MOV  R30,R0
	.ENDM

	.MACRO __GETD1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R1,Z+
	LD   R22,Z+
	LD   R23,Z
	MOVW R30,R0
	.ENDM

	.MACRO __GETB2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R26,X
	.ENDM

	.MACRO __GETW2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	.ENDM

	.MACRO __GETD2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R1,X+
	LD   R24,X+
	LD   R25,X
	MOVW R26,R0
	.ENDM

	.MACRO __GETBRSX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	LD   R@0,Z
	.ENDM

	.MACRO __GETWRSX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	LD   R@0,Z+
	LD   R@1,Z
	.ENDM

	.MACRO __GETBRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	LD   R@0,X
	.ENDM

	.MACRO __GETWRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	LD   R@0,X+
	LD   R@1,X
	.ENDM

	.MACRO __LSLW8SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	CLR  R30
	.ENDM

	.MACRO __PUTB1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __CLRW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __CLRD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R30
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __PUTB2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z,R26
	.ENDM

	.MACRO __PUTW2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z,R27
	.ENDM

	.MACRO __PUTD2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z+,R27
	ST   Z+,R24
	ST   Z,R25
	.ENDM

	.MACRO __PUTBSRX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	ST   Z,R@0
	.ENDM

	.MACRO __PUTWSRX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	ST   Z+,R@0
	ST   Z,R@1
	.ENDM

	.MACRO __PUTB1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __MULBRR
	MULS R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRRU
	MUL  R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRR0
	MULS R@0,R@1
	.ENDM

	.MACRO __MULBRRU0
	MUL  R@0,R@1
	.ENDM

	.MACRO __MULBNWRU
	LDI  R26,@2
	MUL  R26,R@0
	MOVW R30,R0
	MUL  R26,R@1
	ADD  R31,R0
	.ENDM

;NAME DEFINITIONS FOR GLOBAL VARIABLES ALLOCATED TO REGISTERS
	.DEF _time_press=R3
	.DEF _push_status=R6
	.DEF _menu_pressed=R5
	.DEF _last_push=R8
	.DEF _count=R9
	.DEF _state=R7
	.DEF _show_error_timeout=R11
	.DEF _postpone_show=R14
	.DEF _error_counter=R13

	.CSEG
	.ORG 0x00

;START OF CODE MARKER
__START_OF_CODE:

;INTERRUPT VECTORS
	RJMP __RESET
	RJMP 0x00
	RJMP _ext_int1_isr
	RJMP 0x00
	RJMP _timer2_ovf_isr
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP _timer0_ovf_isr
	RJMP 0x00
	RJMP _usart_rx_isr
	RJMP _usart_tx_isr
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00

_main1:
	.DB  LOW(_main2*2),HIGH(_main2*2),LOW(_main7*2),HIGH(_main7*2),0x0,0x0,LOW(_child11*2),HIGH(_child11*2)
	.DB  0x0,0x0,LOW(_main1_x_long_press),HIGH(_main1_x_long_press),0x0,0x0,0xFF,LOW(_0xE0000*2)
	.DB  HIGH(_0xE0000*2)
_main2:
	.DB  LOW(_main3*2),HIGH(_main3*2),LOW(_main1*2),HIGH(_main1*2),0x0,0x0,LOW(_child21*2),HIGH(_child21*2)
	.DB  0x0,0x0,LOW(_main1_x_long_press),HIGH(_main1_x_long_press),0x0,0x0,0xFF,LOW(_0xE0000*2+15)
	.DB  HIGH(_0xE0000*2+15)
_main3:
	.DB  LOW(_main4*2),HIGH(_main4*2),LOW(_main2*2),HIGH(_main2*2),0x0,0x0,LOW(_child31*2),HIGH(_child31*2)
	.DB  LOW(_main_3_check),HIGH(_main_3_check),LOW(_main1_x_long_press),HIGH(_main1_x_long_press),0x0,0x0,0xFF,LOW(_0xE0000*2+30)
	.DB  HIGH(_0xE0000*2+30)
_main4:
	.DB  LOW(_main5*2),HIGH(_main5*2),LOW(_main3*2),HIGH(_main3*2),0x0,0x0,LOW(_child41*2),HIGH(_child41*2)
	.DB  LOW(_main_4_check),HIGH(_main_4_check),LOW(_main1_x_long_press),HIGH(_main1_x_long_press),0x0,0x0,0xFF,LOW(_0xE0000*2+45)
	.DB  HIGH(_0xE0000*2+45)
_main5:
	.DB  LOW(_main6*2),HIGH(_main6*2),LOW(_main4*2),HIGH(_main4*2),0x0,0x0,LOW(_child51*2),HIGH(_child51*2)
	.DB  LOW(_main_5_check),HIGH(_main_5_check),LOW(_main1_x_long_press),HIGH(_main1_x_long_press),0x0,0x0,0xFF,LOW(_0xE0000*2+60)
	.DB  HIGH(_0xE0000*2+60)
_main6:
	.DB  LOW(_main7*2),HIGH(_main7*2),LOW(_main5*2),HIGH(_main5*2),0x0,0x0,LOW(_child61*2),HIGH(_child61*2)
	.DB  LOW(_main_6_check),HIGH(_main_6_check),LOW(_main1_x_long_press),HIGH(_main1_x_long_press),0x0,0x0,0xFF,LOW(_0xE0000*2+75)
	.DB  HIGH(_0xE0000*2+75)
_main7:
	.DB  LOW(_main1*2),HIGH(_main1*2),LOW(_main6*2),HIGH(_main6*2),0x0,0x0,LOW(_child71*2),HIGH(_child71*2)
	.DB  0x0,0x0,LOW(_main1_x_long_press),HIGH(_main1_x_long_press),0x0,0x0,0xFF,LOW(_0xE0000*2+90)
	.DB  HIGH(_0xE0000*2+90)
_child11:
	.DB  LOW(_child12*2),HIGH(_child12*2),LOW(_child17*2),HIGH(_child17*2),LOW(_main1*2),HIGH(_main1*2),0x0,0x0
	.DB  LOW(_chlid_1x_select),HIGH(_chlid_1x_select),LOW(_child_1x_long_press),HIGH(_child_1x_long_press),LOW(_chlid_1x_encr_decr),HIGH(_chlid_1x_encr_decr),0x8,LOW(_0xE0000*2+105)
	.DB  HIGH(_0xE0000*2+105)
_child12:
	.DB  LOW(_child13*2),HIGH(_child13*2),LOW(_child11*2),HIGH(_child11*2),LOW(_main1*2),HIGH(_main1*2),0x0,0x0
	.DB  LOW(_chlid_1x_select),HIGH(_chlid_1x_select),LOW(_child_1x_long_press),HIGH(_child_1x_long_press),LOW(_chlid_1x_encr_decr),HIGH(_chlid_1x_encr_decr),0x6,LOW(_0xE0000*2+120)
	.DB  HIGH(_0xE0000*2+120)
_child13:
	.DB  LOW(_child14*2),HIGH(_child14*2),LOW(_child12*2),HIGH(_child12*2),LOW(_main1*2),HIGH(_main1*2),0x0,0x0
	.DB  LOW(_chlid_1x_select),HIGH(_chlid_1x_select),LOW(_child_1x_long_press),HIGH(_child_1x_long_press),LOW(_chlid_1x_encr_decr),HIGH(_chlid_1x_encr_decr),0x7,LOW(_0xE0000*2+135)
	.DB  HIGH(_0xE0000*2+135)
_child14:
	.DB  LOW(_child15*2),HIGH(_child15*2),LOW(_child13*2),HIGH(_child13*2),LOW(_main1*2),HIGH(_main1*2),0x0,0x0
	.DB  LOW(_chlid_1x_select),HIGH(_chlid_1x_select),LOW(_child_1x_long_press),HIGH(_child_1x_long_press),LOW(_chlid_1x_encr_decr),HIGH(_chlid_1x_encr_decr),0x2,LOW(_0xE0000*2+150)
	.DB  HIGH(_0xE0000*2+150)
_child15:
	.DB  LOW(_child16*2),HIGH(_child16*2),LOW(_child14*2),HIGH(_child14*2),LOW(_main1*2),HIGH(_main1*2),0x0,0x0
	.DB  LOW(_chlid_1x_select),HIGH(_chlid_1x_select),LOW(_child_1x_long_press),HIGH(_child_1x_long_press),LOW(_chlid_1x_encr_decr),HIGH(_chlid_1x_encr_decr),0x3,LOW(_0xE0000*2+165)
	.DB  HIGH(_0xE0000*2+165)
_child16:
	.DB  LOW(_child17*2),HIGH(_child17*2),LOW(_child15*2),HIGH(_child15*2),LOW(_main1*2),HIGH(_main1*2),0x0,0x0
	.DB  LOW(_chlid_1x_select),HIGH(_chlid_1x_select),LOW(_child_1x_long_press),HIGH(_child_1x_long_press),LOW(_chlid_1x_encr_decr),HIGH(_chlid_1x_encr_decr),0x4,LOW(_0xE0000*2+180)
	.DB  HIGH(_0xE0000*2+180)
_child17:
	.DB  LOW(_child11*2),HIGH(_child11*2),LOW(_child16*2),HIGH(_child16*2),LOW(_main1*2),HIGH(_main1*2),0x0,0x0
	.DB  LOW(_chlid_1x_select),HIGH(_chlid_1x_select),LOW(_child_1x_long_press),HIGH(_child_1x_long_press),LOW(_chlid_1x_encr_decr),HIGH(_chlid_1x_encr_decr),0x5,LOW(_0xE0000*2+195)
	.DB  HIGH(_0xE0000*2+195)
_child21:
	.DB  LOW(_child22*2),HIGH(_child22*2),LOW(_child22*2),HIGH(_child22*2),LOW(_main2*2),HIGH(_main2*2),LOW(_child211*2),HIGH(_child211*2)
	.DB  LOW(_child_21_check),HIGH(_child_21_check),0x0,0x0,0x0,0x0,0xFF,LOW(_0xE0000*2+210)
	.DB  HIGH(_0xE0000*2+210)
_child211:
	.DB  LOW(_child212*2),HIGH(_child212*2),LOW(_child215*2),HIGH(_child215*2),LOW(_child21*2),HIGH(_child21*2),0x0,0x0
	.DB  LOW(_child_21x_select),HIGH(_child_21x_select),0x0,0x0,0x0,0x0,0x0,LOW(_0xE0000*2+240)
	.DB  HIGH(_0xE0000*2+240)
_child212:
	.DB  LOW(_child213*2),HIGH(_child213*2),LOW(_child211*2),HIGH(_child211*2),LOW(_child21*2),HIGH(_child21*2),0x0,0x0
	.DB  LOW(_child_21x_select),HIGH(_child_21x_select),0x0,0x0,0x0,0x0,0x5,LOW(_0xE0000*2+255)
	.DB  HIGH(_0xE0000*2+255)
_child213:
	.DB  LOW(_child214*2),HIGH(_child214*2),LOW(_child212*2),HIGH(_child212*2),LOW(_child21*2),HIGH(_child21*2),0x0,0x0
	.DB  LOW(_child_21x_select),HIGH(_child_21x_select),0x0,0x0,0x0,0x0,0x7,LOW(_0xE0000*2+270)
	.DB  HIGH(_0xE0000*2+270)
_child214:
	.DB  LOW(_child215*2),HIGH(_child215*2),LOW(_child213*2),HIGH(_child213*2),LOW(_child21*2),HIGH(_child21*2),0x0,0x0
	.DB  LOW(_child_21x_select),HIGH(_child_21x_select),0x0,0x0,0x0,0x0,0xA,LOW(_0xE0000*2+285)
	.DB  HIGH(_0xE0000*2+285)
_child215:
	.DB  LOW(_child211*2),HIGH(_child211*2),LOW(_child214*2),HIGH(_child214*2),LOW(_child21*2),HIGH(_child21*2),0x0,0x0
	.DB  LOW(_child_21x_select),HIGH(_child_21x_select),0x0,0x0,0x0,0x0,0xC,LOW(_0xE0000*2+300)
	.DB  HIGH(_0xE0000*2+300)
_child22:
	.DB  LOW(_child21*2),HIGH(_child21*2),LOW(_child21*2),HIGH(_child21*2),LOW(_main2*2),HIGH(_main2*2),LOW(_child221*2),HIGH(_child221*2)
	.DB  LOW(_child_22_check),HIGH(_child_22_check),0x0,0x0,0x0,0x0,0xFF,LOW(_0xE0000*2+225)
	.DB  HIGH(_0xE0000*2+225)
_child221:
	.DB  LOW(_child222*2),HIGH(_child222*2),LOW(_child224*2),HIGH(_child224*2),LOW(_child22*2),HIGH(_child22*2),0x0,0x0
	.DB  LOW(_child_22x_select),HIGH(_child_22x_select),0x0,0x0,0x0,0x0,0x0,LOW(_0xE0000*2+315)
	.DB  HIGH(_0xE0000*2+315)
_child222:
	.DB  LOW(_child223*2),HIGH(_child223*2),LOW(_child221*2),HIGH(_child221*2),LOW(_child22*2),HIGH(_child22*2),0x0,0x0
	.DB  LOW(_child_22x_select),HIGH(_child_22x_select),0x0,0x0,0x0,0x0,0x11,LOW(_0xE0000*2+330)
	.DB  HIGH(_0xE0000*2+330)
_child223:
	.DB  LOW(_child224*2),HIGH(_child224*2),LOW(_child222*2),HIGH(_child222*2),LOW(_child22*2),HIGH(_child22*2),0x0,0x0
	.DB  LOW(_child_22x_select),HIGH(_child_22x_select),0x0,0x0,0x0,0x0,0x32,LOW(_0xE0000*2+345)
	.DB  HIGH(_0xE0000*2+345)
_child224:
	.DB  LOW(_child221*2),HIGH(_child221*2),LOW(_child223*2),HIGH(_child223*2),LOW(_child22*2),HIGH(_child22*2),0x0,0x0
	.DB  LOW(_child_22x_select),HIGH(_child_22x_select),0x0,0x0,0x0,0x0,0x64,LOW(_0xE0000*2+360)
	.DB  HIGH(_0xE0000*2+360)
_child31:
	.DB  LOW(_child32*2),HIGH(_child32*2),LOW(_child34*2),HIGH(_child34*2),LOW(_main3*2),HIGH(_main3*2),0x0,0x0
	.DB  LOW(_child_3x_select),HIGH(_child_3x_select),0x0,0x0,0x0,0x0,0x0,LOW(_0xE0000*2+375)
	.DB  HIGH(_0xE0000*2+375)
_child32:
	.DB  LOW(_child33*2),HIGH(_child33*2),LOW(_child31*2),HIGH(_child31*2),LOW(_main3*2),HIGH(_main3*2),0x0,0x0
	.DB  LOW(_child_3x_select),HIGH(_child_3x_select),0x0,0x0,0x0,0x0,0x1,LOW(_0xE0000*2+390)
	.DB  HIGH(_0xE0000*2+390)
_child33:
	.DB  LOW(_child34*2),HIGH(_child34*2),LOW(_child32*2),HIGH(_child32*2),LOW(_main3*2),HIGH(_main3*2),0x0,0x0
	.DB  LOW(_child_3x_select),HIGH(_child_3x_select),0x0,0x0,0x0,0x0,0x2,LOW(_0xE0000*2+405)
	.DB  HIGH(_0xE0000*2+405)
_child34:
	.DB  LOW(_child31*2),HIGH(_child31*2),LOW(_child33*2),HIGH(_child33*2),LOW(_main3*2),HIGH(_main3*2),0x0,0x0
	.DB  LOW(_child_3x_select),HIGH(_child_3x_select),0x0,0x0,0x0,0x0,0x3,LOW(_0xE0000*2+420)
	.DB  HIGH(_0xE0000*2+420)
_child41:
	.DB  LOW(_child42*2),HIGH(_child42*2),LOW(_child42*2),HIGH(_child42*2),LOW(_main4*2),HIGH(_main4*2),0x0,0x0
	.DB  LOW(_child_4x_select),HIGH(_child_4x_select),0x0,0x0,0x0,0x0,0x0,LOW(_0xE0000*2+435)
	.DB  HIGH(_0xE0000*2+435)
_child42:
	.DB  LOW(_child41*2),HIGH(_child41*2),LOW(_child41*2),HIGH(_child41*2),LOW(_main4*2),HIGH(_main4*2),0x0,0x0
	.DB  LOW(_child_4x_select),HIGH(_child_4x_select),0x0,0x0,0x0,0x0,0x1,LOW(_0xE0000*2+450)
	.DB  HIGH(_0xE0000*2+450)
_child51:
	.DB  LOW(_child52*2),HIGH(_child52*2),LOW(_child51*2),HIGH(_child51*2),LOW(_main5*2),HIGH(_main5*2),0x0,0x0
	.DB  LOW(_child_5x_select),HIGH(_child_5x_select),0x0,0x0,0x0,0x0,0x0,LOW(_0xE0000*2+465)
	.DB  HIGH(_0xE0000*2+465)
_child52:
	.DB  LOW(_child51*2),HIGH(_child51*2),LOW(_child52*2),HIGH(_child52*2),LOW(_main5*2),HIGH(_main5*2),0x0,0x0
	.DB  LOW(_child_5x_select),HIGH(_child_5x_select),0x0,0x0,0x0,0x0,0x1,LOW(_0xE0000*2+480)
	.DB  HIGH(_0xE0000*2+480)
_child61:
	.DB  LOW(_child62*2),HIGH(_child62*2),LOW(_child62*2),HIGH(_child62*2),LOW(_main6*2),HIGH(_main6*2),0x0,0x0
	.DB  LOW(_child_6x_select),HIGH(_child_6x_select),0x0,0x0,0x0,0x0,0x0,LOW(_0xE0000*2+495)
	.DB  HIGH(_0xE0000*2+495)
_child62:
	.DB  LOW(_child61*2),HIGH(_child61*2),LOW(_child61*2),HIGH(_child61*2),LOW(_main6*2),HIGH(_main6*2),0x0,0x0
	.DB  LOW(_child_6x_select),HIGH(_child_6x_select),0x0,0x0,0x0,0x0,0x1,LOW(_0xE0000*2+510)
	.DB  HIGH(_0xE0000*2+510)
_child71:
	.DB  LOW(_child72*2),HIGH(_child72*2),LOW(_child72*2),HIGH(_child72*2),LOW(_main7*2),HIGH(_main7*2),0x0,0x0
	.DB  LOW(_child_7x_select),HIGH(_child_7x_select),0x0,0x0,0x0,0x0,0x1,LOW(_0xE0000*2+525)
	.DB  HIGH(_0xE0000*2+525)
_child72:
	.DB  LOW(_child71*2),HIGH(_child71*2),LOW(_child71*2),HIGH(_child71*2),LOW(_main7*2),HIGH(_main7*2),0x0,0x0
	.DB  LOW(_child_7x_select),HIGH(_child_7x_select),0x0,0x0,0x0,0x0,0x0,LOW(_0xE0000*2+540)
	.DB  HIGH(_0xE0000*2+540)
_err_str:
	.DB  0x65,0x72,0x72,0x20,0x0
_unkown_str:
	.DB  0x78,0x78,0x78,0x0
_depth_min_str:
	.DB  0x6D,0x69,0x6E,0x3E,0x0
_depth_med_str:
	.DB  0x6D,0x65,0x64,0x3E,0x0
_depth_max_str:
	.DB  0x6D,0x61,0x78,0x3E,0x0
_depth_m_r_str:
	.DB  0x72,0x65,0x61,0x72,0x0
_depth_m_f_str:
	.DB  0x66,0x72,0x6F,0x6E,0x0
_depth_m_all_str:
	.DB  0x61,0x6C,0x6C,0x20,0x0
_message_box_save_min:
	.DB  0x20,0x20,0x6D,0x69,0x6E,0x20,0x70,0x6F
	.DB  0x73,0x69,0x74,0x69,0x6F,0x6E,0x20,0x20
	.DB  0x20,0x20,0x20,0x20,0x20,0x73,0x61,0x76
	.DB  0x65,0x64,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x0
_message_box_save_med:
	.DB  0x20,0x20,0x6D,0x65,0x64,0x20,0x70,0x6F
	.DB  0x73,0x69,0x74,0x69,0x6F,0x6E,0x20,0x20
	.DB  0x20,0x20,0x20,0x20,0x20,0x73,0x61,0x76
	.DB  0x65,0x64,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x0
_message_box_save_max:
	.DB  0x20,0x20,0x6D,0x61,0x78,0x20,0x70,0x6F
	.DB  0x73,0x69,0x74,0x69,0x6F,0x6E,0x20,0x20
	.DB  0x20,0x20,0x20,0x20,0x20,0x73,0x61,0x76
	.DB  0x65,0x64,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x0
_communication_error_frame:
	.DB  0x5F,0x43,0x4F,0x4D,0x4D,0x55,0x4E,0x49
	.DB  0x43,0x41,0x54,0x49,0x4F,0x4E,0x5F,0x5F
	.DB  0x5F,0x5F,0x5F,0x5F,0x45,0x52,0x52,0x4F
	.DB  0x52,0x5F,0x5F,0x5F,0x5F,0x5F,0x5F,0x5F
	.DB  0x0
_main_template_frame:
	.DB  0x46,0x4C,0x78,0x78,0x78,0x55,0x78,0x78
	.DB  0x2E,0x78,0x76,0x46,0x52,0x78,0x78,0x78
	.DB  0x52,0x4C,0x78,0x78,0x78,0x5B,0x78,0x78
	.DB  0x78,0x78,0x5D,0x52,0x52,0x78,0x78,0x78
	.DB  0x0
_error_sensor_frame:
	.DB  0x73,0x65,0x6E,0x73,0x6F,0x72,0x20,0x65
	.DB  0x72,0x72,0x6F,0x72,0x20,0x20,0x20,0x20
	.DB  0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x0
_calibrate_save_error_frame:
	.DB  0x63,0x61,0x6C,0x69,0x62,0x72,0x61,0x74
	.DB  0x65,0x20,0x6F,0x72,0x20,0x20,0x20,0x20
	.DB  0x20,0x20,0x73,0x61,0x76,0x65,0x20,0x65
	.DB  0x72,0x72,0x6F,0x72,0x20,0x20,0x20,0x20
	.DB  0x0
_low_power_frame:
	.DB  0x62,0x61,0x74,0x74,0x65,0x72,0x79,0x20
	.DB  0x6C,0x6F,0x77,0x20,0x20,0x20,0x20,0x20
	.DB  0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x0
_compressor_timeout_frame:
	.DB  0x63,0x6F,0x6D,0x70,0x72,0x65,0x73,0x73
	.DB  0x6F,0x72,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x20,0x20,0x20,0x20,0x74,0x69,0x6D,0x65
	.DB  0x6F,0x75,0x74,0x20,0x20,0x20,0x20,0x20
	.DB  0x0
_clearance_set_timeout_frame:
	.DB  0x63,0x6C,0x65,0x61,0x72,0x61,0x6E,0x63
	.DB  0x65,0x20,0x73,0x65,0x74,0x20,0x20,0x20
	.DB  0x20,0x20,0x20,0x20,0x74,0x69,0x6D,0x65
	.DB  0x6F,0x75,0x74,0x20,0x20,0x20,0x20,0x20
	.DB  0x0
_tbl10_G101:
	.DB  0x10,0x27,0xE8,0x3,0x64,0x0,0xA,0x0
	.DB  0x1,0x0
_tbl16_G101:
	.DB  0x0,0x10,0x0,0x1,0x10,0x0,0x1,0x0

_0x4000E:
	.DB  0x0
_0xE0000:
	.DB  0x63,0x61,0x6C,0x69,0x62,0x72,0x61,0x74
	.DB  0x65,0x20,0x20,0x20,0x20,0x20,0x0,0x74
	.DB  0x69,0x6D,0x65,0x20,0x4F,0x4E,0x2F,0x4F
	.DB  0x46,0x46,0x20,0x20,0x20,0x0,0x62,0x61
	.DB  0x74,0x74,0x65,0x72,0x79,0x20,0x63,0x74
	.DB  0x72,0x6C,0x20,0x20,0x0,0x63,0x6F,0x6D
	.DB  0x70,0x72,0x65,0x73,0x73,0x6F,0x72,0x20
	.DB  0x20,0x20,0x20,0x0,0x64,0x6F,0x6F,0x72
	.DB  0x20,0x63,0x6F,0x6E,0x74,0x72,0x6F,0x6C
	.DB  0x20,0x20,0x0,0x62,0x72,0x61,0x6B,0x65
	.DB  0x20,0x63,0x6F,0x6E,0x74,0x72,0x6F,0x6C
	.DB  0x20,0x0,0x73,0x61,0x76,0x65,0x2F,0x72
	.DB  0x65,0x73,0x65,0x74,0x20,0x20,0x20,0x20
	.DB  0x0,0x41,0x4C,0x4C,0x20,0x55,0x50,0x2D
	.DB  0x44,0x4F,0x57,0x4E,0x20,0x20,0x20,0x0
	.DB  0x46,0x20,0x55,0x50,0x2D,0x44,0x4F,0x57
	.DB  0x4E,0x20,0x20,0x20,0x20,0x20,0x0,0x52
	.DB  0x20,0x55,0x50,0x2D,0x44,0x4F,0x57,0x4E
	.DB  0x20,0x20,0x20,0x20,0x20,0x0,0x46,0x4C
	.DB  0x20,0x55,0x50,0x2D,0x44,0x4F,0x57,0x4E
	.DB  0x20,0x20,0x20,0x20,0x0,0x46,0x52,0x20
	.DB  0x55,0x50,0x2D,0x44,0x4F,0x57,0x4E,0x20
	.DB  0x20,0x20,0x20,0x0,0x52,0x4C,0x20,0x55
	.DB  0x50,0x2D,0x44,0x4F,0x57,0x4E,0x20,0x20
	.DB  0x20,0x20,0x0,0x52,0x52,0x20,0x55,0x50
	.DB  0x2D,0x44,0x4F,0x57,0x4E,0x20,0x20,0x20
	.DB  0x20,0x0,0x74,0x69,0x6D,0x65,0x20,0x4F
	.DB  0x4E,0x20,0x76,0x61,0x6C,0x76,0x65,0x20
	.DB  0x0,0x74,0x69,0x6D,0x65,0x20,0x4F,0x46
	.DB  0x46,0x20,0x76,0x61,0x6C,0x76,0x65,0x0
	.DB  0x63,0x6F,0x6E,0x74,0x69,0x6E,0x75,0x6F
	.DB  0x75,0x73,0x6C,0x79,0x20,0x20,0x0,0x35
	.DB  0x20,0x6D,0x73,0x20,0x20,0x20,0x20,0x20
	.DB  0x20,0x20,0x20,0x20,0x20,0x0,0x37,0x20
	.DB  0x6D,0x73,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x20,0x20,0x20,0x20,0x0,0x31,0x30,0x20
	.DB  0x6D,0x73,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x20,0x20,0x20,0x0,0x31,0x32,0x20,0x6D
	.DB  0x73,0x20,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x20,0x20,0x0,0x30,0x20,0x6D,0x73,0x20
	.DB  0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x20,0x0,0x31,0x37,0x20,0x6D,0x73,0x20
	.DB  0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x0,0x35,0x30,0x20,0x6D,0x73,0x20,0x20
	.DB  0x20,0x20,0x20,0x20,0x20,0x20,0x20,0x0
	.DB  0x31,0x30,0x30,0x20,0x6D,0x73,0x20,0x20
	.DB  0x20,0x20,0x20,0x20,0x20,0x20,0x0,0x6F
	.DB  0x66,0x66,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x20,0x20,0x20,0x20,0x20,0x0,0x31,0x30
	.DB  0x2E,0x30,0x20,0x56,0x20,0x20,0x20,0x20
	.DB  0x20,0x20,0x20,0x20,0x0,0x31,0x31,0x2E
	.DB  0x30,0x20,0x56,0x20,0x20,0x20,0x20,0x20
	.DB  0x20,0x20,0x20,0x0,0x31,0x32,0x2E,0x30
	.DB  0x20,0x56,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x20,0x20,0x0,0x63,0x6F,0x6D,0x70,0x72
	.DB  0x20,0x63,0x74,0x72,0x6C,0x20,0x4F,0x46
	.DB  0x46,0x0,0x63,0x6F,0x6D,0x70,0x72,0x20
	.DB  0x63,0x74,0x72,0x6C,0x20,0x4F,0x4E,0x20
	.DB  0x0,0x64,0x6F,0x6F,0x72,0x20,0x63,0x74
	.DB  0x72,0x6C,0x20,0x4F,0x46,0x46,0x20,0x0
	.DB  0x64,0x6F,0x6F,0x72,0x20,0x63,0x74,0x72
	.DB  0x6C,0x20,0x4F,0x4E,0x20,0x20,0x0,0x62
	.DB  0x72,0x61,0x6B,0x65,0x20,0x63,0x74,0x72
	.DB  0x6C,0x20,0x4F,0x46,0x46,0x0,0x62,0x72
	.DB  0x61,0x6B,0x65,0x20,0x63,0x74,0x72,0x6C
	.DB  0x20,0x4F,0x4E,0x20,0x0,0x73,0x61,0x76
	.DB  0x65,0x20,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x20,0x20,0x20,0x0,0x72,0x65,0x73,0x65
	.DB  0x74,0x20,0x20,0x20,0x20,0x20,0x20,0x20
	.DB  0x20,0x20,0x0
_0x16001E:
	.DB  0x3,0x0,0x0,0x0,0x0,0x0,0x0,0x0
_0x1E0003:
	.DB  0x2F,0x10,0x11,0x13,0x12,0x17
_0x240003:
	.DB  0x1,0x0,0x0,0x0,0x2,0x0,0x0,0x0
	.DB  0x4,0x0,0x0,0x0,0x40,0x0,0x0,0x0
	.DB  0x8,0x0,0x0,0x0,0x10,0x0,0x0,0x0
	.DB  0x20
_0x240006:
	.DB  0x0
_0x2000003:
	.DB  0x80,0xC0
_0x2060060:
	.DB  0x1
_0x2060000:
	.DB  0x2D,0x4E,0x41,0x4E,0x0,0x49,0x4E,0x46
	.DB  0x0

__GLOBAL_INI_TBL:
	.DW  0x01
	.DW  0x05
	.DW  _0x4000E*2

	.DW  0x08
	.DW  0x07
	.DW  _0x16001E*2

	.DW  0x06
	.DW  _poll_list
	.DW  _0x1E0003*2

	.DW  0x19
	.DW  _error_list
	.DW  _0x240003*2

	.DW  0x01
	.DW  0x0D
	.DW  _0x240006*2

	.DW  0x02
	.DW  __base_y_G100
	.DW  _0x2000003*2

	.DW  0x01
	.DW  __seed_G103
	.DW  _0x2060060*2

_0xFFFFFFFF:
	.DW  0

__RESET:
	CLI
	CLR  R30
	OUT  EECR,R30

;INTERRUPT VECTORS ARE PLACED
;AT THE START OF FLASH
	LDI  R31,1
	OUT  GICR,R31
	OUT  GICR,R30
	OUT  MCUCR,R30

;DISABLE WATCHDOG
	LDI  R31,0x18
	OUT  WDTCR,R31
	OUT  WDTCR,R30

;CLEAR R2-R14
	LDI  R24,(14-2)+1
	LDI  R26,2
	CLR  R27
__CLEAR_REG:
	ST   X+,R30
	DEC  R24
	BRNE __CLEAR_REG

;CLEAR SRAM
	LDI  R24,LOW(__CLEAR_SRAM_SIZE)
	LDI  R25,HIGH(__CLEAR_SRAM_SIZE)
	LDI  R26,__SRAM_START
__CLEAR_SRAM:
	ST   X+,R30
	SBIW R24,1
	BRNE __CLEAR_SRAM

;GLOBAL VARIABLES INITIALIZATION
	LDI  R30,LOW(__GLOBAL_INI_TBL*2)
	LDI  R31,HIGH(__GLOBAL_INI_TBL*2)
__GLOBAL_INI_NEXT:
	LPM  R24,Z+
	LPM  R25,Z+
	SBIW R24,0
	BREQ __GLOBAL_INI_END
	LPM  R26,Z+
	LPM  R27,Z+
	LPM  R0,Z+
	LPM  R1,Z+
	MOVW R22,R30
	MOVW R30,R0
__GLOBAL_INI_LOOP:
	LPM  R0,Z+
	ST   X+,R0
	SBIW R24,1
	BRNE __GLOBAL_INI_LOOP
	MOVW R30,R22
	RJMP __GLOBAL_INI_NEXT
__GLOBAL_INI_END:

;HARDWARE STACK POINTER INITIALIZATION
	LDI  R30,LOW(__SRAM_END-__HEAP_SIZE)
	OUT  SPL,R30
	LDI  R30,HIGH(__SRAM_END-__HEAP_SIZE)
	OUT  SPH,R30

;DATA STACK POINTER INITIALIZATION
	LDI  R28,LOW(__SRAM_START+__DSTACK_SIZE)
	LDI  R29,HIGH(__SRAM_START+__DSTACK_SIZE)

	RJMP _main

	.ESEG
	.ORG 0

	.DSEG
	.ORG 0x160

	.CSEG
;
;#include <mega8.h>
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;#include <alcd.h>
;#include <stdio.h>
;#include <string.h>
;#include <delay.h>
;#include "timer.h"
;#include "button.h"
;#include "view.h"
;#include "ctrl.h"
;
;
;void mcu_init(void);
;void gpio_init(void);
;
;
;
;interrupt [EXT_INT1] void ext_int1_isr(void)
; 0000 0013 {

	.CSEG
_ext_int1_isr:
	RCALL SUBOPT_0x0
; 0000 0014     button_handler();
	RCALL _button_handler
; 0000 0015 }
	RJMP _0x7
;
;
;interrupt [USART_RXC] void usart_rx_isr(void)
; 0000 0019 {
_usart_rx_isr:
	RCALL SUBOPT_0x0
; 0000 001A     uart_rx_isr_handler();
	RCALL _uart_rx_isr_handler
; 0000 001B }
	RJMP _0x7
;
;
;interrupt [USART_DRE] void usart_tx_isr(void)
; 0000 001F {
_usart_tx_isr:
	RCALL SUBOPT_0x0
; 0000 0020     uart_tx_isr_handler();
	RCALL _uart_tx_isr_handler
; 0000 0021 }
	RJMP _0x7
;
;
;interrupt [TIM0_OVF] void timer0_ovf_isr(void)
; 0000 0025 {
_timer0_ovf_isr:
	RCALL SUBOPT_0x0
; 0000 0026     TCNT0=0x06; // reload
	LDI  R30,LOW(6)
	OUT  0x32,R30
; 0000 0027     tim_handler();
	RCALL _tim_handler
; 0000 0028     button_timer_handler();
	RCALL _button_timer_handler
; 0000 0029 }
	RJMP _0x7
;
;interrupt [TIM2_OVF] void timer2_ovf_isr(void)
; 0000 002C {
_timer2_ovf_isr:
	RCALL SUBOPT_0x0
; 0000 002D     TCNT2=0x06;
	LDI  R30,LOW(6)
	OUT  0x24,R30
; 0000 002E     proto_tim_handler();
	RCALL _proto_tim_handler
; 0000 002F }
_0x7:
	LD   R30,Y+
	OUT  SREG,R30
	LD   R31,Y+
	LD   R30,Y+
	LD   R27,Y+
	LD   R26,Y+
	LD   R25,Y+
	LD   R24,Y+
	LD   R23,Y+
	LD   R22,Y+
	LD   R15,Y+
	LD   R1,Y+
	LD   R0,Y+
	RETI
;
;//-------------------------------------------------------------------------------------//
;
;
;void main(void)
; 0000 0035 {
_main:
; 0000 0036     mcu_init();
	RCALL _mcu_init
; 0000 0037     gpio_init();
	RCALL _gpio_init
; 0000 0038     tim_init();
	RCALL _tim_init
; 0000 0039     uart_init();
	RCALL _uart_init
; 0000 003A     proto_init();
	RCALL _proto_init
; 0000 003B     lcd_init(16);
	LDI  R30,LOW(16)
	ST   -Y,R30
	RCALL _lcd_init
; 0000 003C     menu_init();
	RCALL _menu_init
; 0000 003D     menu_exit();
	RCALL _menu_exit
; 0000 003E     #asm("sei")
	sei
; 0000 003F 
; 0000 0040     while (1)
_0x3:
; 0000 0041     {
; 0000 0042         ctrl_task();
	RCALL _ctrl_task
; 0000 0043     }
	RJMP _0x3
; 0000 0044 
; 0000 0045 }
_0x6:
	RJMP _0x6
;
;
;void gpio_init (void)
; 0000 0049 {
_gpio_init:
; 0000 004A     PORTB = 0x03;
	LDI  R30,LOW(3)
	OUT  0x18,R30
; 0000 004B     DDRB = 0x3C;
	LDI  R30,LOW(60)
	OUT  0x17,R30
; 0000 004C     PORTC = 0x00;
	LDI  R30,LOW(0)
	OUT  0x15,R30
; 0000 004D     DDRC = 0x00;
	OUT  0x14,R30
; 0000 004E     PORTD = 0xF4;
	LDI  R30,LOW(244)
	OUT  0x12,R30
; 0000 004F     DDRD = 0x00;
	LDI  R30,LOW(0)
	OUT  0x11,R30
; 0000 0050 }
	RET
;
;void mcu_init(void)
; 0000 0053 {
_mcu_init:
; 0000 0054 
; 0000 0055     GICR|=0x80;
	IN   R30,0x3B
	ORI  R30,0x80
	OUT  0x3B,R30
; 0000 0056     MCUCR=0x08;
	LDI  R30,LOW(8)
	OUT  0x35,R30
; 0000 0057     GIFR=0x80;
	LDI  R30,LOW(128)
	OUT  0x3A,R30
; 0000 0058 
; 0000 0059     ACSR=0x80;
	OUT  0x8,R30
; 0000 005A     SFIOR=0x00;
	LDI  R30,LOW(0)
	OUT  0x30,R30
; 0000 005B     ADCSRA=0x00;
	OUT  0x6,R30
; 0000 005C     SPCR=0x00;
	OUT  0xD,R30
; 0000 005D     TWCR=0x00;
	OUT  0x36,R30
; 0000 005E }
	RET
;#include "buffer.h"
;
;
;/*
; *   Initialize a circular buffer of type buffer_type
; */
;void buffer_init(buffer_type *name_buffer, int size, uint8_t *buffer, uint8_t element_size, int max_pop_size)
; 0001 0008 {

	.CSEG
_buffer_init:
; 0001 0009     name_buffer->size = size;
;	*name_buffer -> Y+7
;	size -> Y+5
;	*buffer -> Y+3
;	element_size -> Y+2
;	max_pop_size -> Y+0
	LDD  R30,Y+5
	__PUTB1SNS 7,2
; 0001 000A     name_buffer->start = 0;
	LDD  R26,Y+7
	LDD  R27,Y+7+1
	RCALL SUBOPT_0x1
; 0001 000B     name_buffer->count = 0;
	ADIW R26,5
	RCALL SUBOPT_0x1
; 0001 000C     name_buffer->end = 0;
	ADIW R26,1
	LDI  R30,LOW(0)
	ST   X,R30
; 0001 000D     name_buffer->elem_size = element_size;
	LDD  R30,Y+2
	__PUTB1SNS 7,3
; 0001 000E     name_buffer->max_pop_size = max_pop_size;
	LD   R30,Y
	__PUTB1SNS 7,4
; 0001 000F     name_buffer->element = buffer;
	LDD  R30,Y+3
	LDD  R31,Y+3+1
	__PUTW1SNS 7,6
; 0001 0010 }
	ADIW R28,9
	RET
;
;/*
; *   flushes the buffer
; *   *buffer : pointer to ring buffer
; */
;void buffer_flash_queue(buffer_type *buffer)
; 0001 0017 {
_buffer_flash_queue:
; 0001 0018     buffer->start = buffer->end;  //the tail goes up to the head and buffer becomes empty
;	*buffer -> Y+0
	RCALL SUBOPT_0x2
	RCALL SUBOPT_0x3
	RCALL SUBOPT_0x2
	ST   X,R30
; 0001 0019     buffer->count = 0;
	RCALL SUBOPT_0x2
	ADIW R26,5
	LDI  R30,LOW(0)
	ST   X,R30
; 0001 001A }
	RJMP _0x20C0004
;
;/*
; * checks if the buffer is half full (empty)
; * status of ring buffer (=1 if half full =0 otherwise)
; */
;int buffer_is_halffull(buffer_type *buffer)
; 0001 0021 {
; 0001 0022   return buffer->count >= buffer->size - buffer->max_pop_size ? 1 : 0;
;	*buffer -> Y+0
; 0001 0023 }
;
;/*
; * is_half_empty
; * checks if the buffer is less than half
; * status of ring buffer (=1 if half empty =0 otherwise)
; */
;int buffer_is_halfempty(buffer_type *buffer)
; 0001 002B {
; 0001 002C   return (buffer->count <= buffer->max_pop_size) ? 1 : 0;
;	*buffer -> Y+0
; 0001 002D }
;
;/*
; * full
; * indicates if the given buffer is full or not
; * status of ring buffer (=1 if full =0 otherwise)
; */
;int buffer_is_full(buffer_type *buffer)
; 0001 0035 {
_buffer_is_full:
; 0001 0036   return buffer->count == buffer->size ? 1 : 0;
;	*buffer -> Y+0
	RCALL SUBOPT_0x4
	LDD  R0,Z+5
	RCALL SUBOPT_0x2
	ADIW R26,2
	LD   R30,X
	CP   R30,R0
	BRNE _0x20009
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	RJMP _0x2000A
_0x20009:
	RCALL SUBOPT_0x5
_0x2000A:
	RJMP _0x20C0004
; 0001 0037 }
;
;/*
; *  empty
; *  indicates if the given buffer is empty or not
; *  status of ring buffer (=1 if empty =0 otherwise)
; */
;int buffer_is_empty(buffer_type *buffer)
; 0001 003F {
_buffer_is_empty:
; 0001 0040   return buffer->count == 0 ? 1 : 0;
;	*buffer -> Y+0
	RCALL SUBOPT_0x4
	LDD  R26,Z+5
	CPI  R26,LOW(0x0)
	BRNE _0x2000C
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	RJMP _0x2000D
_0x2000C:
	RCALL SUBOPT_0x5
_0x2000D:
	RJMP _0x20C0004
; 0001 0041 }
;
;/*
; *  push_buffer_queue
; *  pushes a new item onto the circular buffer (queues it)
; *  data : value to be Q'ed in ring buffer.
; */
;void buffer_push_queue(buffer_type *buffer, uint8_t *data)
; 0001 0049 {
_buffer_push_queue:
; 0001 004A     if (buffer_is_full(buffer))
;	*buffer -> Y+2
;	*data -> Y+0
	RCALL SUBOPT_0x6
	RCALL SUBOPT_0x7
	RCALL _buffer_is_full
	SBIW R30,0
	BREQ _0x2000F
; 0001 004B     {
; 0001 004C       return;
	RJMP _0x20C000D
; 0001 004D     }
; 0001 004E     else
_0x2000F:
; 0001 004F     {
; 0001 0050         buffer->count+=buffer->elem_size;
	RCALL SUBOPT_0x8
	RCALL SUBOPT_0x9
; 0001 0051         memcpy(&buffer->element[buffer->end],data, buffer->elem_size);
	__GETWRZ 0,1,6
	RCALL SUBOPT_0xA
	RCALL SUBOPT_0x3
	RCALL SUBOPT_0xB
	ADD  R30,R0
	ADC  R31,R1
	RCALL SUBOPT_0x7
	RCALL SUBOPT_0xC
	RCALL SUBOPT_0xD
; 0001 0052         buffer->end+=buffer->elem_size;
	RCALL SUBOPT_0x6
	RCALL SUBOPT_0xE
	ADIW R26,3
	RCALL SUBOPT_0x9
; 0001 0053         buffer->end %= buffer->size;
	RCALL SUBOPT_0xE
	RCALL SUBOPT_0xF
; 0001 0054     }
; 0001 0055 
; 0001 0056 }
	RJMP _0x20C000D
;
;
;void  buffer_pop_queue(buffer_type *buffer, uint8_t *element)
; 0001 005A {
_buffer_pop_queue:
; 0001 005B 
; 0001 005C     if (buffer_is_empty(buffer))
;	*buffer -> Y+2
;	*element -> Y+0
	RCALL SUBOPT_0xC
	RCALL _buffer_is_empty
	SBIW R30,0
	BREQ _0x20011
; 0001 005D     {
; 0001 005E       return;
	RJMP _0x20C000D
; 0001 005F     }
; 0001 0060     else {
_0x20011:
; 0001 0061         /* First in First Out*/
; 0001 0062       memcpy(element, &buffer->element[buffer->start], buffer->elem_size);
	RCALL SUBOPT_0x10
	RCALL SUBOPT_0x11
	__GETWRZ 0,1,6
	RCALL SUBOPT_0x12
	LD   R30,X
	RCALL SUBOPT_0xB
	ADD  R30,R0
	ADC  R31,R1
	RCALL SUBOPT_0x7
	RCALL SUBOPT_0xD
; 0001 0063       buffer->start = buffer->start + buffer->elem_size;
	RCALL SUBOPT_0xA
	LD   R0,X
	RCALL SUBOPT_0xA
	ADIW R26,3
	LD   R30,X
	ADD  R30,R0
	RCALL SUBOPT_0xA
	ST   X,R30
; 0001 0064       buffer->count-=buffer->elem_size;
	RCALL SUBOPT_0x8
	LD   R26,X
	MOV  R30,R0
	SUB  R30,R26
	MOVW R26,R22
	ST   X,R30
; 0001 0065       buffer->start %= buffer->size;
	RCALL SUBOPT_0xA
	MOVW R22,R26
	LD   R0,X
	RCALL SUBOPT_0xA
	RCALL SUBOPT_0xF
; 0001 0066     }
; 0001 0067 
; 0001 0068 }
	RJMP _0x20C000D
;
;/*
; * rewinds_buffer_queue
; * rewinds the circular buffer
; * count : number of bytes to rewind
; */
;void buffer_rewinds_queue(buffer_type *buffer, int count)
; 0001 0070 {
; 0001 0071     int buf_end = buffer->end;
; 0001 0072     if(buffer->start - count >= 0)
;	*buffer -> Y+4
;	count -> Y+2
;	buf_end -> R16,R17
; 0001 0073       {
; 0001 0074           buffer->start = buffer->start - count;
; 0001 0075           if(buf_end > buffer->start)
; 0001 0076               buffer->count = buf_end - buffer->start;
; 0001 0077           else
; 0001 0078               buffer->count = (buffer->size-buffer->start)+buf_end;
; 0001 0079       }
; 0001 007A     else
; 0001 007B       {
; 0001 007C           buffer->start = buffer->size - (count - buffer->start);
; 0001 007D           buffer->count = (buffer->size - buffer->start)+ buf_end;
; 0001 007E       }
; 0001 007F }
;
;
;#include "button.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;
;
;uint16_t time_press;
;bool push_status;
;bool menu_pressed = 0;
;button_t last_push;
;
;
;
;void button_handler(void)
; 0002 000C {

	.CSEG
_button_handler:
; 0002 000D     if(BTN_DOWN_GPIO == 0x00)
	SBIC 0x16,1
	RJMP _0x40003
; 0002 000E         last_push = btn_down;
	LDI  R30,LOW(2)
	MOV  R8,R30
; 0002 000F     else if(BTN_UP_GPIO == 0x00)
	RJMP _0x40004
_0x40003:
	SBIC 0x16,0
	RJMP _0x40005
; 0002 0010         last_push = btn_up;
	LDI  R30,LOW(1)
	MOV  R8,R30
; 0002 0011     else if(BTN_RIGHT_GPIO == 0x00)
	RJMP _0x40006
_0x40005:
	SBIC 0x10,7
	RJMP _0x40007
; 0002 0012         last_push = btn_right;
	LDI  R30,LOW(3)
	MOV  R8,R30
; 0002 0013     else if(BTN_MENU_GPIO == 0x00)
	RJMP _0x40008
_0x40007:
	SBIC 0x10,6
	RJMP _0x40009
; 0002 0014     {
; 0002 0015         menu_pressed = true;
	LDI  R30,LOW(1)
	MOV  R5,R30
; 0002 0016         time_press  = tim_get_tic();
	RCALL _tim_get_tic
	__PUTW1R 3,4
; 0002 0017         push_status = false;
	RJMP _0x20C0012
; 0002 0018         return;
; 0002 0019     }
; 0002 001A     push_status = true;
_0x40009:
_0x40008:
_0x40006:
_0x40004:
	LDI  R30,LOW(1)
	MOV  R6,R30
; 0002 001B }
	RET
;
;void button_timer_handler (void)
; 0002 001E {
_button_timer_handler:
; 0002 001F     if(menu_pressed)
	TST  R5
	BREQ _0x4000A
; 0002 0020     {
; 0002 0021         if(BTN_MENU_GPIO == 0x00)
	SBIC 0x10,6
	RJMP _0x4000B
; 0002 0022         {
; 0002 0023             if(tim_get_time_mc(time_press) >= LONG_PRESS_TIME_TIC)
	__GETW1R 3,4
	RCALL SUBOPT_0x13
	__CPD1N 0x2BC
	BRLO _0x4000C
; 0002 0024             {
; 0002 0025                 menu_pressed = false;
	CLR  R5
; 0002 0026                 last_push = btn_long_menu;
	LDI  R30,LOW(5)
	MOV  R8,R30
; 0002 0027                 push_status = true;
	LDI  R30,LOW(1)
	MOV  R6,R30
; 0002 0028             }
; 0002 0029         } else{
_0x4000C:
	RJMP _0x4000D
_0x4000B:
; 0002 002A             menu_pressed = false;
	CLR  R5
; 0002 002B             push_status = true;
	LDI  R30,LOW(1)
	MOV  R6,R30
; 0002 002C             last_push = btn_menu;
	LDI  R30,LOW(4)
	MOV  R8,R30
; 0002 002D         }
_0x4000D:
; 0002 002E 
; 0002 002F     }
; 0002 0030 }
_0x4000A:
	RET
;
;
;
;bool button_is_pushed()
; 0002 0035 {
_button_is_pushed:
; 0002 0036     return push_status;
	MOV  R30,R6
	RET
; 0002 0037 }
;
;
;button_t button_last_push(void)
; 0002 003B {
_button_last_push:
; 0002 003C     return  last_push;
	MOV  R30,R8
	RET
; 0002 003D }
;
;void button_clear_info(void)
; 0002 0040 {
_button_clear_info:
; 0002 0041     push_status = false;
_0x20C0012:
	CLR  R6
; 0002 0042 }
	RET
;#include "chks.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;
;
;uint8_t chks_is_ok (uint8_t* packet)
; 0003 0005 {

	.CSEG
_chks_is_ok:
; 0003 0006     packet_t *pct = (packet_t *)packet;
; 0003 0007 
; 0003 0008     if(packet == NULL)
	RCALL __SAVELOCR2
;	*packet -> Y+2
;	*pct -> R16,R17
	__GETWRS 16,17,2
	RCALL SUBOPT_0x6
	SBIW R30,0
	BRNE _0x60003
; 0003 0009         return 0;
	LDI  R30,LOW(0)
	RCALL __LOADLOCR2
	RJMP _0x20C000D
; 0003 000A 
; 0003 000B     if (pct->chks != chks(packet))
_0x60003:
	MOVW R30,R16
	LDD  R26,Z+8
	PUSH R26
	RCALL SUBOPT_0xC
	RCALL _chks
	POP  R26
	CP   R30,R26
	BREQ _0x60004
; 0003 000C         return 0;
	LDI  R30,LOW(0)
	RCALL __LOADLOCR2
	RJMP _0x20C000D
; 0003 000D 
; 0003 000E     return 1;
_0x60004:
	LDI  R30,LOW(1)
	RCALL __LOADLOCR2
	RJMP _0x20C000D
; 0003 000F }
;
;uint8_t chks(uint8_t* packet)
; 0003 0012 {
_chks:
; 0003 0013     uint8_t i,chks = 0;
; 0003 0014     if (packet == NULL)
	RCALL __SAVELOCR2
;	*packet -> Y+2
;	i -> R17
;	chks -> R16
	LDI  R16,0
	RCALL SUBOPT_0x6
	SBIW R30,0
	BRNE _0x60005
; 0003 0015         return 0;
	LDI  R30,LOW(0)
	RCALL __LOADLOCR2
	RJMP _0x20C000D
; 0003 0016 
; 0003 0017     for(i = 0; i < PACKET_SIZE - 1; i++)
_0x60005:
	LDI  R17,LOW(0)
_0x60007:
	CPI  R17,8
	BRSH _0x60008
; 0003 0018     {
; 0003 0019         chks += packet[i];
	RCALL SUBOPT_0xA
	RCALL SUBOPT_0x14
	ADD  R16,R30
; 0003 001A     }
	SUBI R17,-1
	RJMP _0x60007
_0x60008:
; 0003 001B     chks = -chks;
	NEG  R16
; 0003 001C     return chks;
	MOV  R30,R16
	RCALL __LOADLOCR2
	RJMP _0x20C000D
; 0003 001D 
; 0003 001E }
;#include "data.h"
;
;
;
;data_t data_pool[56];
;
;
;uint32_t data(uint16_t addr)
; 0004 0009 {

	.CSEG
_data:
; 0004 000A     return data_pool[addr].value;
;	addr -> Y+0
	RCALL SUBOPT_0x15
	SUBI R30,LOW(-_data_pool)
	SBCI R31,HIGH(-_data_pool)
	MOVW R26,R30
	RCALL __GETD1P
	RJMP _0x20C0004
; 0004 000B }
;
;bool data_is_download(uint16_t addr)
; 0004 000E {
_data_is_download:
; 0004 000F     return data_pool[addr].download == 1;
;	addr -> Y+0
	RCALL SUBOPT_0x15
	__ADDW1MN _data_pool,4
	LD   R30,Z
	ANDI R30,LOW(0x1)
	LDI  R26,LOW(1)
	RCALL __EQB12
	RJMP _0x20C0004
; 0004 0010 }
;
;bool data_is_upload(uint16_t addr)
; 0004 0013 {
_data_is_upload:
; 0004 0014     return data_pool[addr].upload == 1;
;	addr -> Y+0
	RCALL SUBOPT_0x15
	RCALL SUBOPT_0x16
	LD   R30,Z
	LSR  R30
	ANDI R30,LOW(0x1)
	LDI  R26,LOW(1)
	RCALL __EQB12
	RJMP _0x20C0004
; 0004 0015 }
;
;void data_clear(uint16_t addr)
; 0004 0018 {
_data_clear:
; 0004 0019     data_pool[addr].upload = 0;
;	addr -> Y+0
	RCALL SUBOPT_0x15
	RCALL SUBOPT_0x16
	RCALL SUBOPT_0x17
	ANDI R30,0xFD
	ST   X,R30
; 0004 001A     data_pool[addr].download = 0;
	RCALL SUBOPT_0x15
	RCALL SUBOPT_0x16
	RCALL SUBOPT_0x17
	ANDI R30,0xFE
	ST   X,R30
; 0004 001B     //data_pool[addr].value = 0;
; 0004 001C }
	RJMP _0x20C0004
;
;void data_download_cb(uint16_t addr, uint32_t val)
; 0004 001F {
_data_download_cb:
; 0004 0020     data_pool[addr].download = true;
;	addr -> Y+4
;	val -> Y+0
	RCALL SUBOPT_0x18
	RCALL SUBOPT_0x16
	RCALL SUBOPT_0x17
	ORI  R30,1
	ST   X,R30
; 0004 0021     data_pool[addr].value = val ;
	RCALL SUBOPT_0x18
	RCALL SUBOPT_0x19
; 0004 0022 }
	RJMP _0x20C0002
;
;void data_upload_cb( uint16_t addr, uint32_t val)
; 0004 0025 {
_data_upload_cb:
; 0004 0026     data_pool[addr].upload = true;
;	addr -> Y+4
;	val -> Y+0
	RCALL SUBOPT_0x18
	RCALL SUBOPT_0x16
	RCALL SUBOPT_0x17
	ORI  R30,2
	ST   X,R30
; 0004 0027     data_pool[addr].value = val;
	RCALL SUBOPT_0x18
	RCALL SUBOPT_0x19
; 0004 0028 }
	RJMP _0x20C0002
;#include "debug.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;
;
;void _portb3(ctrl_t cmd)
; 0005 0005 {

	.CSEG
; 0005 0006     if(cmd == on)
;	cmd -> Y+0
; 0005 0007         PORTB |= (1 << PORTB3);
; 0005 0008     else
; 0005 0009         PORTB &= ~(1 << PORTB3);
; 0005 000A }
;
;void _portb4(ctrl_t cmd)
; 0005 000D {
; 0005 000E     if(cmd == on)
;	cmd -> Y+0
; 0005 000F         PORTB |= (1 << PORTB4);
; 0005 0010     else
; 0005 0011         PORTB &= ~(1 << PORTB4);
; 0005 0012 }
;
;void _portb5(ctrl_t cmd)
; 0005 0015 {
; 0005 0016     if(cmd == on)
;	cmd -> Y+0
; 0005 0017         PORTB |= (1 << PORTB5);
; 0005 0018     else
; 0005 0019         PORTB &= ~(1 << PORTB5);
; 0005 001A 
; 0005 001B }
;#include "lcd.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;
;
;
;void lcd_show(char *frame)
; 0006 0006 {

	.CSEG
_lcd_show:
; 0006 0007     uint8_t i;
; 0006 0008     lcd_clear();
	ST   -Y,R17
;	*frame -> Y+1
;	i -> R17
	RCALL _lcd_clear
; 0006 0009     lcd_gotoxy(0,0);
	RCALL SUBOPT_0x1A
	RCALL SUBOPT_0x1A
	RCALL _lcd_gotoxy
; 0006 000A     for(i = 0; i < LCD_SIZE ; i++ )
	LDI  R17,LOW(0)
_0xC0004:
	CPI  R17,32
	BRSH _0xC0005
; 0006 000B     {
; 0006 000C         lcd_putchar(frame[i]);
	RCALL SUBOPT_0x1B
	RCALL SUBOPT_0x14
	ST   -Y,R30
	RCALL _lcd_putchar
; 0006 000D     }
	SUBI R17,-1
	RJMP _0xC0004
_0xC0005:
; 0006 000E }
	RJMP _0x20C000A
;
;
;void lcd_backlight(ctrl_t ctrl)
; 0006 0012 {
_lcd_backlight:
; 0006 0013     if(ctrl == on)
;	ctrl -> Y+0
	LD   R30,Y
	CPI  R30,0
	BRNE _0xC0006
; 0006 0014         LCD_BACKLIGHT_PORT |= (1 << LCD_BACKLIGHT_CTRL_PIN);
	SBI  0x18,2
; 0006 0015     else if (ctrl == off)
	RJMP _0xC0007
_0xC0006:
	LD   R26,Y
	CPI  R26,LOW(0x1)
	BRNE _0xC0008
; 0006 0016         LCD_BACKLIGHT_PORT &= ~(1 << LCD_BACKLIGHT_CTRL_PIN);
	CBI  0x18,2
; 0006 0017 
; 0006 0018 }
_0xC0008:
_0xC0007:
	RJMP _0x20C0003
;
;
;void lcd_off(void)
; 0006 001C {
_lcd_off:
; 0006 001D     lcd_clear();
	RCALL _lcd_clear
; 0006 001E     lcd_backlight(off);
	LDI  R30,LOW(1)
	ST   -Y,R30
	RCALL _lcd_backlight
; 0006 001F }
	RET
;
;
;
;#include "menu.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;#include "button.h"
;#include "mfunc.h"
;
;menu_t menu;
;
;DECLARE_ITEM( main1 );
;DECLARE_ITEM( main2 );
;DECLARE_ITEM( main3 );
;DECLARE_ITEM( main4 );
;DECLARE_ITEM( main5 );
;DECLARE_ITEM( main6 );
;DECLARE_ITEM( main7 );
;
;   DECLARE_ITEM( child11 );
;   DECLARE_ITEM( child12 );
;   DECLARE_ITEM( child13 );
;   DECLARE_ITEM( child14 );
;   DECLARE_ITEM( child15 );
;   DECLARE_ITEM( child16 );
;   DECLARE_ITEM( child17 );
;
;   DECLARE_ITEM( child21 );
;         DECLARE_ITEM( child211 );
;         DECLARE_ITEM( child212 );
;         DECLARE_ITEM( child213 );
;         DECLARE_ITEM( child214 );
;         DECLARE_ITEM( child215 );
;
;   DECLARE_ITEM( child22 );
;         DECLARE_ITEM( child221 );
;         DECLARE_ITEM( child222 );
;         DECLARE_ITEM( child223 );
;         DECLARE_ITEM( child224 );
;
;   DECLARE_ITEM( child31 );
;   DECLARE_ITEM( child32 );
;   DECLARE_ITEM( child33 );
;   DECLARE_ITEM( child34 );
;
;   DECLARE_ITEM( child41 );
;   DECLARE_ITEM( child42 );
;
;
;   DECLARE_ITEM( child51 );
;   DECLARE_ITEM( child52 );
;
;
;   DECLARE_ITEM( child61 );
;   DECLARE_ITEM( child62 );
;
;   DECLARE_ITEM( child71 );
;   DECLARE_ITEM( child72 );
;
;//                      next,       prev,    parent,    child,        Select,           LongPressed,        /incr/decr/,         	item_value,              text
;MAKE_ITEM main1 =    {  &main2,     &main7,   NULL,    &child11,       NULL,        &main1_x_long_press,        NULL,                NOTUSED,           "calibrate     " };
;MAKE_ITEM main2 =    {  &main3,     &main1,   NULL,    &child21,       NULL,        &main1_x_long_press,        NULL,                NOTUSED,           "time ON/OFF   " };
;MAKE_ITEM main3=     {  &main4,     &main2,   NULL,    &child31,    &main_3_check,  &main1_x_long_press,        NULL,                NOTUSED,           "battery ctrl  " };
;MAKE_ITEM main4=     {  &main5,     &main3,   NULL,    &child41,    &main_4_check,  &main1_x_long_press,        NULL,                NOTUSED,           "compressor    " };
;MAKE_ITEM main5=     {  &main6,     &main4,   NULL,    &child51,    &main_5_check,  &main1_x_long_press,        NULL,                NOTUSED,           "door control  " };
;MAKE_ITEM main6=     {  &main7,     &main5,   NULL,    &child61,    &main_6_check,  &main1_x_long_press,        NULL,                NOTUSED,           "brake control " };
;MAKE_ITEM main7=     {  &main1,     &main6,   NULL,    &child71,       NULL,        &main1_x_long_press,        NULL,                NOTUSED,           "save/reset    " };
;
;MAKE_ITEM child11 =  { &child12,   &child17,  &main1,    NULL,	  &chlid_1x_select,	&child_1x_long_press, &chlid_1x_encr_decr, ADDR_CONTR_ALL_VALVE,    "ALL UP-DOWN   " };
;MAKE_ITEM child12 =  { &child13,   &child11,  &main1,    NULL,    &chlid_1x_select, &child_1x_long_press, &chlid_1x_encr_decr, ADDR_CONTR_F_VALVE,      "F UP-DOWN     " };
;MAKE_ITEM child13 =  { &child14,   &child12,  &main1,    NULL,    &chlid_1x_select, &child_1x_long_press, &chlid_1x_encr_decr, ADDR_CONTR_R_VALVE,      "R UP-DOWN     " };
;MAKE_ITEM child14 =  { &child15,   &child13,  &main1,    NULL,    &chlid_1x_select, &child_1x_long_press, &chlid_1x_encr_decr, ADDR_CONTR_FL_VALVE,     "FL UP-DOWN    " };
;MAKE_ITEM child15 =  { &child16,   &child14,  &main1,    NULL,    &chlid_1x_select, &child_1x_long_press, &chlid_1x_encr_decr, ADDR_CONTR_FR_VALVE,     "FR UP-DOWN    " };
;MAKE_ITEM child16 =  { &child17,   &child15,  &main1,    NULL,    &chlid_1x_select, &child_1x_long_press, &chlid_1x_encr_decr, ADDR_CONTR_RL_VALVE,     "RL UP-DOWN    " };
;MAKE_ITEM child17 =  { &child11,   &child16,  &main1,    NULL,    &chlid_1x_select, &child_1x_long_press, &chlid_1x_encr_decr, ADDR_CONTR_RR_VALVE,     "RR UP-DOWN    " };
;
;MAKE_ITEM child21 =  { &child22,   &child22,  &main2,  &child211,   &child_21_check,        NULL,               NULL,                NOTUSED,           "time ON valve " };
;MAKE_ITEM child22 =  { &child21,   &child21,  &main2,  &child221,   &child_22_check,        NULL,               NULL,                NOTUSED,           "time OFF valve" };
;
;MAKE_ITEM child211 = { &child212, &child215, &child21,   NULL,    &child_21x_select,        NULL,               NULL,           VALUE_VAL_ONTIME0,      "continuously  " };
;MAKE_ITEM child212 = { &child213, &child211, &child21,   NULL,    &child_21x_select,        NULL,               NULL,           VALUE_VAL_ONTIME5,      "5 ms          " };
;MAKE_ITEM child213 = { &child214, &child212, &child21,   NULL,    &child_21x_select,        NULL,               NULL,           VALUE_VAL_ONTIME7,      "7 ms          " };
;MAKE_ITEM child214 = { &child215, &child213, &child21,   NULL,    &child_21x_select,        NULL,               NULL,           VALUE_VAL_ONTIME10,     "10 ms         " };
;MAKE_ITEM child215 = { &child211, &child214, &child21,   NULL,    &child_21x_select,        NULL,               NULL,           VALUE_VAL_ONTIME12,     "12 ms         " };
;
;MAKE_ITEM child221 = { &child222, &child224, &child22,   NULL,    &child_22x_select,        NULL,               NULL,            VALUE_VAL_OFFTIME0,    "0 ms          " };
;MAKE_ITEM child222 = { &child223, &child221, &child22,   NULL,    &child_22x_select,        NULL,               NULL,           VALUE_VAL_OFFTIME17,    "17 ms         " };
;MAKE_ITEM child223 = { &child224, &child222, &child22,   NULL,    &child_22x_select,        NULL,               NULL,           VALUE_VAL_OFFTIME50,    "50 ms         " };
;MAKE_ITEM child224 = { &child221, &child223, &child22,   NULL,    &child_22x_select,        NULL,               NULL,           VALUE_VAL_OFFTIME100,   "100 ms        " };
;
;MAKE_ITEM child31 =  { &child32,  &child34,   &main3,    NULL,    &child_3x_select,         NULL,               NULL,           VALUE_TRSH_VOL_DISABLE, "off           " };
;MAKE_ITEM child32 =  { &child33,  &child31,   &main3,    NULL,    &child_3x_select,         NULL,               NULL,             VALUE_TRSH_VOL_10V,   "10.0 V        " };
;MAKE_ITEM child33 =  { &child34,  &child32,   &main3,    NULL,    &child_3x_select,         NULL,               NULL,             VALUE_TRSH_VOL_11V,   "11.0 V        " };
;MAKE_ITEM child34 =  { &child31,  &child33,   &main3,    NULL,    &child_3x_select,         NULL,               NULL,             VALUE_TRSH_VOL_12V,   "12.0 V        " };
;
;MAKE_ITEM child41 =  { &child42,  &child42,   &main4,    NULL,    &child_4x_select,         NULL,               NULL,              VALUE_COMP_OFF,      "compr ctrl OFF" };
;MAKE_ITEM child42=   { &child41,  &child41,   &main4,    NULL,    &child_4x_select,         NULL,               NULL,              VALUE_COMP_ON,       "compr ctrl ON " };
;
;MAKE_ITEM child51 =  { &child52,   &child51,  &main5,    NULL,    &child_5x_select,         NULL,               NULL,            VALUE_DOOR_CONTR_OFF,  "door ctrl OFF " };
;MAKE_ITEM child52 =  { &child51,   &child52,  &main5,    NULL,    &child_5x_select,         NULL,               NULL,            VALUE_DOOR_CONTR_ON,   "door ctrl ON  " };
;
;MAKE_ITEM child61 =  { &child62,   &child62,  &main6,    NULL,    &child_6x_select,         NULL,               NULL,             VALUE_STOP_DISABLE,   "brake ctrl OFF" };
;MAKE_ITEM child62 =  { &child61,   &child61,  &main6,    NULL,    &child_6x_select,         NULL,               NULL,             VALUE_STOP_ENABLE,    "brake ctrl ON " };
;
;MAKE_ITEM child71 =  { &child72,   &child72,  &main7,    NULL,    &child_7x_select,         NULL,               NULL,             VALUE_SAVE_SETTING,   "save          " };
;MAKE_ITEM child72 =  { &child71,   &child71,  &main7,    NULL,    &child_7x_select,         NULL,               NULL,            VALUE_RESET_SETTING,   "reset         " };
;
;
;void menu_init(void)
; 0007 0069 {

	.CSEG
_menu_init:
; 0007 006A     menu.exit = false;
	LDI  R30,LOW(0)
	__PUTB1MN _menu,40
; 0007 006B     menu.item = &main1;
	LDI  R30,LOW(_main1*2)
	LDI  R31,HIGH(_main1*2)
	RCALL SUBOPT_0x1C
; 0007 006C     menu.select_state = NOT_SELECT;
	RCALL SUBOPT_0x1D
; 0007 006D     menu.readed_data = NOTUSED;
	LDI  R30,LOW(255)
	__PUTB1MN _menu,1
; 0007 006E }
	RET
;
;void menu_enter_setect_state(uint8_t state)
; 0007 0071 {
_menu_enter_setect_state:
; 0007 0072     menu.select_state = state;
;	state -> Y+0
	LD   R30,Y
	__PUTB1MN _menu,5
; 0007 0073 }
	RJMP _0x20C0003
;
;void menu_update(void){
; 0007 0075 void menu_update(void){
_menu_update:
; 0007 0076 
; 0007 0077     MAKE_ITEM *next_item = menu.item->next;
; 0007 0078 
; 0007 0079     memset(menu.frame,' ',FRAME_SIZE);
	RCALL __SAVELOCR2
;	*next_item -> R16,R17
	RCALL SUBOPT_0x1E
	RCALL __GETW1PF
	MOVW R16,R30
	__POINTW1MN _menu,6
	RCALL SUBOPT_0x7
	LDI  R30,LOW(32)
	ST   -Y,R30
	RCALL SUBOPT_0x1F
	RCALL _memset
; 0007 007A     menu.frame[0]= SYMBOL_CURSOR;
	LDI  R30,LOW(62)
	__PUTB1MN _menu,6
; 0007 007B 
; 0007 007C 
; 0007 007D     if(menu.select_state == INCR_DECR_WHEN_SELECTED || menu.select_state == SHOW_CHOOSEN_VALUE)
	RCALL SUBOPT_0x20
	CPI  R26,LOW(0xFE)
	BREQ _0xE0004
	RCALL SUBOPT_0x20
	CPI  R26,LOW(0xFD)
	BRNE _0xE0003
_0xE0004:
; 0007 007E     {
; 0007 007F         if(menu.readed_data == menu.item->item_value)
	__GETB2MN _menu,1
	RCALL SUBOPT_0x1E
	ADIW R30,14
	LPM  R30,Z
	CP   R30,R26
	BRNE _0xE0006
; 0007 0080             menu.frame[FRAME_ROW - 1] = SYMBOL_SET;
	LDI  R30,LOW(111)
	__PUTB1MN _menu,21
; 0007 0081 
; 0007 0082         if(menu.readed_data == next_item->item_value)
_0xE0006:
	__GETB2MN _menu,1
	MOVW R30,R16
	ADIW R30,14
	LPM  R30,Z
	CP   R30,R26
	BRNE _0xE0007
; 0007 0083             menu.frame[FRAME_SIZE - 1] = SYMBOL_SET;
	LDI  R30,LOW(111)
	__PUTB1MN _menu,37
; 0007 0084     }
_0xE0007:
; 0007 0085     memcpyf(&menu.frame[1],menu.item->text,ITEM_TEXT_SIZE);
_0xE0003:
	__POINTW1MN _menu,7
	RCALL SUBOPT_0x7
	RCALL SUBOPT_0x1E
	RCALL SUBOPT_0x21
; 0007 0086     memcpyf(&menu.frame[FRAME_ROW + 1],next_item->text,ITEM_TEXT_SIZE);
	__POINTW1MN _menu,23
	RCALL SUBOPT_0x7
	MOVW R30,R16
	RCALL SUBOPT_0x21
; 0007 0087 }
	RJMP _0x20C0007
;
;char *menu_get_frame(void)
; 0007 008A {
_menu_get_frame:
; 0007 008B 	return menu.frame;
	__POINTW1MN _menu,6
	RET
; 0007 008C }
;
;
;void menu_enter_data(uint32_t data)
; 0007 0090 {
_menu_enter_data:
; 0007 0091 	menu.readed_data = data;
;	data -> Y+0
	LD   R30,Y
	__PUTB1MN _menu,1
; 0007 0092 }
	RJMP _0x20C000D
;
;uint32_t menu_get_item_value(void)
; 0007 0095 {
_menu_get_item_value:
; 0007 0096 	return menu.item->item_value;
	RCALL SUBOPT_0x1E
	ADIW R30,14
	LPM  R30,Z
	RCALL SUBOPT_0x22
	RET
; 0007 0097 }
;
;
;bool menu_item_select(void)
; 0007 009B {
_menu_item_select:
; 0007 009C 	if( menu.item->select_callback != NULL )
	RCALL SUBOPT_0x1E
	ADIW R30,8
	RCALL SUBOPT_0x23
	BREQ _0xE0008
; 0007 009D 	{
; 0007 009E 		((void(*)(void))menu.item->select_callback)();
	RCALL SUBOPT_0x1E
	ADIW R30,8
	RCALL __GETW1PF
	ICALL
; 0007 009F 		return true;
	RJMP _0x20C0010
; 0007 00A0 	};
_0xE0008:
; 0007 00A1 	return false;
	RJMP _0x20C0011
; 0007 00A2 }
;
;bool menu_item_incr_decr(void)
; 0007 00A5 {
_menu_item_incr_decr:
; 0007 00A6 	if( menu.item->incr_decr_callback != NULL )
	RCALL SUBOPT_0x1E
	ADIW R30,12
	RCALL SUBOPT_0x23
	BREQ _0xE0009
; 0007 00A7 	{
; 0007 00A8 		((void(*)(void))menu.item->incr_decr_callback)();
	RCALL SUBOPT_0x1E
	ADIW R30,12
	RCALL __GETW1PF
	ICALL
; 0007 00A9 		return true;
	RJMP _0x20C0010
; 0007 00AA 	};
_0xE0009:
; 0007 00AB 	return false;
_0x20C0011:
	LDI  R30,LOW(0)
	RET
; 0007 00AC }
;
;bool menu_item_long_press(void)
; 0007 00AF {
_menu_item_long_press:
; 0007 00B0 	if( menu.item->long_press_callback != NULL )
	RCALL SUBOPT_0x1E
	ADIW R30,10
	RCALL SUBOPT_0x23
	BREQ _0xE000A
; 0007 00B1 	{
; 0007 00B2 		((void(*)(void))menu.item->long_press_callback)();
	RCALL SUBOPT_0x1E
	ADIW R30,10
	RCALL __GETW1PF
	ICALL
; 0007 00B3 		return true;
; 0007 00B4 	};
_0xE000A:
; 0007 00B5 	return true;
_0x20C0010:
	LDI  R30,LOW(1)
	RET
; 0007 00B6 }
;
;void menu_navigate(uint8_t btn)
; 0007 00B9 {
_menu_navigate:
; 0007 00BA 	switch( btn )
;	btn -> Y+0
	LD   R30,Y
; 0007 00BB 	{
; 0007 00BC 		case btn_empty:
	CPI  R30,0
	BRNE _0xE000E
; 0007 00BD 			return;
	RJMP _0x20C0003
; 0007 00BE 			break;
; 0007 00BF 
; 0007 00C0 		case btn_down:
_0xE000E:
	CPI  R30,LOW(0x2)
	BRNE _0xE000F
; 0007 00C1 		    if(menu.select_state == NOT_SELECT || menu.select_state == SHOW_CHOOSEN_VALUE)
	RCALL SUBOPT_0x20
	CPI  R26,LOW(0x0)
	BREQ _0xE0011
	RCALL SUBOPT_0x20
	CPI  R26,LOW(0xFD)
	BRNE _0xE0010
_0xE0011:
; 0007 00C2             {
; 0007 00C3                 if( menu.item->next != NULL )
	RCALL SUBOPT_0x1E
	RCALL SUBOPT_0x23
	BREQ _0xE0013
; 0007 00C4                     menu.item = menu.item->next;
	RCALL SUBOPT_0x1E
	RCALL SUBOPT_0x24
; 0007 00C5 
; 0007 00C6             }else if(menu.select_state == INCR_DECR_WHEN_SELECTED)
_0xE0013:
	RJMP _0xE0014
_0xE0010:
	RCALL SUBOPT_0x20
	CPI  R26,LOW(0xFE)
	BRNE _0xE0015
; 0007 00C7             {
; 0007 00C8                 menu_item_incr_decr();
	RCALL _menu_item_incr_decr
; 0007 00C9             }
; 0007 00CA 
; 0007 00CB             break;
_0xE0015:
_0xE0014:
	RJMP _0xE000D
; 0007 00CC 
; 0007 00CD 		case btn_up:
_0xE000F:
	CPI  R30,LOW(0x1)
	BRNE _0xE0016
; 0007 00CE             if(menu.select_state == NOT_SELECT || menu.select_state == SHOW_CHOOSEN_VALUE)
	RCALL SUBOPT_0x20
	CPI  R26,LOW(0x0)
	BREQ _0xE0018
	RCALL SUBOPT_0x20
	CPI  R26,LOW(0xFD)
	BRNE _0xE0017
_0xE0018:
; 0007 00CF             {
; 0007 00D0                 if( menu.item->prev != NULL )
	RCALL SUBOPT_0x1E
	ADIW R30,2
	RCALL SUBOPT_0x23
	BREQ _0xE001A
; 0007 00D1                     menu.item = menu.item->prev;
	RCALL SUBOPT_0x1E
	ADIW R30,2
	RCALL SUBOPT_0x24
; 0007 00D2 
; 0007 00D3             }else if(menu.select_state == INCR_DECR_WHEN_SELECTED)
_0xE001A:
	RJMP _0xE001B
_0xE0017:
	RCALL SUBOPT_0x20
	CPI  R26,LOW(0xFE)
	BRNE _0xE001C
; 0007 00D4             {
; 0007 00D5                 menu_item_incr_decr();
	RCALL _menu_item_incr_decr
; 0007 00D6             }
; 0007 00D7 
; 0007 00D8             break;
_0xE001C:
_0xE001B:
	RJMP _0xE000D
; 0007 00D9 
; 0007 00DA 		case btn_right: // enter
_0xE0016:
	CPI  R30,LOW(0x3)
	BRNE _0xE001D
; 0007 00DB             if( menu.select_state == INCR_DECR_WHEN_SELECTED)
	RCALL SUBOPT_0x20
	CPI  R26,LOW(0xFE)
	BRNE _0xE001E
; 0007 00DC             {
; 0007 00DD                 menu.select_state = NOT_SELECT;
	RCALL SUBOPT_0x1D
; 0007 00DE                 return;
	RJMP _0x20C0003
; 0007 00DF             }
; 0007 00E0 
; 0007 00E1             menu_item_select();
_0xE001E:
	RCALL _menu_item_select
; 0007 00E2 
; 0007 00E3 			if( menu.item->child != NULL )
	RCALL SUBOPT_0x1E
	ADIW R30,6
	RCALL SUBOPT_0x23
	BREQ _0xE001F
; 0007 00E4                 menu.item = menu.item->child;
	RCALL SUBOPT_0x1E
	ADIW R30,6
	RCALL SUBOPT_0x24
; 0007 00E5 
; 0007 00E6 
; 0007 00E7 			break;
_0xE001F:
	RJMP _0xE000D
; 0007 00E8 
; 0007 00E9         case btn_menu:
_0xE001D:
	CPI  R30,LOW(0x4)
	BRNE _0xE0020
; 0007 00EA             if(menu.select_state != NOT_SELECT)
	__GETB1MN _menu,5
	CPI  R30,0
	BREQ _0xE0021
; 0007 00EB                 menu.select_state = NOT_SELECT;
	RCALL SUBOPT_0x1D
; 0007 00EC 
; 0007 00ED 			if( menu.item->parent != NULL )
_0xE0021:
	RCALL SUBOPT_0x1E
	ADIW R30,4
	RCALL SUBOPT_0x23
	BREQ _0xE0022
; 0007 00EE 				menu.item = menu.item->parent;
	RCALL SUBOPT_0x1E
	ADIW R30,4
	RCALL SUBOPT_0x24
; 0007 00EF 
; 0007 00F0 			break;
_0xE0022:
	RJMP _0xE000D
; 0007 00F1 
; 0007 00F2         case btn_long_menu:
_0xE0020:
	CPI  R30,LOW(0x5)
	BRNE _0xE000D
; 0007 00F3             if(menu_item_long_press())
	RCALL _menu_item_long_press
	CPI  R30,0
	BREQ _0xE0024
; 0007 00F4               return;
	RJMP _0x20C0003
; 0007 00F5 			break;
_0xE0024:
; 0007 00F6 
; 0007 00F7 	};
_0xE000D:
; 0007 00F8 }
	RJMP _0x20C0003
;
;void menu_set_message_box(flash char* messadge)
; 0007 00FB {
_menu_set_message_box:
; 0007 00FC     menu.message_box = messadge;
;	*messadge -> Y+0
	RCALL SUBOPT_0x4
	RCALL SUBOPT_0x25
; 0007 00FD }
	RJMP _0x20C0004
;
;void menu_clear_message_box(void)
; 0007 0100 {
_menu_clear_message_box:
; 0007 0101 	menu.message_box = NULL;
	RCALL SUBOPT_0x5
	RCALL SUBOPT_0x25
; 0007 0102 }
	RET
;
;flash char* menu_get_message_box(void)
; 0007 0105 {
_menu_get_message_box:
; 0007 0106     return menu.message_box;
	__GETW1MN _menu,38
	RET
; 0007 0107 }
;
;void menu_exit(void)
; 0007 010A {
_menu_exit:
; 0007 010B 	menu.exit = true;
	LDI  R30,LOW(1)
	__PUTB1MN _menu,40
; 0007 010C }
	RET
;
;bool menu_is_off(void)
; 0007 010F {
_menu_is_off:
; 0007 0110 	return menu.exit;
	__GETB1MN _menu,40
	RET
; 0007 0111 }
;#include "proto.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;
;proto_t proto;
;
;void proto_init(void)
; 0008 0006 {

	.CSEG
_proto_init:
; 0008 0007     proto.state = idle;
	LDI  R30,LOW(0)
	RCALL SUBOPT_0x26
; 0008 0008     proto.counter = 0;
	RCALL SUBOPT_0x27
; 0008 0009     buffer_init(&proto._request_buffer,REQUEST_BUFFER_SIZE,proto.request_buffer,
; 0008 000A                                                PAYLOAD_SIZE,REQUEST_BUFFER_SIZE/2);
	RCALL SUBOPT_0x28
	RCALL SUBOPT_0x29
	__POINTW1MN _proto,16
	RCALL SUBOPT_0x2A
; 0008 000B     buffer_init(&proto._response_buffer,RESPONSE_BUFFER_SIZE,proto.response_buffer,
; 0008 000C                                               PAYLOAD_SIZE,RESPONSE_BUFFER_SIZE/2);
	RCALL SUBOPT_0x2B
	RCALL SUBOPT_0x29
	__POINTW1MN _proto,72
	RCALL SUBOPT_0x2A
; 0008 000D     buffer_flash_queue(&proto._request_buffer);
	RCALL SUBOPT_0x28
	RCALL _buffer_flash_queue
; 0008 000E     buffer_flash_queue(&proto._response_buffer);
	RCALL SUBOPT_0x2B
	RCALL _buffer_flash_queue
; 0008 000F }
	RET
;
;void proto_responce(payload_t *payload)
; 0008 0012 {
_proto_responce:
; 0008 0013     #asm("cli")
;	*payload -> Y+0
	cli
; 0008 0014     buffer_push_queue(&proto._response_buffer,(uint8_t * )payload);
	RCALL SUBOPT_0x2B
	RCALL SUBOPT_0xC
	RCALL _buffer_push_queue
; 0008 0015     #asm("sei")
	sei
; 0008 0016 }
	RJMP _0x20C0004
;
;
;void proto_request( uint8_t type_command, uint16_t address, uint32_t data )
; 0008 001A {
_proto_request:
; 0008 001B     payload_t payload;
; 0008 001C     payload.value = data;
	SBIW R28,7
;	type_command -> Y+13
;	address -> Y+11
;	data -> Y+7
;	payload -> Y+0
	__GETD1S 7
	__PUTD1S 3
; 0008 001D     payload.address = address;
	LDD  R30,Y+11
	LDD  R31,Y+11+1
	STD  Y+1,R30
	STD  Y+1+1,R31
; 0008 001E     payload.type_command = type_command;
	LDD  R30,Y+13
	ST   Y,R30
; 0008 001F 
; 0008 0020     #asm("cli")
	cli
; 0008 0021     buffer_push_queue(&proto._request_buffer,(uint8_t*)&payload);
	RCALL SUBOPT_0x28
	RCALL SUBOPT_0x2C
	RCALL _buffer_push_queue
; 0008 0022     #asm("sei")
	sei
; 0008 0023 }
	ADIW R28,14
	RET
;
;
;void proto_pesponse_parser(payload_t * payload)
; 0008 0027 {
_proto_pesponse_parser:
; 0008 0028     uint32_t value;
; 0008 0029     switch (payload->type_command)
	SBIW R28,4
;	*payload -> Y+4
;	value -> Y+0
	RCALL SUBOPT_0x12
	LD   R30,X
; 0008 002A     {
; 0008 002B         case CMD_TYPE_READ_OK:
	CPI  R30,LOW(0x43)
	BRNE _0x100006
; 0008 002C             data_download_cb(payload->address,payload->value);
	RCALL SUBOPT_0x2D
	RCALL _data_download_cb
; 0008 002D             break;
	RJMP _0x100005
; 0008 002E         case CMD_TYPE_WRITE_OK:
_0x100006:
	CPI  R30,LOW(0x45)
	BRNE _0x100007
; 0008 002F             data_upload_cb(payload->address,payload->value);
	RCALL SUBOPT_0x2D
	RCALL _data_upload_cb
; 0008 0030             break;
	RJMP _0x100005
; 0008 0031         case CMD_TYPE_READ_ERROR:
_0x100007:
	CPI  R30,LOW(0x33)
	BRNE _0x100008
; 0008 0032             value = data(payload->address);
	RCALL SUBOPT_0x2E
	RCALL SUBOPT_0x2F
; 0008 0033             data_download_cb(payload->address,value);
	RCALL SUBOPT_0x30
	RCALL _data_download_cb
; 0008 0034             break;
	RJMP _0x100005
; 0008 0035         case CMD_TYPE_WRITE_ERROR:
_0x100008:
	CPI  R30,LOW(0x35)
	BRNE _0x10000A
; 0008 0036             value = data(payload->address);
	RCALL SUBOPT_0x2E
	RCALL SUBOPT_0x2F
; 0008 0037             data_upload_cb(payload->address,value);
	RCALL SUBOPT_0x30
	RCALL _data_upload_cb
; 0008 0038             break;
; 0008 0039         default:
_0x10000A:
; 0008 003A             break;
; 0008 003B     }
_0x100005:
; 0008 003C }
	RJMP _0x20C0002
;
;
;void proto_buf_worker(void)
; 0008 0040 {
_proto_buf_worker:
; 0008 0041     payload_t payload;
; 0008 0042     if(!buffer_is_empty(&proto._response_buffer))
	SBIW R28,7
;	payload -> Y+0
	RCALL SUBOPT_0x2B
	RCALL _buffer_is_empty
	SBIW R30,0
	BRNE _0x10000B
; 0008 0043     {
; 0008 0044         #asm("cli")
	cli
; 0008 0045         buffer_pop_queue(&proto._response_buffer,(uint8_t * )&payload);
	RCALL SUBOPT_0x2B
	RCALL SUBOPT_0x2C
	RCALL _buffer_pop_queue
; 0008 0046         #asm("sei")
	sei
; 0008 0047         proto_pesponse_parser(&payload);
	RCALL SUBOPT_0x31
	RCALL _proto_pesponse_parser
; 0008 0048     }
; 0008 0049     if(!(buffer_is_empty(&proto._request_buffer)) && (idle == uart_get_tx_state()) )
_0x10000B:
	RCALL SUBOPT_0x28
	RCALL _buffer_is_empty
	SBIW R30,0
	BRNE _0x10000D
	RCALL _uart_get_tx_state
	CPI  R30,0
	BREQ _0x10000E
_0x10000D:
	RJMP _0x10000C
_0x10000E:
; 0008 004A     {
; 0008 004B         #asm("cli")
	cli
; 0008 004C         buffer_pop_queue(&proto._request_buffer,(uint8_t*)&payload);
	RCALL SUBOPT_0x28
	RCALL SUBOPT_0x2C
	RCALL _buffer_pop_queue
; 0008 004D         #asm("sei")
	sei
; 0008 004E         uart_transmit(&payload);
	RCALL SUBOPT_0x31
	RCALL _uart_transmit
; 0008 004F     }
; 0008 0050 }
_0x10000C:
	RJMP _0x20C000E
;
;void proto_tim_handler(void)
; 0008 0053 {
_proto_tim_handler:
; 0008 0054     if(busy == uart_get_rx_state())
	RCALL _uart_get_rx_state
	CPI  R30,LOW(0x1)
	BRNE _0x10000F
; 0008 0055     {
; 0008 0056         proto.counter++;
	RCALL SUBOPT_0x32
	SBIW R30,1
; 0008 0057         if(proto.counter >= TIME_GETTING_PACKET)
	RCALL SUBOPT_0x33
	CPI  R26,LOW(0x96)
	LDI  R30,HIGH(0x96)
	CPC  R27,R30
	BRLO _0x100010
; 0008 0058         {
; 0008 0059             proto.state = error;
	LDI  R30,LOW(2)
	RCALL SUBOPT_0x26
; 0008 005A         }
; 0008 005B     } else if(proto.state == idle)
_0x100010:
	RJMP _0x100011
_0x10000F:
	__GETB1MN _proto,128
	CPI  R30,0
	BRNE _0x100012
; 0008 005C         proto.counter = 0;
	RCALL SUBOPT_0x27
; 0008 005D 
; 0008 005E     if(error == uart_get_rx_state())
_0x100012:
_0x100011:
	RCALL _uart_get_rx_state
	CPI  R30,LOW(0x2)
	BRNE _0x100013
; 0008 005F     {
; 0008 0060         proto.state = error;
	LDI  R30,LOW(2)
	RCALL SUBOPT_0x26
; 0008 0061     }
; 0008 0062 
; 0008 0063     if(proto.state == error)
_0x100013:
	__GETB2MN _proto,128
	CPI  R26,LOW(0x2)
	BRNE _0x100014
; 0008 0064     {
; 0008 0065         proto.counter ++ ;
	RCALL SUBOPT_0x32
; 0008 0066         if(proto.counter >= TIME_PROTO_REBOOT)
	RCALL SUBOPT_0x33
	CPI  R26,LOW(0x3E8)
	LDI  R30,HIGH(0x3E8)
	CPC  R27,R30
	BRLO _0x100015
; 0008 0067         {
; 0008 0068             uart_init();
	RCALL _uart_init
; 0008 0069             proto_init();
	RCALL _proto_init
; 0008 006A             proto.counter = 0;
	RCALL SUBOPT_0x27
; 0008 006B             proto.state = idle;
	LDI  R30,LOW(0)
	RCALL SUBOPT_0x26
; 0008 006C         }
; 0008 006D     }
_0x100015:
; 0008 006E     proto_buf_worker();
_0x100014:
	RCALL _proto_buf_worker
; 0008 006F }
	RET
;
;
;#include "timer.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;
;
;
;uint16_t count;
;
;
;void tim_init(void)
; 0009 0009 {

	.CSEG
_tim_init:
; 0009 000A 
; 0009 000B     ASSR=0x00;
	LDI  R30,LOW(0)
	OUT  0x22,R30
; 0009 000C     TCCR2=0x00;
	OUT  0x25,R30
; 0009 000D     TCNT2=0x00;
	OUT  0x24,R30
; 0009 000E     OCR2=0x00;
	OUT  0x23,R30
; 0009 000F 
; 0009 0010     TCCR0=(0<<CS02) | (1<<CS01) | (1<<CS00);
	LDI  R30,LOW(3)
	OUT  0x33,R30
; 0009 0011     TCNT0=0x06;
	LDI  R30,LOW(6)
	OUT  0x32,R30
; 0009 0012 
; 0009 0013     count = 0;
	CLR  R9
	CLR  R10
; 0009 0014 
; 0009 0015     ASSR=0<<AS2;
	LDI  R30,LOW(0)
	OUT  0x22,R30
; 0009 0016     TCCR2=(0<<PWM2) | (0<<COM21) | (0<<COM20) | (0<<CTC2) | (1<<CS22) | (0<<CS21) | (0<<CS20);
	LDI  R30,LOW(4)
	OUT  0x25,R30
; 0009 0017     TCNT2=0x06;
	LDI  R30,LOW(6)
	OUT  0x24,R30
; 0009 0018     OCR2=0x00;
	LDI  R30,LOW(0)
	OUT  0x23,R30
; 0009 0019 
; 0009 001A     TIMSK=(0<<OCIE2) | (1<<TOIE2) | (0<<TICIE1) | (0<<OCIE1A) | (0<<OCIE1B) | (0<<TOIE1) | (1<<TOIE0);
	LDI  R30,LOW(65)
	OUT  0x39,R30
; 0009 001B }
	RET
;
;void tim_handler(void)
; 0009 001E {
_tim_handler:
; 0009 001F     count++;
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	__ADDWRR 9,10,30,31
; 0009 0020 }
	RET
;
;
;uint16_t tim_get_tic(void)
; 0009 0024 {
_tim_get_tic:
; 0009 0025     return count;
	__GETW1R 9,10
	RET
; 0009 0026 }
;
;uint32_t tim_get_time_mc (uint32_t prev)
; 0009 0029 {
_tim_get_time_mc:
; 0009 002A     uint32_t delta = tim_get_tic() - prev;
; 0009 002B     if(delta < 0)
	SBIW R28,4
;	prev -> Y+4
;	delta -> Y+0
	RCALL _tim_get_tic
	MOVW R26,R30
	__GETD1S 4
	CLR  R24
	CLR  R25
	RCALL __SWAPD12
	RCALL __SUBD12
	RCALL SUBOPT_0x34
; 0009 002C         delta += 0xFFFF;
; 0009 002D 
; 0009 002E     return delta;
	RCALL SUBOPT_0x35
	RJMP _0x20C0005
; 0009 002F }
;#include "uart.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;#include "proto.h"
;
;
;
;
;com_t tx;
;com_t rx;
;
;
;void uart_init(void)
; 000A 000C {

	.CSEG
_uart_init:
; 000A 000D     memset(&tx.buffer,0,PACKET_SIZE);
	LDI  R30,LOW(_tx)
	LDI  R31,HIGH(_tx)
	RCALL SUBOPT_0x7
	RCALL SUBOPT_0x1A
	RCALL SUBOPT_0x36
; 000A 000E     memset(&rx.buffer,0,PACKET_SIZE);
	RCALL SUBOPT_0x37
	RCALL SUBOPT_0x1A
	RCALL SUBOPT_0x36
; 000A 000F     tx.counter = rx.counter = 0;
	LDI  R30,LOW(0)
	__PUTB1MN _rx,9
	__PUTB1MN _tx,9
; 000A 0010     tx.state = rx.state = idle;
	RCALL SUBOPT_0x38
	__PUTB1MN _tx,10
; 000A 0011     UCSRA=(0<<RXC) | (0<<TXC) | (0<<UDRE) | (0<<FE) | (0<<DOR) | (0<<UPE) | (0<<U2X) | (0<<MPCM);
	LDI  R30,LOW(0)
	OUT  0xB,R30
; 000A 0012     UCSRB=(1<<RXCIE) | (0<<TXCIE) | (0<<UDRIE) | (1<<RXEN) | (1<<TXEN) | (0<<UCSZ2) | (0<<RXB8) | (0<<TXB8);
	LDI  R30,LOW(152)
	OUT  0xA,R30
; 000A 0013     UCSRC=(1<<URSEL) | (0<<UMSEL) | (0<<UPM1) | (0<<UPM0) | (0<<USBS) | (1<<UCSZ1) | (1<<UCSZ0) | (0<<UCPOL);
	LDI  R30,LOW(134)
	OUT  0x20,R30
; 000A 0014     UBRRH=0x00;
	LDI  R30,LOW(0)
	OUT  0x20,R30
; 000A 0015     UBRRL=0x33;
	LDI  R30,LOW(51)
	OUT  0x9,R30
; 000A 0016 
; 000A 0017 }
	RET
;
;
;void uart_rx_isr_handler(void)
; 000A 001B {
_uart_rx_isr_handler:
; 000A 001C     packet_t * packet;
; 000A 001D     rx.state = busy;
	RCALL __SAVELOCR2
;	*packet -> R16,R17
	LDI  R30,LOW(1)
	RCALL SUBOPT_0x38
; 000A 001E     rx.buffer[rx.counter++] = UDR;
	__GETB1MN _rx,9
	SUBI R30,-LOW(1)
	__PUTB1MN _rx,9
	SUBI R30,LOW(1)
	RCALL SUBOPT_0xB
	SUBI R30,LOW(-_rx)
	SBCI R31,HIGH(-_rx)
	MOVW R26,R30
	IN   R30,0xC
	ST   X,R30
; 000A 001F 
; 000A 0020     if(rx.counter == PACKET_SIZE)
	__GETB2MN _rx,9
	CPI  R26,LOW(0x9)
	BRNE _0x140003
; 000A 0021     {
; 000A 0022         rx.counter = 0;
	LDI  R30,LOW(0)
	__PUTB1MN _rx,9
; 000A 0023 
; 000A 0024         packet = (packet_t *) rx.buffer;
	__POINTWRM 16,17,_rx
; 000A 0025 
; 000A 0026         if(chks_is_ok(rx.buffer) && (packet->id == SLAVE_ID))
	RCALL SUBOPT_0x37
	RCALL _chks_is_ok
	CPI  R30,0
	BREQ _0x140005
	MOVW R26,R16
	LD   R26,X
	CPI  R26,LOW(0x1)
	BREQ _0x140006
_0x140005:
	RJMP _0x140004
_0x140006:
; 000A 0027         {
; 000A 0028             proto_responce(&packet->payload);
	MOVW R30,R16
	ADIW R30,1
	RCALL SUBOPT_0x7
	RCALL _proto_responce
; 000A 0029             rx.state = idle;
	LDI  R30,LOW(0)
	RCALL SUBOPT_0x38
; 000A 002A 
; 000A 002B         } else {
	RJMP _0x140007
_0x140004:
; 000A 002C             if (!chks_is_ok(rx.buffer))
	RCALL SUBOPT_0x37
	RCALL _chks_is_ok
	CPI  R30,0
	BRNE _0x140008
; 000A 002D             {
; 000A 002E                 rx.state = error;
	LDI  R30,LOW(2)
	RCALL SUBOPT_0x38
; 000A 002F                 UCSRB &= ~(1 << RXCIE);
	CBI  0xA,7
; 000A 0030             }
; 000A 0031         }
_0x140008:
_0x140007:
; 000A 0032     }
; 000A 0033 }
_0x140003:
	RJMP _0x20C0007
;
;
;void uart_tx_isr_handler(void)
; 000A 0037 {
_uart_tx_isr_handler:
; 000A 0038     if(tx.counter < PACKET_SIZE)
	__GETB2MN _tx,9
	CPI  R26,LOW(0x9)
	BRSH _0x140009
; 000A 0039         UDR = tx.buffer[tx.counter++];
	__GETB1MN _tx,9
	SUBI R30,-LOW(1)
	__PUTB1MN _tx,9
	SUBI R30,LOW(1)
	RCALL SUBOPT_0xB
	SUBI R30,LOW(-_tx)
	SBCI R31,HIGH(-_tx)
	LD   R30,Z
	OUT  0xC,R30
; 000A 003A     else
	RJMP _0x14000A
_0x140009:
; 000A 003B         {
; 000A 003C             tx.state = idle;
	LDI  R30,LOW(0)
	__PUTB1MN _tx,10
; 000A 003D             UCSRB &= ~(1 << UDRIE);
	CBI  0xA,5
; 000A 003E         }
_0x14000A:
; 000A 003F }
	RET
;
;
;
;void uart_transmit(payload_t * payload)
; 000A 0044 {
_uart_transmit:
; 000A 0045 
; 000A 0046     packet_t packet;
; 000A 0047     if((tx.state != idle) || (payload == NULL))
	SBIW R28,9
;	*payload -> Y+9
;	packet -> Y+0
	__GETB2MN _tx,10
	CPI  R26,LOW(0x0)
	BRNE _0x14000C
	LDD  R26,Y+9
	LDD  R27,Y+9+1
	SBIW R26,0
	BRNE _0x14000B
_0x14000C:
; 000A 0048         return;
	RJMP _0x20C000F
; 000A 0049 
; 000A 004A     packet.id = MASTER_ID;
_0x14000B:
	LDI  R30,LOW(2)
	ST   Y,R30
; 000A 004B     memcpy(&packet.payload,payload,PAYLOAD_SIZE);
	MOVW R30,R28
	ADIW R30,1
	RCALL SUBOPT_0x7
	LDD  R30,Y+11
	LDD  R31,Y+11+1
	RCALL SUBOPT_0x7
	LDI  R30,LOW(7)
	LDI  R31,HIGH(7)
	RCALL SUBOPT_0x7
	RCALL _memcpy
; 000A 004C     packet.chks = chks((uint8_t*)&packet);
	RCALL SUBOPT_0x31
	RCALL _chks
	STD  Y+8,R30
; 000A 004D     memcpy(tx.buffer,&packet,PACKET_SIZE);
	LDI  R30,LOW(_tx)
	LDI  R31,HIGH(_tx)
	RCALL SUBOPT_0x7
	RCALL SUBOPT_0x2C
	LDI  R30,LOW(9)
	LDI  R31,HIGH(9)
	RCALL SUBOPT_0x7
	RCALL _memcpy
; 000A 004E     tx.counter = 0;
	LDI  R30,LOW(0)
	__PUTB1MN _tx,9
; 000A 004F     tx.state = busy;
	LDI  R30,LOW(1)
	__PUTB1MN _tx,10
; 000A 0050     UCSRB |= (1 << UDRIE);
	SBI  0xA,5
; 000A 0051 
; 000A 0052 }
_0x20C000F:
	ADIW R28,11
	RET
;
;
;uint8_t uart_get_tx_state(void)
; 000A 0056 {
_uart_get_tx_state:
; 000A 0057     return tx.state;
	__GETB1MN _tx,10
	RET
; 000A 0058 }
;
;uint8_t uart_get_rx_state(void)
; 000A 005B {
_uart_get_rx_state:
; 000A 005C     return rx.state;
	__GETB1MN _rx,10
	RET
; 000A 005D }
;
;#include "ctrl.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;#include "delay.h"
;#include "status.h"
;#include "timer.h"
;#include "data_cmd.h"
;#include "lcd.h"
;#include <mega8.h>
;
;#define RESTART_PAUSE 1000
;#define SHOW_ERROR_INTERVAL 5000
;#define POLL_STATUS_TIMEOUT_IN_SLEEP 200
;
;ctrl_state_t state = main;
;
;uint16_t show_error_timeout = 0;
;bool postpone_show = false;
;
;
;
;
;bool master_device_is_off(void)
; 000B 0016 {

	.CSEG
_master_device_is_off:
; 000B 0017     uint32_t data = get_data(ADDR_PARAM_POWER_STATE);
; 000B 0018     return data == VALUE_POWER_SLEEP ? true : false ;
	SBIW R28,4
;	data -> Y+0
	LDI  R30,LOW(54)
	LDI  R31,HIGH(54)
	RCALL SUBOPT_0x39
	RCALL SUBOPT_0x34
	RCALL SUBOPT_0x3A
	BRNE _0x160003
	LDI  R30,LOW(1)
	RJMP _0x160004
_0x160003:
	LDI  R30,LOW(0)
_0x160004:
	RJMP _0x20C000D
; 000B 0019 }
;
;void ctrl_task(void)
; 000B 001C {
_ctrl_task:
; 000B 001D     uint32_t current_error = 0;
; 000B 001E     uint16_t status_timeout = 0;
; 000B 001F 
; 000B 0020     if(master_device_is_off())
	RCALL SUBOPT_0x3B
	RCALL __SAVELOCR2
;	current_error -> Y+2
;	status_timeout -> R16,R17
	__GETWRN 16,17,0
	RCALL _master_device_is_off
	CPI  R30,0
	BREQ _0x160006
; 000B 0021     {
; 000B 0022         state = sleep;
	LDI  R30,LOW(1)
	MOV  R7,R30
; 000B 0023         status_timeout = tim_get_tic();
	RCALL SUBOPT_0x3C
; 000B 0024     }
; 000B 0025 
; 000B 0026 
; 000B 0027     switch(state)
_0x160006:
	MOV  R30,R7
; 000B 0028     {
; 000B 0029         case main: view_main(); break;
	CPI  R30,LOW(0x3)
	BRNE _0x16000A
	RCALL _view_main
	RJMP _0x160009
; 000B 002A         case menu: view_menu(); break;
_0x16000A:
	CPI  R30,LOW(0x2)
	BRNE _0x16000B
	RCALL _view_menu
	RJMP _0x160009
; 000B 002B         case sleep:
_0x16000B:
	CPI  R30,LOW(0x1)
	BRNE _0x160009
; 000B 002C             lcd_off();
	RCALL _lcd_off
; 000B 002D             while (tim_get_time_mc(status_timeout) < POLL_STATUS_TIMEOUT_IN_SLEEP)
	RCALL SUBOPT_0x3D
	__CPD1N 0xC8
	BRSH _0x16000F
; 000B 002E             {
; 000B 002F                 state = main;
	LDI  R30,LOW(3)
	MOV  R7,R30
; 000B 0030                 return;
	RCALL __LOADLOCR2
	RJMP _0x20C0002
; 000B 0031             }
_0x16000F:
; 000B 0032             break;
; 000B 0033     }
_0x160009:
; 000B 0034 
; 000B 0035     if(menu_is_off())
	RCALL _menu_is_off
	CPI  R30,0
	BREQ _0x160010
; 000B 0036         state = main;
	LDI  R30,LOW(3)
	RJMP _0x16001D
; 000B 0037     else if(!master_device_is_off())
_0x160010:
	RCALL _master_device_is_off
	CPI  R30,0
	BRNE _0x160012
; 000B 0038         state = menu;
	LDI  R30,LOW(2)
_0x16001D:
	MOV  R7,R30
; 000B 0039 
; 000B 003A     if(state == main && !postpone_show)
_0x160012:
	LDI  R30,LOW(3)
	CP   R30,R7
	BRNE _0x160014
	TST  R14
	BREQ _0x160015
_0x160014:
	RJMP _0x160013
_0x160015:
; 000B 003B     {
; 000B 003C         current_error = status_get_next_error();
	RCALL _status_get_next_error
	RCALL SUBOPT_0x3E
; 000B 003D         if(current_error != 0)
	RCALL SUBOPT_0x3F
	RCALL __CPD10
	BREQ _0x160016
; 000B 003E         {
; 000B 003F             view_error(current_error);
	RCALL SUBOPT_0x30
	RCALL _view_error
; 000B 0040             show_error_timeout = tim_get_tic();
	RCALL _tim_get_tic
	__PUTW1R 11,12
; 000B 0041             postpone_show = true;
	LDI  R30,LOW(1)
	MOV  R14,R30
; 000B 0042         }
; 000B 0043     }
_0x160016:
; 000B 0044     if(postpone_show && tim_get_time_mc(show_error_timeout) > SHOW_ERROR_INTERVAL)
_0x160013:
	TST  R14
	BREQ _0x160018
	__GETW1R 11,12
	RCALL SUBOPT_0x13
	__CPD1N 0x1389
	BRSH _0x160019
_0x160018:
	RJMP _0x160017
_0x160019:
; 000B 0045     {
; 000B 0046         postpone_show = false;
	CLR  R14
; 000B 0047     }
; 000B 0048 
; 000B 0049 }
_0x160017:
	RCALL __LOADLOCR2
	RJMP _0x20C0002
;
;void ctrl_restart(void)
; 000B 004C {
_ctrl_restart:
; 000B 004D     delay_ms(RESTART_PAUSE);
	LDI  R30,LOW(1000)
	LDI  R31,HIGH(1000)
	RCALL SUBOPT_0x40
; 000B 004E     WDTCR=0x18;
	LDI  R30,LOW(24)
	OUT  0x21,R30
; 000B 004F     WDTCR=0x08;
	LDI  R30,LOW(8)
	OUT  0x21,R30
; 000B 0050     #asm("wdr")
	wdr
; 000B 0051     while(1);
_0x16001A:
	RJMP _0x16001A
; 000B 0052 }
;
;#include "data_cmd.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;#include "ctrl.h"
;
;
;uint32_t get_data(uint16_t addr)
; 000C 0006 {

	.CSEG
_get_data:
; 000C 0007     uint16_t count = tim_get_tic();
; 000C 0008     data_clear(addr);
	RCALL __SAVELOCR2
;	addr -> Y+2
;	count -> R16,R17
	RCALL SUBOPT_0x3C
	RCALL SUBOPT_0xC
	RCALL _data_clear
; 000C 0009     proto_request(CMD_TYPE_READ,addr,0);
	LDI  R30,LOW(3)
	ST   -Y,R30
	LDD  R30,Y+3
	LDD  R31,Y+3+1
	RCALL SUBOPT_0x41
; 000C 000A     while (!data_is_download(addr))
_0x180003:
	RCALL SUBOPT_0xC
	RCALL _data_is_download
	CPI  R30,0
	BRNE _0x180005
; 000C 000B     {
; 000C 000C         if(tim_get_time_mc(count) >= TIME_TRANSACTION)
	RCALL SUBOPT_0x3D
	RCALL SUBOPT_0x42
	BRLO _0x180006
; 000C 000D         {
; 000C 000E             ctrl_restart();
	RCALL _ctrl_restart
; 000C 000F             return 0 ;
	RCALL SUBOPT_0x43
	RCALL __LOADLOCR2
	RJMP _0x20C000D
; 000C 0010         }
; 000C 0011     }
_0x180006:
	RJMP _0x180003
_0x180005:
; 000C 0012     return  data(addr);
	RCALL SUBOPT_0xC
	RCALL _data
	RCALL __LOADLOCR2
	RJMP _0x20C000D
; 000C 0013 
; 000C 0014 }
;
;
;void set_data(uint16_t addr, uint32_t data)
; 000C 0018 {
_set_data:
; 000C 0019     uint16_t count = tim_get_tic();
; 000C 001A     data_clear(addr);
	RCALL __SAVELOCR2
;	addr -> Y+6
;	data -> Y+2
;	count -> R16,R17
	RCALL SUBOPT_0x3C
	RCALL SUBOPT_0x44
	RCALL _data_clear
; 000C 001B     proto_request(CMD_TYPE_WRITE,addr,data);
	LDI  R30,LOW(5)
	ST   -Y,R30
	LDD  R30,Y+7
	LDD  R31,Y+7+1
	RCALL SUBOPT_0x7
	__GETD1S 5
	RCALL __PUTPARD1
	RCALL _proto_request
; 000C 001C     while (!data_is_upload(addr))
_0x180007:
	RCALL SUBOPT_0x44
	RCALL _data_is_upload
	CPI  R30,0
	BRNE _0x180009
; 000C 001D     {
; 000C 001E         if(tim_get_time_mc(count) >= TIME_TRANSACTION)
	RCALL SUBOPT_0x3D
	RCALL SUBOPT_0x42
	BRLO _0x18000A
; 000C 001F         {
; 000C 0020            ctrl_restart();
	RCALL _ctrl_restart
; 000C 0021            return  ;
	RCALL __LOADLOCR2
	RJMP _0x20C0005
; 000C 0022         }
; 000C 0023     }
_0x18000A:
	RJMP _0x180007
_0x180009:
; 000C 0024 }
	RCALL __LOADLOCR2
	RJMP _0x20C0005
;#include "data_cnvtr.h"
;
;
;flash char err_str[] = {"err "};
;flash char unkown_str[] = {"xxx"};
;flash char depth_min_str[] = {"min>"};
;flash char depth_med_str[] = {"med>"};
;flash char depth_max_str[] = {"max>"};
;flash char depth_m_r_str[] = {"rear"};
;flash char depth_m_f_str[] = {"fron"};
;flash char depth_m_all_str[] = {"all "};
;
;float int_to_float(uint32_t b)
; 000D 000E {

	.CSEG
_int_to_float:
; 000D 000F     return (float) b * 10000 / 27306666;
;	b -> Y+0
	RCALL SUBOPT_0x35
	RCALL __CDF1U
	__GETD2N 0x461C4000
	RCALL __MULF12
	MOVW R26,R30
	MOVW R24,R22
	__GETD1N 0x4BD05555
	RCALL __DIVF21
	RJMP _0x20C000D
; 000D 0010 }
;
;void convert_percent_to_str(uint32_t data, uint8_t *str)
; 000D 0013 {
_convert_percent_to_str:
; 000D 0014     uint8_t count;
; 000D 0015     if(data >100 || data == VALUE_PERSENT_UNKOWN)
	ST   -Y,R17
;	data -> Y+3
;	*str -> Y+1
;	count -> R17
	RCALL SUBOPT_0x45
	__CPD2N 0x65
	BRSH _0x1A0004
	RCALL SUBOPT_0x45
	__CPD2N 0xFFFFFFFF
	BRNE _0x1A0003
_0x1A0004:
; 000D 0016     {
; 000D 0017       memcpyf(str,unkown_str,3);
	LDD  R30,Y+1
	LDD  R31,Y+1+1
	RCALL SUBOPT_0x7
	LDI  R30,LOW(_unkown_str*2)
	LDI  R31,HIGH(_unkown_str*2)
	RCALL SUBOPT_0x7
	LDI  R30,LOW(3)
	LDI  R31,HIGH(3)
	RCALL SUBOPT_0x7
	RCALL _memcpyf
; 000D 0018     } else {
	RJMP _0x1A0006
_0x1A0003:
; 000D 0019         str[0] = data/100;
	RCALL SUBOPT_0x45
	__GETD1N 0x64
	RCALL __DIVD21U
	RCALL SUBOPT_0x1B
	ST   X,R30
; 000D 001A         str[1] = (data-str[0]*100)/10;
	RCALL SUBOPT_0x46
	RCALL __SUBD21
	__GETD1N 0xA
	RCALL __DIVD21U
	__PUTB1SNS 1,1
; 000D 001B         str[2] = data-str[0]*100-str[1]*10;
	RCALL SUBOPT_0x46
	RCALL __SWAPD12
	RCALL __SUBD12
	PUSH R23
	PUSH R22
	PUSH R31
	PUSH R30
	RCALL SUBOPT_0x1B
	RCALL SUBOPT_0x3
	LDI  R26,LOW(10)
	RCALL SUBOPT_0x47
	POP  R26
	POP  R27
	POP  R24
	POP  R25
	RCALL SUBOPT_0x22
	RCALL __SWAPD12
	RCALL __SUBD12
	__PUTB1SNS 1,2
; 000D 001C         for(count = 0 ; count < 3; count++)
	LDI  R17,LOW(0)
_0x1A0008:
	CPI  R17,3
	BRSH _0x1A0009
; 000D 001D         {
; 000D 001E             str[count]+=0x30;
	RCALL SUBOPT_0x1B
	RCALL SUBOPT_0x14
	SUBI R30,-LOW(48)
	ST   X,R30
; 000D 001F         }
	SUBI R17,-1
	RJMP _0x1A0008
_0x1A0009:
; 000D 0020     }
_0x1A0006:
; 000D 0021 }
	LDD  R17,Y+0
_0x20C000E:
	ADIW R28,7
	RET
;
;void convert_voltage_data_to_str(uint32_t data,uint8_t *str)
; 000D 0024 {
_convert_voltage_data_to_str:
; 000D 0025     float voltage = int_to_float(data);
; 000D 0026     uint8_t voltage_int;
; 000D 0027     uint8_t count;
; 000D 0028     if(voltage > 25.0f || voltage < 7.0f)
	SBIW R28,4
	RCALL __SAVELOCR2
;	data -> Y+8
;	*str -> Y+6
;	voltage -> Y+2
;	voltage_int -> R17
;	count -> R16
	__GETD1S 8
	RCALL __PUTPARD1
	RCALL _int_to_float
	RCALL SUBOPT_0x3E
	RCALL SUBOPT_0x48
	__GETD1N 0x41C80000
	RCALL __CMPF12
	BREQ PC+3
	BRCS PC+2
	RJMP _0x1A000B
	RCALL SUBOPT_0x48
	__GETD1N 0x40E00000
	RCALL __CMPF12
	BRSH _0x1A000A
_0x1A000B:
; 000D 0029     {
; 000D 002A       memcpyf(str,err_str,4);
	RCALL SUBOPT_0x44
	LDI  R30,LOW(_err_str*2)
	LDI  R31,HIGH(_err_str*2)
	RCALL SUBOPT_0x7
	RCALL SUBOPT_0x49
; 000D 002B     } else {
	RJMP _0x1A000D
_0x1A000A:
; 000D 002C 
; 000D 002D         voltage *= 10.0f;
	RCALL SUBOPT_0x48
	__GETD1N 0x41200000
	RCALL __MULF12
	RCALL SUBOPT_0x3E
; 000D 002E         voltage_int  = (uint8_t) voltage;
	RCALL SUBOPT_0x3F
	RCALL __CFD1U
	MOV  R17,R30
; 000D 002F         str[0] =  voltage_int / 100;
	MOV  R26,R17
	LDI  R30,LOW(100)
	RCALL __DIVB21U
	RCALL SUBOPT_0x4A
	ST   X,R30
; 000D 0030         str[1] = (voltage_int - str[0] * 100) / 10;
	RCALL SUBOPT_0x4B
	MOV  R26,R30
	LDI  R30,LOW(10)
	RCALL __DIVB21U
	__PUTB1SNS 6,1
; 000D 0031         str[3] =  voltage_int - str[0] * 100 - str[1] * 10;
	RCALL SUBOPT_0x4B
	MOV  R22,R30
	RCALL SUBOPT_0x4A
	RCALL SUBOPT_0x3
	LDI  R26,LOW(10)
	RCALL SUBOPT_0x47
	MOV  R26,R30
	MOV  R30,R22
	SUB  R30,R26
	__PUTB1SNS 6,3
; 000D 0032         for(count = 0; count < 4; count++)
	LDI  R16,LOW(0)
_0x1A000F:
	CPI  R16,4
	BRSH _0x1A0010
; 000D 0033         {
; 000D 0034             str[count]+=0x30;
	RCALL SUBOPT_0x4A
	CLR  R30
	ADD  R26,R16
	ADC  R27,R30
	LD   R30,X
	SUBI R30,-LOW(48)
	ST   X,R30
; 000D 0035         }
	SUBI R16,-1
	RJMP _0x1A000F
_0x1A0010:
; 000D 0036         str[2]='.';
	RCALL SUBOPT_0x4A
	ADIW R26,2
	LDI  R30,LOW(46)
	ST   X,R30
; 000D 0037     }
_0x1A000D:
; 000D 0038 }
	RCALL __LOADLOCR2
	ADIW R28,12
	RET
;
;void convert_depth_mode_to_str( uint32_t data, uint8_t *str)
; 000D 003B {
_convert_depth_mode_to_str:
; 000D 003C 
; 000D 003D     if(data == VALUE_DEPTH_MIN)
;	data -> Y+2
;	*str -> Y+0
	RCALL SUBOPT_0x3F
	RCALL __CPD10
	BRNE _0x1A0011
; 000D 003E         memcpyf(str,depth_min_str,4);
	RCALL SUBOPT_0x10
	LDI  R30,LOW(_depth_min_str*2)
	LDI  R31,HIGH(_depth_min_str*2)
	RJMP _0x1A001C
; 000D 003F     else if(data == VALUE_DEPTH_MED)
_0x1A0011:
	RCALL SUBOPT_0x48
	RCALL SUBOPT_0x3A
	BRNE _0x1A0013
; 000D 0040         memcpyf(str,depth_med_str,4);
	RCALL SUBOPT_0x10
	LDI  R30,LOW(_depth_med_str*2)
	LDI  R31,HIGH(_depth_med_str*2)
	RJMP _0x1A001C
; 000D 0041     else if(data == VALUE_DEPTH_MAX)
_0x1A0013:
	RCALL SUBOPT_0x48
	RCALL SUBOPT_0x4C
	BRNE _0x1A0015
; 000D 0042         memcpyf(str,depth_max_str,4);
	RCALL SUBOPT_0x10
	LDI  R30,LOW(_depth_max_str*2)
	LDI  R31,HIGH(_depth_max_str*2)
	RJMP _0x1A001C
; 000D 0043     else if(data == VALUE_DEPTH_MALL)
_0x1A0015:
	RCALL SUBOPT_0x48
	RCALL SUBOPT_0x4D
	BRNE _0x1A0017
; 000D 0044         memcpyf(str,depth_m_all_str,4);
	RCALL SUBOPT_0x10
	LDI  R30,LOW(_depth_m_all_str*2)
	LDI  R31,HIGH(_depth_m_all_str*2)
	RJMP _0x1A001C
; 000D 0045     else if(data == VALUE_DEPTH_MR)
_0x1A0017:
	RCALL SUBOPT_0x48
	RCALL SUBOPT_0x4E
	BRNE _0x1A0019
; 000D 0046         memcpyf(str,depth_m_r_str,4);
	RCALL SUBOPT_0x10
	LDI  R30,LOW(_depth_m_r_str*2)
	LDI  R31,HIGH(_depth_m_r_str*2)
	RJMP _0x1A001C
; 000D 0047     else if(data == VALUE_DEPTH_MF)
_0x1A0019:
	RCALL SUBOPT_0x48
	RCALL SUBOPT_0x4F
	BRNE _0x1A001B
; 000D 0048         memcpyf(str,depth_m_f_str,4);
	RCALL SUBOPT_0x10
	LDI  R30,LOW(_depth_m_f_str*2)
	LDI  R31,HIGH(_depth_m_f_str*2)
_0x1A001C:
	ST   -Y,R31
	ST   -Y,R30
	RCALL SUBOPT_0x49
; 000D 0049 
; 000D 004A }
_0x1A001B:
	RJMP _0x20C0002
;
;
;
;
;#include "mfunc.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;
;
;
;flash char message_box_save_min[] = {"  min position       saved      "};
;flash char message_box_save_med[] = {"  med position       saved      "};
;flash char message_box_save_max[] = {"  max position       saved      "};
;
;
;void main_3_check(void)
; 000E 000B {

	.CSEG
_main_3_check:
; 000E 000C     uint32_t data = get_data(ADDR_PARAM_VOLT_SYS_THRS);
; 000E 000D     menu_enter_setect_state(SHOW_CHOOSEN_VALUE);
	SBIW R28,4
;	data -> Y+0
	LDI  R30,LOW(49)
	LDI  R31,HIGH(49)
	RCALL SUBOPT_0x39
	RCALL SUBOPT_0x50
	RJMP _0x20C000C
; 000E 000E     menu_enter_data(data);
; 000E 000F }
;
;void main_4_check(void)
; 000E 0012 {
_main_4_check:
; 000E 0013     uint32_t data = get_data(ADDR_PARAM_COMPR_CONTROL);
; 000E 0014     menu_enter_setect_state(SHOW_CHOOSEN_VALUE);
	SBIW R28,4
;	data -> Y+0
	LDI  R30,LOW(11)
	LDI  R31,HIGH(11)
	RCALL SUBOPT_0x39
	RCALL SUBOPT_0x50
	RJMP _0x20C000C
; 000E 0015     menu_enter_data(data);
; 000E 0016 
; 000E 0017 }
;
;void main_5_check(void)
; 000E 001A {
_main_5_check:
; 000E 001B     uint32_t data = get_data(ADDR_PARAM_CONTROL_DOOR);
; 000E 001C     menu_enter_setect_state(SHOW_CHOOSEN_VALUE);
	SBIW R28,4
;	data -> Y+0
	LDI  R30,LOW(51)
	LDI  R31,HIGH(51)
	RCALL SUBOPT_0x39
	RCALL SUBOPT_0x50
	RJMP _0x20C000C
; 000E 001D     menu_enter_data(data);
; 000E 001E }
;
;void main_6_check(void)
; 000E 0021 {
_main_6_check:
; 000E 0022     uint32_t data = get_data(ADDR_PARAM_STOP_SIG_CONTR);
; 000E 0023     menu_enter_setect_state(SHOW_CHOOSEN_VALUE);
	SBIW R28,4
;	data -> Y+0
	LDI  R30,LOW(53)
	LDI  R31,HIGH(53)
	RCALL SUBOPT_0x39
	RCALL SUBOPT_0x50
	RJMP _0x20C000C
; 000E 0024     menu_enter_data(data);
; 000E 0025 }
;
;
;void main1_x_long_press(void)
; 000E 0029 {
_main1_x_long_press:
; 000E 002A     menu_exit();
	RCALL _menu_exit
; 000E 002B }
	RET
;
;
;void chlid_1x_encr_decr(void)
; 000E 002F {
_chlid_1x_encr_decr:
; 000E 0030     uint16_t addr = menu_get_item_value();
; 000E 0031     button_t button = button_last_push();
; 000E 0032     uint32_t data  = 0;
; 000E 0033 
; 000E 0034     if(button == btn_down)
	RCALL SUBOPT_0x3B
	RCALL __SAVELOCR4
;	addr -> R16,R17
;	button -> R19
;	data -> Y+4
	RCALL _menu_get_item_value
	MOVW R16,R30
	RCALL _button_last_push
	MOV  R19,R30
	CPI  R19,2
	BRNE _0x1C0003
; 000E 0035         data = ACTION_VALVE_EX;
	RCALL SUBOPT_0x51
; 000E 0036     else if(button == btn_up)
	RJMP _0x1C0004
_0x1C0003:
	CPI  R19,1
	BRNE _0x1C0005
; 000E 0037         data = ACTION_VALVE_IN;
	RCALL SUBOPT_0x52
	RCALL SUBOPT_0x53
; 000E 0038 
; 000E 0039     set_data(addr,data);
_0x1C0005:
_0x1C0004:
	ST   -Y,R17
	ST   -Y,R16
	RCALL SUBOPT_0x54
; 000E 003A }
	RCALL __LOADLOCR4
	RJMP _0x20C0005
;
;
;
;
;void child_1x_long_press(void)
; 000E 0040 {
_child_1x_long_press:
; 000E 0041 
; 000E 0042 
; 000E 0043     uint32_t data = get_data(ADDR_PARAM_SAVE_DAMPS_VALUE);
; 000E 0044     switch (data)
	SBIW R28,4
;	data -> Y+0
	RCALL SUBOPT_0x55
	RCALL SUBOPT_0x39
	RCALL SUBOPT_0x56
	RCALL SUBOPT_0x35
; 000E 0045     {
; 000E 0046         case VALUE_DONT_SAVE_DAMPS:
	RCALL __CPD10
	BRNE _0x1C0009
; 000E 0047             set_data(ADDR_PARAM_SAVE_DAMPS_VALUE,VALUE_SAVE_MIN_DAMPS);
	RCALL SUBOPT_0x55
	RCALL SUBOPT_0x7
	RCALL SUBOPT_0x52
	RCALL SUBOPT_0x57
; 000E 0048             menu_set_message_box(message_box_save_min);
	LDI  R30,LOW(_message_box_save_min*2)
	LDI  R31,HIGH(_message_box_save_min*2)
	RCALL SUBOPT_0x7
	RCALL _menu_set_message_box
; 000E 0049             break;
	RJMP _0x1C0008
; 000E 004A         case VALUE_SAVE_MIN_DAMPS:
_0x1C0009:
	RCALL SUBOPT_0x58
	BRNE _0x1C000A
; 000E 004B             set_data(ADDR_PARAM_SAVE_DAMPS_VALUE,VALUE_SAVE_MED_DAMPS);
	RCALL SUBOPT_0x55
	RCALL SUBOPT_0x59
	RCALL SUBOPT_0x57
; 000E 004C             menu_set_message_box(message_box_save_med);
	LDI  R30,LOW(_message_box_save_med*2)
	LDI  R31,HIGH(_message_box_save_med*2)
	RCALL SUBOPT_0x7
	RCALL _menu_set_message_box
; 000E 004D             break;
	RJMP _0x1C0008
; 000E 004E         case VALUE_SAVE_MED_DAMPS:
_0x1C000A:
	RCALL SUBOPT_0x5A
	BRNE _0x1C000B
; 000E 004F             set_data(ADDR_PARAM_SAVE_DAMPS_VALUE,VALUE_SAVE_MAX_DAMPS);
	RCALL SUBOPT_0x55
	RCALL SUBOPT_0x7
	RCALL SUBOPT_0x5B
	RCALL SUBOPT_0x57
; 000E 0050             menu_set_message_box(message_box_save_max);
	LDI  R30,LOW(_message_box_save_max*2)
	LDI  R31,HIGH(_message_box_save_max*2)
	RCALL SUBOPT_0x7
	RCALL _menu_set_message_box
; 000E 0051             break;
	RJMP _0x1C0008
; 000E 0052         case VALUE_SAVE_MAX_DAMPS:
_0x1C000B:
	__CPD1N 0x3
	BRNE _0x1C0008
; 000E 0053             return;
	RJMP _0x20C000D
; 000E 0054     }
_0x1C0008:
; 000E 0055 }
	RJMP _0x20C000D
;
;
;
;void chlid_1x_select(void)
; 000E 005A {
_chlid_1x_select:
; 000E 005B     uint16_t data = menu_get_item_value();
; 000E 005C     menu_enter_setect_state(INCR_DECR_WHEN_SELECTED);
	RCALL __SAVELOCR2
;	data -> R16,R17
	RCALL _menu_get_item_value
	MOVW R16,R30
	LDI  R30,LOW(254)
	ST   -Y,R30
	RCALL _menu_enter_setect_state
; 000E 005D     menu_enter_data(data);
	MOVW R30,R16
	CLR  R22
	CLR  R23
	RCALL __PUTPARD1
	RCALL _menu_enter_data
; 000E 005E }
	RJMP _0x20C0007
;
;
;
;
;
;void child_21_check(void)
; 000E 0065 {
_child_21_check:
; 000E 0066     uint32_t data = get_data(ADDR_PARAM_VALVE_TIME_ON);
; 000E 0067     menu_enter_setect_state(SHOW_CHOOSEN_VALUE);
	SBIW R28,4
;	data -> Y+0
	LDI  R30,LOW(21)
	LDI  R31,HIGH(21)
	RCALL SUBOPT_0x39
	RCALL SUBOPT_0x50
	RJMP _0x20C000C
; 000E 0068     menu_enter_data(data);
; 000E 0069 }
;
;void child_22_check(void)
; 000E 006C {
_child_22_check:
; 000E 006D     uint32_t data = get_data(ADDR_PARAM_VALVE_TIME_OFF);
; 000E 006E     menu_enter_setect_state(SHOW_CHOOSEN_VALUE);
	SBIW R28,4
;	data -> Y+0
	LDI  R30,LOW(22)
	LDI  R31,HIGH(22)
	RCALL SUBOPT_0x39
	RCALL SUBOPT_0x50
	RJMP _0x20C000C
; 000E 006F     menu_enter_data(data);
; 000E 0070 }
;
;void  child_21x_select(void)
; 000E 0073 {
_child_21x_select:
; 000E 0074     uint32_t value = menu_get_item_value();
; 000E 0075     set_data(ADDR_PARAM_VALVE_TIME_ON,value);
	RCALL SUBOPT_0x5C
;	value -> Y+0
	LDI  R30,LOW(21)
	LDI  R31,HIGH(21)
	RCALL SUBOPT_0x5D
	RCALL _set_data
; 000E 0076     value = data(ADDR_PARAM_VALVE_TIME_ON);
	LDI  R30,LOW(21)
	LDI  R31,HIGH(21)
	RCALL SUBOPT_0x5E
; 000E 0077     menu_enter_data(value);
	RJMP _0x20C000B
; 000E 0078 }
;
;void child_22x_select(void)
; 000E 007B {
_child_22x_select:
; 000E 007C     uint32_t value = menu_get_item_value();
; 000E 007D     set_data(ADDR_PARAM_VALVE_TIME_OFF,value);
	RCALL SUBOPT_0x5C
;	value -> Y+0
	LDI  R30,LOW(22)
	LDI  R31,HIGH(22)
	RCALL SUBOPT_0x5D
	RCALL _set_data
; 000E 007E     value = data(ADDR_PARAM_VALVE_TIME_OFF);
	LDI  R30,LOW(22)
	LDI  R31,HIGH(22)
	RCALL SUBOPT_0x5E
; 000E 007F     menu_enter_data(value);
	RJMP _0x20C000B
; 000E 0080 }
;
;
;void  child_3x_select(void)
; 000E 0084 {
_child_3x_select:
; 000E 0085     uint32_t value = menu_get_item_value();
; 000E 0086     set_data(ADDR_PARAM_VOLT_SYS_THRS,value);
	RCALL SUBOPT_0x5C
;	value -> Y+0
	LDI  R30,LOW(49)
	LDI  R31,HIGH(49)
	RCALL SUBOPT_0x5D
	RCALL _set_data
; 000E 0087     value = data(ADDR_PARAM_VOLT_SYS_THRS);
	LDI  R30,LOW(49)
	LDI  R31,HIGH(49)
	RCALL SUBOPT_0x5E
; 000E 0088     menu_enter_data(value);
	RJMP _0x20C000B
; 000E 0089 }
;
;void  child_4x_select(void)
; 000E 008C {
_child_4x_select:
; 000E 008D     uint32_t value = menu_get_item_value();
; 000E 008E     set_data(ADDR_PARAM_COMPR_CONTROL,value);
	RCALL SUBOPT_0x5C
;	value -> Y+0
	LDI  R30,LOW(11)
	LDI  R31,HIGH(11)
	RCALL SUBOPT_0x5D
	RCALL _set_data
; 000E 008F     value = data(ADDR_PARAM_COMPR_CONTROL);
	LDI  R30,LOW(11)
	LDI  R31,HIGH(11)
	RCALL SUBOPT_0x5E
; 000E 0090     menu_enter_data(value);
	RJMP _0x20C000B
; 000E 0091 }
;
;void  child_5x_select(void)
; 000E 0094 {
_child_5x_select:
; 000E 0095     uint32_t value = menu_get_item_value();
; 000E 0096     set_data(ADDR_PARAM_CONTROL_DOOR,value);
	RCALL SUBOPT_0x5C
;	value -> Y+0
	LDI  R30,LOW(51)
	LDI  R31,HIGH(51)
	RCALL SUBOPT_0x5D
	RCALL _set_data
; 000E 0097     value = data(ADDR_PARAM_CONTROL_DOOR);
	LDI  R30,LOW(51)
	LDI  R31,HIGH(51)
	RCALL SUBOPT_0x5E
; 000E 0098     menu_enter_data(value);
	RJMP _0x20C000B
; 000E 0099 }
;
;void  child_6x_select(void)
; 000E 009C {
_child_6x_select:
; 000E 009D 
; 000E 009E     uint32_t value = menu_get_item_value();
; 000E 009F     set_data(ADDR_PARAM_STOP_SIG_CONTR,value);
	RCALL SUBOPT_0x5C
;	value -> Y+0
	LDI  R30,LOW(53)
	LDI  R31,HIGH(53)
	RCALL SUBOPT_0x5D
	RCALL _set_data
; 000E 00A0     value = data(ADDR_PARAM_STOP_SIG_CONTR);
	LDI  R30,LOW(53)
	LDI  R31,HIGH(53)
	RCALL SUBOPT_0x5E
; 000E 00A1     menu_enter_data(value);
	RJMP _0x20C000B
; 000E 00A2 }
;
;void  child_7x_select(void)
; 000E 00A5 {
_child_7x_select:
; 000E 00A6     uint32_t value = menu_get_item_value();
; 000E 00A7     set_data(ADDR_CONTROL_SAVERESET,value);
	RCALL SUBOPT_0x5C
;	value -> Y+0
	RCALL SUBOPT_0x29
	RCALL SUBOPT_0x30
	RCALL _set_data
; 000E 00A8     menu_enter_setect_state(NOT_SELECT);
	LDI  R30,LOW(0)
_0x20C000C:
	ST   -Y,R30
	RCALL _menu_enter_setect_state
; 000E 00A9     menu_enter_data(value);
_0x20C000B:
	RCALL __GETD1S0
	RCALL __PUTPARD1
	RCALL _menu_enter_data
; 000E 00AA }
_0x20C000D:
	ADIW R28,4
	RET
;#include "poll.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;
;#define POLL_LIST_SIZE sizeof(poll_list) / sizeof(poll_list[0])
;
;
;uint8_t poll_list[] = {ADDR_PARAM_VOLTAGE,
;                       ADDR_PARAM_MEASURED_IN_PERS_FL,
;                       ADDR_PARAM_MEASURED_IN_PERS_FR,
;                       ADDR_PARAM_MEASURED_IN_PERS_RL,
;                       ADDR_PARAM_MEASURED_IN_PERS_RR,
;                       ADDR_PARAM_DEPTH_MODE};

	.DSEG
;
;void polling_start(void)
; 000F 000E {

	.CSEG
_polling_start:
; 000F 000F     uint8_t i;
; 000F 0010     for(i = 0; i < POLL_LIST_SIZE; i++)
	ST   -Y,R17
;	i -> R17
	LDI  R17,LOW(0)
_0x1E0005:
	CPI  R17,6
	BRSH _0x1E0006
; 000F 0011     {
; 000F 0012         data_clear(poll_list[i]);
	RCALL SUBOPT_0x5F
	RCALL SUBOPT_0x7
	RCALL _data_clear
; 000F 0013         proto_request(CMD_TYPE_READ,poll_list[i],0);
	LDI  R30,LOW(3)
	ST   -Y,R30
	RCALL SUBOPT_0x5F
	RCALL SUBOPT_0x41
; 000F 0014     }
	SUBI R17,-1
	RJMP _0x1E0005
_0x1E0006:
; 000F 0015 }
	RJMP _0x20C0008
;
;bool poll_is_finished(void)
; 000F 0018 {
_poll_is_finished:
; 000F 0019     return data_is_download(poll_list[POLL_LIST_SIZE - 1]);
	__GETB1MN _poll_list,5
	RCALL SUBOPT_0xB
	RCALL SUBOPT_0x7
	RCALL _data_is_download
	RET
; 000F 001A }
;#include "view.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;#include "addr_map.h"
;#include "ctrl.h"
;
;
;flash char communication_error_frame[] =   {"_COMMUNICATION______ERROR_______"};
;flash char main_template_frame[] =         {"FLxxxUxx.xvFRxxxRLxxx[xxxx]RRxxx"};
;flash char error_sensor_frame[] =          {"sensor error                    "};
;flash char calibrate_save_error_frame[] =  {"calibrate or      save error    "};
;flash char low_power_frame[] =             {"battery low                     "};
;flash char compressor_timeout_frame[] =    {"compressor          timeout     "};
;flash char clearance_set_timeout_frame[] = {"clearance set       timeout     "};
;
;void make_main_frame(char *frame)
; 0010 000F {

	.CSEG
_make_main_frame:
; 0010 0010     uint32_t param;
; 0010 0011 
; 0010 0012     memcpyf(frame,&main_template_frame,LCD_SIZE);
	SBIW R28,4
;	*frame -> Y+4
;	param -> Y+0
	RCALL SUBOPT_0x11
	RCALL SUBOPT_0x7
	LDI  R30,LOW(_main_template_frame*2)
	LDI  R31,HIGH(_main_template_frame*2)
	RCALL SUBOPT_0x7
	RCALL SUBOPT_0x1F
	RCALL _memcpyf
; 0010 0013 
; 0010 0014     param = data(ADDR_PARAM_VOLTAGE);
	LDI  R30,LOW(47)
	LDI  R31,HIGH(47)
	RCALL SUBOPT_0x5E
; 0010 0015     convert_voltage_data_to_str(param,&frame[6]);
	RCALL SUBOPT_0x60
	ADIW R30,6
	RCALL SUBOPT_0x7
	RCALL _convert_voltage_data_to_str
; 0010 0016 
; 0010 0017     param = data(ADDR_PARAM_MEASURED_IN_PERS_FL);
	LDI  R30,LOW(16)
	LDI  R31,HIGH(16)
	RCALL SUBOPT_0x5E
; 0010 0018     convert_percent_to_str(param,&frame[2]);
	RCALL SUBOPT_0x60
	ADIW R30,2
	RCALL SUBOPT_0x61
; 0010 0019 
; 0010 001A     param = data(ADDR_PARAM_MEASURED_IN_PERS_FR);
	LDI  R30,LOW(17)
	LDI  R31,HIGH(17)
	RCALL SUBOPT_0x5E
; 0010 001B     convert_percent_to_str(param,&frame[13]);
	RCALL SUBOPT_0x60
	ADIW R30,13
	RCALL SUBOPT_0x61
; 0010 001C 
; 0010 001D     param = data(ADDR_PARAM_MEASURED_IN_PERS_RL);
	LDI  R30,LOW(19)
	LDI  R31,HIGH(19)
	RCALL SUBOPT_0x5E
; 0010 001E     convert_percent_to_str(param,&frame[18]);
	RCALL SUBOPT_0x60
	ADIW R30,18
	RCALL SUBOPT_0x61
; 0010 001F 
; 0010 0020     param = data(ADDR_PARAM_MEASURED_IN_PERS_RR);
	LDI  R30,LOW(18)
	LDI  R31,HIGH(18)
	RCALL SUBOPT_0x5E
; 0010 0021     convert_percent_to_str(param,&frame[29]);
	RCALL SUBOPT_0x60
	ADIW R30,29
	RCALL SUBOPT_0x61
; 0010 0022 
; 0010 0023     param = data(ADDR_PARAM_DEPTH_MODE);
	RCALL SUBOPT_0x62
	RCALL SUBOPT_0x5E
; 0010 0024     convert_depth_mode_to_str(param,&frame[22]);
	RCALL SUBOPT_0x60
	ADIW R30,22
	RCALL SUBOPT_0x7
	RCALL _convert_depth_mode_to_str
; 0010 0025 
; 0010 0026 }
	RJMP _0x20C0002
;
;void show_message_box(char *message)
; 0010 0029 {
_show_message_box:
; 0010 002A     uint8_t i;
; 0010 002B     lcd_show(message);
	ST   -Y,R17
;	*message -> Y+1
;	i -> R17
	LDD  R30,Y+1
	LDD  R31,Y+1+1
	RCALL SUBOPT_0x7
	RCALL _lcd_show
; 0010 002C     for(i = 0; i < 5; i++)
	LDI  R17,LOW(0)
_0x200004:
	CPI  R17,5
	BRSH _0x200005
; 0010 002D     {
; 0010 002E         lcd_backlight(off);
	LDI  R30,LOW(1)
	ST   -Y,R30
	RCALL SUBOPT_0x63
; 0010 002F         delay_ms(200);
; 0010 0030         lcd_backlight(on);
	RCALL SUBOPT_0x1A
	RCALL SUBOPT_0x63
; 0010 0031         delay_ms(200);
; 0010 0032     }
	SUBI R17,-1
	RJMP _0x200004
_0x200005:
; 0010 0033 }
_0x20C000A:
	LDD  R17,Y+0
	ADIW R28,3
	RET
;
;void view_main(void)
; 0010 0036 {
_view_main:
; 0010 0037     char frame[LCD_SIZE];
; 0010 0038     uint16_t time;
; 0010 0039     button_t btn;
; 0010 003A     mode_type_t mode;
; 0010 003B     lcd_backlight(on);
	SBIW R28,32
	RCALL __SAVELOCR4
;	frame -> Y+4
;	time -> R16,R17
;	btn -> R19
;	mode -> R18
	RCALL SUBOPT_0x1A
	RCALL _lcd_backlight
; 0010 003C     polling_start();
	RCALL _polling_start
; 0010 003D     time = tim_get_tic();
	RCALL SUBOPT_0x3C
; 0010 003E     make_main_frame(frame);
	RCALL SUBOPT_0x64
	RCALL _make_main_frame
; 0010 003F     lcd_show(frame);
	RCALL SUBOPT_0x64
	RCALL _lcd_show
; 0010 0040 
; 0010 0041     while (!poll_is_finished)
_0x200006:
	LDI  R30,LOW(_poll_is_finished)
	LDI  R31,HIGH(_poll_is_finished)
	SBIW R30,0
	BRNE _0x200008
; 0010 0042     {
; 0010 0043         if(tim_get_time_mc(time) > POLLING_TIME)
	RCALL SUBOPT_0x3D
	__CPD1N 0x5DD
	BRLO _0x200009
; 0010 0044         {
; 0010 0045             ctrl_restart();
	RCALL _ctrl_restart
; 0010 0046             return;
	RJMP _0x20C0009
; 0010 0047         }
; 0010 0048     }
_0x200009:
	RJMP _0x200006
_0x200008:
; 0010 0049     if(button_is_pushed())
	RCALL _button_is_pushed
	CPI  R30,0
	BREQ _0x20000A
; 0010 004A     {
; 0010 004B         btn = button_last_push();
	RCALL _button_last_push
	MOV  R19,R30
; 0010 004C         mode = clearance_get_mode();
	RCALL _clearance_get_mode
	MOV  R18,R30
; 0010 004D 
; 0010 004E         switch (btn)
	MOV  R30,R19
; 0010 004F         {
; 0010 0050             case btn_long_menu:
	CPI  R30,LOW(0x5)
	BRNE _0x20000E
; 0010 0051                 menu_init();
	RCALL _menu_init
; 0010 0052                 break;
	RJMP _0x20000D
; 0010 0053             case btn_menu:
_0x20000E:
	CPI  R30,LOW(0x4)
	BRNE _0x20000F
; 0010 0054                 clearance_switch_auto_manual();
	RCALL _clearance_switch_auto_manual
; 0010 0055                 break;
	RJMP _0x20000D
; 0010 0056             case btn_right:
_0x20000F:
	CPI  R30,LOW(0x3)
	BRNE _0x200010
; 0010 0057                 if(mode == auto_mode)
	CPI  R18,1
	BRNE _0x200011
; 0010 0058                     clearance_set_or_manual_up();
	RCALL _clearance_set_or_manual_up
; 0010 0059                 else if(mode == manual_mode)
	RJMP _0x200012
_0x200011:
	CPI  R18,0
	BRNE _0x200013
; 0010 005A                     clearance_switch_manual_param();
	RCALL _clearance_switch_manual_param
; 0010 005B                 break;
_0x200013:
_0x200012:
	RJMP _0x20000D
; 0010 005C             case btn_up:
_0x200010:
	CPI  R30,LOW(0x1)
	BRNE _0x200014
; 0010 005D                 if(mode == auto_mode)
	CPI  R18,1
	BRNE _0x200015
; 0010 005E                     clearance_auto_up();
	RCALL _clearance_auto_up
; 0010 005F                 else if(mode == manual_mode)
	RJMP _0x200016
_0x200015:
	CPI  R18,0
	BRNE _0x200017
; 0010 0060                     clearance_set_or_manual_up();
	RCALL _clearance_set_or_manual_up
; 0010 0061                 break;
_0x200017:
_0x200016:
	RJMP _0x20000D
; 0010 0062             case btn_down:
_0x200014:
	CPI  R30,LOW(0x2)
	BRNE _0x20001C
; 0010 0063                 if(mode == auto_mode)
	CPI  R18,1
	BRNE _0x200019
; 0010 0064                     clearance_auto_down();
	RCALL _clearance_auto_down
; 0010 0065                 else if(mode == manual_mode)
	RJMP _0x20001A
_0x200019:
	CPI  R18,0
	BRNE _0x20001B
; 0010 0066                     clearance_manual_down();
	RCALL _clearance_manual_down
; 0010 0067                 break;
_0x20001B:
_0x20001A:
; 0010 0068             default:
_0x20001C:
; 0010 0069                 break;
; 0010 006A         }
_0x20000D:
; 0010 006B         button_clear_info();
	RCALL _button_clear_info
; 0010 006C     }
; 0010 006D }
_0x20000A:
_0x20C0009:
	RCALL __LOADLOCR4
	ADIW R28,36
	RET
;
;void view_error(uint32_t error_code)
; 0010 0070 {
_view_error:
; 0010 0071     uint8_t i;
; 0010 0072     uint8_t frame[LCD_SIZE];
; 0010 0073     flash char * error_frame;
; 0010 0074     switch (error_code)
	SBIW R28,32
	RCALL __SAVELOCR4
;	error_code -> Y+36
;	i -> R17
;	frame -> Y+4
;	*error_frame -> R18,R19
	__GETD1S 36
; 0010 0075     {
; 0010 0076         case VALUE_STATUS_ERROR_SHORT_CUR_SENS : error_frame = error_sensor_frame; break;
	RCALL SUBOPT_0x58
	BRNE _0x200020
	LDI  R30,LOW(_error_sensor_frame*2)
	LDI  R31,HIGH(_error_sensor_frame*2)
	MOVW R18,R30
	RJMP _0x20001F
; 0010 0077         case VALUE_STATUS_ERROR_COMMUNICATION  : error_frame = communication_error_frame; break;
_0x200020:
	RCALL SUBOPT_0x5A
	BRNE _0x200021
	LDI  R30,LOW(_communication_error_frame*2)
	LDI  R31,HIGH(_communication_error_frame*2)
	MOVW R18,R30
	RJMP _0x20001F
; 0010 0078         case VALUE_STATUS_ERROR_CALIBRATE      : error_frame = calibrate_save_error_frame; break;
_0x200021:
	__CPD1N 0x4
	BRNE _0x200022
	LDI  R30,LOW(_calibrate_save_error_frame*2)
	LDI  R31,HIGH(_calibrate_save_error_frame*2)
	MOVW R18,R30
	RJMP _0x20001F
; 0010 0079         case VALUE_STATUS_ERROR_SAVE_USER_DATA : error_frame = calibrate_save_error_frame; break;
_0x200022:
	__CPD1N 0x40
	BRNE _0x200023
	LDI  R30,LOW(_calibrate_save_error_frame*2)
	LDI  R31,HIGH(_calibrate_save_error_frame*2)
	MOVW R18,R30
	RJMP _0x20001F
; 0010 007A         case VALUE_STATUS_ERROR_LOW_POWER      : error_frame = low_power_frame; break;
_0x200023:
	__CPD1N 0x8
	BRNE _0x200024
	LDI  R30,LOW(_low_power_frame*2)
	LDI  R31,HIGH(_low_power_frame*2)
	MOVW R18,R30
	RJMP _0x20001F
; 0010 007B         case VALUE_STATUS_COMPRESSOR_TIMEOUT   : error_frame = compressor_timeout_frame; break;
_0x200024:
	__CPD1N 0x10
	BRNE _0x200025
	LDI  R30,LOW(_compressor_timeout_frame*2)
	LDI  R31,HIGH(_compressor_timeout_frame*2)
	MOVW R18,R30
	RJMP _0x20001F
; 0010 007C         case VALUE_STATUS_CLEARANCE_SET_TIMEOUT: error_frame = clearance_set_timeout_frame; break;
_0x200025:
	__CPD1N 0x20
	BRNE _0x200027
	LDI  R30,LOW(_clearance_set_timeout_frame*2)
	LDI  R31,HIGH(_clearance_set_timeout_frame*2)
	MOVW R18,R30
; 0010 007D         default:
_0x200027:
; 0010 007E             break;
; 0010 007F     }
_0x20001F:
; 0010 0080     memcpyf(frame,error_frame,LCD_SIZE);
	RCALL SUBOPT_0x64
	ST   -Y,R19
	ST   -Y,R18
	RCALL SUBOPT_0x1F
	RCALL _memcpyf
; 0010 0081     show_message_box(frame);
	RCALL SUBOPT_0x64
	RCALL _show_message_box
; 0010 0082 }
	RCALL __LOADLOCR4
	ADIW R28,40
	RET
;
;void view_menu(void)
; 0010 0085 {
_view_menu:
; 0010 0086     button_t btn;
; 0010 0087     char *frame;
; 0010 0088     flash char *message;
; 0010 0089     char message_frame[LCD_SIZE];
; 0010 008A     lcd_backlight(on);
	SBIW R28,32
	RCALL __SAVELOCR6
;	btn -> R17
;	*frame -> R18,R19
;	*message -> R20,R21
;	message_frame -> Y+6
	RCALL SUBOPT_0x1A
	RCALL _lcd_backlight
; 0010 008B     if(button_is_pushed())
	RCALL _button_is_pushed
	CPI  R30,0
	BREQ _0x200028
; 0010 008C     {
; 0010 008D         btn = button_last_push();
	RCALL _button_last_push
	MOV  R17,R30
; 0010 008E         menu_navigate(btn);
	ST   -Y,R17
	RCALL _menu_navigate
; 0010 008F         button_clear_info();
	RCALL _button_clear_info
; 0010 0090     }
; 0010 0091     menu_update();
_0x200028:
	RCALL _menu_update
; 0010 0092     frame = menu_get_frame();
	RCALL _menu_get_frame
	MOVW R18,R30
; 0010 0093     lcd_show(frame);
	ST   -Y,R19
	ST   -Y,R18
	RCALL _lcd_show
; 0010 0094     if(menu_get_message_box() != NULL )
	RCALL _menu_get_message_box
	SBIW R30,0
	BREQ _0x200029
; 0010 0095     {
; 0010 0096         message = menu_get_message_box();
	RCALL _menu_get_message_box
	MOVW R20,R30
; 0010 0097         memcpyf(message_frame,message,LCD_SIZE);
	MOVW R30,R28
	ADIW R30,6
	RCALL SUBOPT_0x7
	ST   -Y,R21
	ST   -Y,R20
	RCALL SUBOPT_0x1F
	RCALL _memcpyf
; 0010 0098         show_message_box(message_frame);
	MOVW R30,R28
	ADIW R30,6
	RCALL SUBOPT_0x7
	RCALL _show_message_box
; 0010 0099         menu_clear_message_box();
	RCALL _menu_clear_message_box
; 0010 009A     }
; 0010 009B     delay_ms(300);
_0x200029:
	LDI  R30,LOW(300)
	LDI  R31,HIGH(300)
	RCALL SUBOPT_0x40
; 0010 009C }
	RCALL __LOADLOCR6
	ADIW R28,38
	RET
;
;
;
;#include "clearance.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;
;
;
;mode_type_t clearance_get_mode(void)
; 0011 0006 {

	.CSEG
_clearance_get_mode:
; 0011 0007     uint8_t set = data(ADDR_PARAM_DEPTH_MODE);
; 0011 0008     if(set == VALUE_DEPTH_MED || set == VALUE_DEPTH_MIN || set == VALUE_DEPTH_MAX)
	ST   -Y,R17
;	set -> R17
	RCALL SUBOPT_0x65
	RCALL _data
	MOV  R17,R30
	CPI  R17,1
	BREQ _0x220004
	CPI  R17,0
	BREQ _0x220004
	CPI  R17,2
	BRNE _0x220003
_0x220004:
; 0011 0009         return auto_mode;
	LDI  R30,LOW(1)
	RJMP _0x20C0008
; 0011 000A     if(set == VALUE_DEPTH_MALL || set == VALUE_DEPTH_MF || set == VALUE_DEPTH_MR)
_0x220003:
	CPI  R17,5
	BREQ _0x220007
	CPI  R17,3
	BREQ _0x220007
	CPI  R17,4
	BRNE _0x220006
_0x220007:
; 0011 000B         return  manual_mode;
	LDI  R30,LOW(0)
; 0011 000C }
_0x220006:
_0x20C0008:
	LD   R17,Y+
	RET
;
;void clearance_switch_auto_manual(void)
; 0011 000F {
_clearance_switch_auto_manual:
; 0011 0010     uint8_t mode;
; 0011 0011     mode_type_t set = clearance_get_mode();
; 0011 0012     if(set == auto_mode)
	RCALL __SAVELOCR2
;	mode -> R17
;	set -> R16
	RCALL _clearance_get_mode
	MOV  R16,R30
	CPI  R16,1
	BRNE _0x220009
; 0011 0013         mode = VALUE_DEPTH_MALL;
	LDI  R17,LOW(5)
; 0011 0014     if(set == manual_mode)
_0x220009:
	CPI  R16,0
	BRNE _0x22000A
; 0011 0015         mode = VALUE_DEPTH_MED;
	LDI  R17,LOW(1)
; 0011 0016     set_data(ADDR_PARAM_DEPTH_MODE,mode);
_0x22000A:
	RCALL SUBOPT_0x65
	MOV  R30,R17
	RCALL SUBOPT_0x22
	RCALL SUBOPT_0x57
; 0011 0017 }
_0x20C0007:
	RCALL __LOADLOCR2P
	RET
;
;void clearance_auto_up(void)
; 0011 001A {
_clearance_auto_up:
; 0011 001B     uint32_t mode, set;
; 0011 001C     set = data(ADDR_PARAM_DEPTH_MODE);
	SBIW R28,8
;	mode -> Y+4
;	set -> Y+0
	RCALL SUBOPT_0x65
	RCALL _data
	RCALL SUBOPT_0x34
; 0011 001D     if(set == VALUE_DEPTH_MAX)
	RCALL SUBOPT_0x4C
	BRNE _0x22000B
; 0011 001E         return;
	RJMP _0x20C0005
; 0011 001F     if(set == VALUE_DEPTH_MIN)
_0x22000B:
	RCALL SUBOPT_0x35
	RCALL __CPD10
	BRNE _0x22000C
; 0011 0020         mode = VALUE_DEPTH_MED;
	RCALL SUBOPT_0x52
	RJMP _0x220018
; 0011 0021     else if(set == VALUE_DEPTH_MED)
_0x22000C:
	RCALL SUBOPT_0x66
	BRNE _0x22000E
; 0011 0022         mode = VALUE_DEPTH_MAX;
	__GETD1N 0x2
_0x220018:
	__PUTD1S 4
; 0011 0023     set_data(ADDR_PARAM_DEPTH_MODE,mode);
_0x22000E:
	RCALL SUBOPT_0x65
	RCALL SUBOPT_0x54
; 0011 0024 }
	RJMP _0x20C0005
;
;void clearance_auto_down(void)
; 0011 0027 {
_clearance_auto_down:
; 0011 0028     uint32_t mode, set;
; 0011 0029     set = data(ADDR_PARAM_DEPTH_MODE);
	SBIW R28,8
;	mode -> Y+4
;	set -> Y+0
	RCALL SUBOPT_0x62
	RCALL SUBOPT_0x5E
; 0011 002A     if(set == VALUE_DEPTH_MIN)
	RCALL SUBOPT_0x35
	RCALL __CPD10
	BRNE _0x22000F
; 0011 002B         return;
	RJMP _0x20C0005
; 0011 002C     if(set == VALUE_DEPTH_MED)
_0x22000F:
	RCALL SUBOPT_0x66
	BRNE _0x220010
; 0011 002D         mode = VALUE_DEPTH_MIN;
	RCALL SUBOPT_0x51
; 0011 002E     else if(set == VALUE_DEPTH_MAX)
	RJMP _0x220011
_0x220010:
	RCALL SUBOPT_0x67
	RCALL SUBOPT_0x4C
	BRNE _0x220012
; 0011 002F         mode = VALUE_DEPTH_MED;
	RCALL SUBOPT_0x52
	RCALL SUBOPT_0x53
; 0011 0030     set_data(ADDR_PARAM_DEPTH_MODE,mode);
_0x220012:
_0x220011:
	RCALL SUBOPT_0x65
	RCALL SUBOPT_0x54
; 0011 0031 }
	RJMP _0x20C0005
;
;void clearance_switch_manual_param(void)
; 0011 0034 {
_clearance_switch_manual_param:
; 0011 0035     uint32_t mode, set;
; 0011 0036     set = data(ADDR_PARAM_DEPTH_MODE);
	SBIW R28,8
;	mode -> Y+4
;	set -> Y+0
	RCALL SUBOPT_0x65
	RCALL _data
	RCALL SUBOPT_0x34
; 0011 0037     if(set == VALUE_DEPTH_MALL)
	RCALL SUBOPT_0x4D
	BRNE _0x220013
; 0011 0038         mode = VALUE_DEPTH_MF;
	RCALL SUBOPT_0x5B
	RJMP _0x220019
; 0011 0039     else if(set == VALUE_DEPTH_MF)
_0x220013:
	RCALL SUBOPT_0x67
	RCALL SUBOPT_0x4F
	BRNE _0x220015
; 0011 003A         mode = VALUE_DEPTH_MR;
	__GETD1N 0x4
	RJMP _0x220019
; 0011 003B     else if(set == VALUE_DEPTH_MR)
_0x220015:
	RCALL SUBOPT_0x67
	RCALL SUBOPT_0x4E
	BRNE _0x220017
; 0011 003C         mode = VALUE_DEPTH_MALL;
	__GETD1N 0x5
_0x220019:
	__PUTD1S 4
; 0011 003D     set_data(ADDR_PARAM_DEPTH_MODE,mode);
_0x220017:
	RCALL SUBOPT_0x65
	RCALL SUBOPT_0x54
; 0011 003E }
	RJMP _0x20C0005
;
;void clearance_set_or_manual_up(void)
; 0011 0041 {
_clearance_set_or_manual_up:
; 0011 0042     set_data(ADDR_CONTROL_DEPTH,VALUE_CTRL_SET_OR_UP);
	LDI  R30,LOW(24)
	LDI  R31,HIGH(24)
	RCALL SUBOPT_0x7
	RCALL SUBOPT_0x52
	RJMP _0x20C0006
; 0011 0043 }
;void clearance_manual_down(void)
; 0011 0045 {
_clearance_manual_down:
; 0011 0046     set_data(ADDR_CONTROL_DEPTH,VALUE_CTRL_DOWN);
	LDI  R30,LOW(24)
	LDI  R31,HIGH(24)
	RCALL SUBOPT_0x59
_0x20C0006:
	RCALL __PUTPARD1
	RCALL _set_data
; 0011 0047 }
	RET
;
;#include "status.h"
;#include "stdbool.h"
;#include "data_cmd.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif
;#include "stdint.h"
;
;uint32_t error_list[] = {
;        VALUE_STATUS_ERROR_SHORT_CUR_SENS,
;        VALUE_STATUS_ERROR_COMMUNICATION,
;        VALUE_STATUS_ERROR_CALIBRATE,
;        VALUE_STATUS_ERROR_SAVE_USER_DATA,
;        VALUE_STATUS_ERROR_LOW_POWER,
;        VALUE_STATUS_COMPRESSOR_TIMEOUT,
;        VALUE_STATUS_CLEARANCE_SET_TIMEOUT,
;};

	.DSEG
;
;#define LIST_ERROR_SIZE  sizeof(error_list)/sizeof(error_list[0])
;
;uint8_t  error_counter = 0;
;
;uint32_t status_get_next_error(void)
; 0012 0015 {

	.CSEG
_status_get_next_error:
; 0012 0016 
; 0012 0017     uint32_t status = get_data(ADDR_PARAM_STATUS);
; 0012 0018     uint32_t checking_error = error_list[error_counter];;
; 0012 0019     error_counter++;
	SBIW R28,8
;	status -> Y+4
;	checking_error -> Y+0
	LDI  R30,LOW(55)
	LDI  R31,HIGH(55)
	RCALL SUBOPT_0x39
	RCALL SUBOPT_0x53
	MOV  R30,R13
	LDI  R26,LOW(_error_list)
	LDI  R27,HIGH(_error_list)
	RCALL SUBOPT_0xB
	RCALL __LSLW2
	ADD  R26,R30
	ADC  R27,R31
	RCALL __GETD1P
	RCALL SUBOPT_0x56
	INC  R13
; 0012 001A     if((status & checking_error) > 0)
	RCALL SUBOPT_0x35
	__GETD2S 4
	RCALL __ANDD12
	MOVW R26,R30
	MOVW R24,R22
	RCALL __CPD02
	BRSH _0x240004
; 0012 001B          return checking_error;
	RCALL SUBOPT_0x35
	RJMP _0x20C0005
; 0012 001C 
; 0012 001D     if(error_counter == LIST_ERROR_SIZE)
_0x240004:
	LDI  R30,LOW(7)
	CP   R30,R13
	BRNE _0x240005
; 0012 001E         error_counter = 0;
	CLR  R13
; 0012 001F 
; 0012 0020     return 0;
_0x240005:
	RCALL SUBOPT_0x43
_0x20C0005:
	ADIW R28,8
	RET
; 0012 0021 }
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif

	.DSEG

	.CSEG
__lcd_write_nibble_G100:
	LD   R30,Y
	ANDI R30,LOW(0x10)
	BREQ _0x2000004
	SBI  0x15,3
	RJMP _0x2000005
_0x2000004:
	CBI  0x15,3
_0x2000005:
	LD   R30,Y
	ANDI R30,LOW(0x20)
	BREQ _0x2000006
	SBI  0x15,2
	RJMP _0x2000007
_0x2000006:
	CBI  0x15,2
_0x2000007:
	LD   R30,Y
	ANDI R30,LOW(0x40)
	BREQ _0x2000008
	SBI  0x15,0
	RJMP _0x2000009
_0x2000008:
	CBI  0x15,0
_0x2000009:
	LD   R30,Y
	ANDI R30,LOW(0x80)
	BREQ _0x200000A
	SBI  0x15,1
	RJMP _0x200000B
_0x200000A:
	CBI  0x15,1
_0x200000B:
	__DELAY_USB 11
	SBI  0x15,4
	__DELAY_USB 27
	CBI  0x15,4
	__DELAY_USB 27
	RJMP _0x20C0003
__lcd_write_data:
	LD   R30,Y
	RCALL SUBOPT_0x68
    ld    r30,y
    swap  r30
    st    y,r30
	LD   R30,Y
	RCALL SUBOPT_0x68
	__DELAY_USW 200
	RJMP _0x20C0003
_lcd_gotoxy:
	LD   R30,Y
	RCALL SUBOPT_0xB
	SUBI R30,LOW(-__base_y_G100)
	SBCI R31,HIGH(-__base_y_G100)
	LD   R30,Z
	LDD  R26,Y+1
	ADD  R30,R26
	RCALL SUBOPT_0x69
	LDD  R30,Y+1
	STS  __lcd_x,R30
	LD   R30,Y
	STS  __lcd_y,R30
_0x20C0004:
	ADIW R28,2
	RET
_lcd_clear:
	LDI  R30,LOW(2)
	RCALL SUBOPT_0x69
	LDI  R30,LOW(3)
	LDI  R31,HIGH(3)
	RCALL SUBOPT_0x40
	LDI  R30,LOW(12)
	RCALL SUBOPT_0x69
	LDI  R30,LOW(1)
	RCALL SUBOPT_0x69
	LDI  R30,LOW(3)
	LDI  R31,HIGH(3)
	RCALL SUBOPT_0x40
	LDI  R30,LOW(0)
	STS  __lcd_y,R30
	STS  __lcd_x,R30
	RET
_lcd_putchar:
	LD   R26,Y
	CPI  R26,LOW(0xA)
	BREQ _0x2000011
	LDS  R30,__lcd_maxx
	LDS  R26,__lcd_x
	CP   R26,R30
	BRLO _0x2000010
_0x2000011:
	RCALL SUBOPT_0x1A
	LDS  R30,__lcd_y
	SUBI R30,-LOW(1)
	STS  __lcd_y,R30
	ST   -Y,R30
	RCALL _lcd_gotoxy
	LD   R26,Y
	CPI  R26,LOW(0xA)
	BREQ _0x20C0003
_0x2000010:
	LDS  R30,__lcd_x
	SUBI R30,-LOW(1)
	STS  __lcd_x,R30
	SBI  0x15,5
	LD   R30,Y
	RCALL SUBOPT_0x69
	CBI  0x15,5
	RJMP _0x20C0003
_lcd_init:
	SBI  0x14,3
	SBI  0x14,2
	SBI  0x14,0
	SBI  0x14,1
	SBI  0x14,4
	SBI  0x14,5
	SBI  0x17,5
	CBI  0x15,4
	CBI  0x15,5
	CBI  0x18,5
	LD   R30,Y
	STS  __lcd_maxx,R30
	SUBI R30,-LOW(128)
	__PUTB1MN __base_y_G100,2
	LD   R30,Y
	SUBI R30,-LOW(192)
	__PUTB1MN __base_y_G100,3
	LDI  R30,LOW(20)
	LDI  R31,HIGH(20)
	RCALL SUBOPT_0x40
	LDI  R30,LOW(48)
	RCALL SUBOPT_0x68
	RCALL SUBOPT_0x6A
	RCALL SUBOPT_0x6A
	RCALL SUBOPT_0x6B
	LDI  R30,LOW(32)
	RCALL SUBOPT_0x68
	RCALL SUBOPT_0x6B
	LDI  R30,LOW(40)
	RCALL SUBOPT_0x69
	LDI  R30,LOW(4)
	RCALL SUBOPT_0x69
	LDI  R30,LOW(133)
	RCALL SUBOPT_0x69
	LDI  R30,LOW(6)
	RCALL SUBOPT_0x69
	RCALL _lcd_clear
_0x20C0003:
	ADIW R28,1
	RET
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x80
	.EQU __sm_mask=0x70
	.EQU __sm_powerdown=0x20
	.EQU __sm_powersave=0x30
	.EQU __sm_standby=0x60
	.EQU __sm_ext_standby=0x70
	.EQU __sm_adc_noise_red=0x10
	.SET power_ctrl_reg=mcucr
	#endif

	.CSEG

	.CSEG
_memcpy:
    ldd  r25,y+1
    ld   r24,y
    adiw r24,0
    breq memcpy1
    ldd  r27,y+5
    ldd  r26,y+4
    ldd  r31,y+3
    ldd  r30,y+2
memcpy0:
    ld   r22,z+
    st   x+,r22
    sbiw r24,1
    brne memcpy0
memcpy1:
    ldd  r31,y+5
    ldd  r30,y+4
	RJMP _0x20C0001
_memcpyf:
    ldd  r25,y+1
    ld   r24,y
    adiw r24,0
    breq memcpyf1
    ldd  r27,y+5
    ldd  r26,y+4
    ldd  r31,y+3
    ldd  r30,y+2
memcpyf0:
	lpm  r0,z+
    st   x+,r0
    sbiw r24,1
    brne memcpyf0
memcpyf1:
    ldd  r31,y+5
    ldd  r30,y+4
_0x20C0001:
_0x20C0002:
	ADIW R28,6
	RET
_memset:
    ldd  r27,y+1
    ld   r26,y
    adiw r26,0
    breq memset1
    ldd  r31,y+4
    ldd  r30,y+3
    ldd  r22,y+2
memset0:
    st   z+,r22
    sbiw r26,1
    brne memset0
memset1:
    ldd  r30,y+3
    ldd  r31,y+4
	ADIW R28,5
	RET

	.CSEG

	.DSEG

	.CSEG

	.CSEG

	.CSEG

	.DSEG
_data_pool:
	.BYTE 0x118
_menu:
	.BYTE 0x29
_proto:
	.BYTE 0x83
_tx:
	.BYTE 0xB
_rx:
	.BYTE 0xB
_poll_list:
	.BYTE 0x6
_error_list:
	.BYTE 0x1C
__base_y_G100:
	.BYTE 0x4
__lcd_x:
	.BYTE 0x1
__lcd_y:
	.BYTE 0x1
__lcd_maxx:
	.BYTE 0x1
__seed_G103:
	.BYTE 0x4

	.CSEG
;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:46 WORDS
SUBOPT_0x0:
	ST   -Y,R0
	ST   -Y,R1
	ST   -Y,R15
	ST   -Y,R22
	ST   -Y,R23
	ST   -Y,R24
	ST   -Y,R25
	ST   -Y,R26
	ST   -Y,R27
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x1:
	LDI  R30,LOW(0)
	ST   X,R30
	LDD  R26,Y+7
	LDD  R27,Y+7+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 9 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x2:
	LD   R26,Y
	LDD  R27,Y+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x3:
	ADIW R26,1
	LD   R30,X
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 10 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x4:
	LD   R30,Y
	LDD  R31,Y+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x5:
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 15 TIMES, CODE SIZE REDUCTION:12 WORDS
SUBOPT_0x6:
	LDD  R30,Y+2
	LDD  R31,Y+2+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 145 TIMES, CODE SIZE REDUCTION:142 WORDS
SUBOPT_0x7:
	ST   -Y,R31
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x8:
	RCALL SUBOPT_0x6
	ADIW R30,5
	MOVW R22,R30
	LD   R0,Z
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	ADIW R26,3
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x9:
	LD   R30,X
	ADD  R30,R0
	MOVW R26,R22
	ST   X,R30
	RJMP SUBOPT_0x6

;OPTIMIZER ADDED SUBROUTINE, CALLED 9 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0xA:
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 13 TIMES, CODE SIZE REDUCTION:22 WORDS
SUBOPT_0xB:
	LDI  R31,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0xC:
	RCALL SUBOPT_0x6
	RJMP SUBOPT_0x7

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0xD:
	LDD  R26,Y+6
	LDD  R27,Y+6+1
	ADIW R26,3
	LD   R30,X
	RCALL SUBOPT_0xB
	RCALL SUBOPT_0x7
	RJMP _memcpy

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0xE:
	ADIW R30,1
	MOVW R22,R30
	LD   R0,Z
	RJMP SUBOPT_0xA

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0xF:
	ADIW R26,2
	LD   R30,X
	MOV  R26,R0
	RCALL __MODB21U
	MOVW R26,R22
	ST   X,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x10:
	RCALL SUBOPT_0x4
	RJMP SUBOPT_0x7

;OPTIMIZER ADDED SUBROUTINE, CALLED 8 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x11:
	LDD  R30,Y+4
	LDD  R31,Y+4+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x12:
	LDD  R26,Y+4
	LDD  R27,Y+4+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:13 WORDS
SUBOPT_0x13:
	CLR  R22
	CLR  R23
	RCALL __PUTPARD1
	RJMP _tim_get_time_mc

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x14:
	CLR  R30
	ADD  R26,R17
	ADC  R27,R30
	LD   R30,X
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x15:
	RCALL SUBOPT_0x2
	LDI  R30,LOW(5)
	RCALL __MULB1W2U
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x16:
	__ADDW1MN _data_pool,4
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x17:
	MOVW R26,R30
	LD   R30,X
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x18:
	RCALL SUBOPT_0x12
	LDI  R30,LOW(5)
	RCALL __MULB1W2U
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:7 WORDS
SUBOPT_0x19:
	SUBI R30,LOW(-_data_pool)
	SBCI R31,HIGH(-_data_pool)
	RCALL __GETD2S0
	RCALL __PUTDZ20
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 8 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x1A:
	LDI  R30,LOW(0)
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x1B:
	LDD  R26,Y+1
	LDD  R27,Y+1+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x1C:
	__PUTW1MN _menu,3
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x1D:
	LDI  R30,LOW(0)
	__PUTB1MN _menu,5
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 18 TIMES, CODE SIZE REDUCTION:49 WORDS
SUBOPT_0x1E:
	__GETW1MN _menu,3
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x1F:
	LDI  R30,LOW(32)
	LDI  R31,HIGH(32)
	RJMP SUBOPT_0x7

;OPTIMIZER ADDED SUBROUTINE, CALLED 9 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x20:
	__GETB2MN _menu,5
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x21:
	ADIW R30,15
	RCALL __GETW1PF
	RCALL SUBOPT_0x7
	LDI  R30,LOW(14)
	LDI  R31,HIGH(14)
	RCALL SUBOPT_0x7
	RJMP _memcpyf

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x22:
	CLR  R31
	CLR  R22
	CLR  R23
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x23:
	RCALL __GETW1PF
	SBIW R30,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x24:
	RCALL __GETW1PF
	RJMP SUBOPT_0x1C

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x25:
	__PUTW1MN _menu,38
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x26:
	__PUTB1MN _proto,128
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x27:
	RCALL SUBOPT_0x5
	__PUTW1MN _proto,129
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x28:
	LDI  R30,LOW(_proto)
	LDI  R31,HIGH(_proto)
	RJMP SUBOPT_0x7

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x29:
	LDI  R30,LOW(56)
	LDI  R31,HIGH(56)
	RJMP SUBOPT_0x7

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x2A:
	RCALL SUBOPT_0x7
	LDI  R30,LOW(7)
	ST   -Y,R30
	LDI  R30,LOW(28)
	LDI  R31,HIGH(28)
	RCALL SUBOPT_0x7
	RJMP _buffer_init

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x2B:
	__POINTW1MN _proto,8
	RJMP SUBOPT_0x7

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x2C:
	MOVW R30,R28
	ADIW R30,2
	RJMP SUBOPT_0x7

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:9 WORDS
SUBOPT_0x2D:
	RCALL SUBOPT_0x11
	LDD  R26,Z+1
	LDD  R27,Z+2
	ST   -Y,R27
	ST   -Y,R26
	LDD  R30,Y+6
	LDD  R31,Y+6+1
	__GETD2Z 3
	RCALL __PUTPARD2
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x2E:
	RCALL SUBOPT_0x11
	LDD  R26,Z+1
	LDD  R27,Z+2
	ST   -Y,R27
	ST   -Y,R26
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x2F:
	RCALL _data
	RCALL __PUTD1S0
	RJMP SUBOPT_0x2E

;OPTIMIZER ADDED SUBROUTINE, CALLED 10 TIMES, CODE SIZE REDUCTION:34 WORDS
SUBOPT_0x30:
	__GETD1S 2
	RCALL __PUTPARD1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x31:
	MOVW R30,R28
	RJMP SUBOPT_0x7

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x32:
	__POINTW2MN _proto,129
	LD   R30,X+
	LD   R31,X+
	ADIW R30,1
	ST   -X,R31
	ST   -X,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x33:
	__GETW2MN _proto,129
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:19 WORDS
SUBOPT_0x34:
	RCALL __PUTD1S0
	RCALL __GETD2S0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 13 TIMES, CODE SIZE REDUCTION:34 WORDS
SUBOPT_0x35:
	RCALL __GETD1S0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x36:
	LDI  R30,LOW(9)
	LDI  R31,HIGH(9)
	RCALL SUBOPT_0x7
	RJMP _memset

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x37:
	LDI  R30,LOW(_rx)
	LDI  R31,HIGH(_rx)
	RJMP SUBOPT_0x7

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x38:
	__PUTB1MN _rx,10
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 9 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x39:
	RCALL SUBOPT_0x7
	RJMP _get_data

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:16 WORDS
SUBOPT_0x3A:
	__CPD2N 0x1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x3B:
	SBIW R28,4
	LDI  R30,LOW(0)
	ST   Y,R30
	STD  Y+1,R30
	STD  Y+2,R30
	STD  Y+3,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x3C:
	RCALL _tim_get_tic
	MOVW R16,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x3D:
	MOVW R30,R16
	RJMP SUBOPT_0x13

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x3E:
	__PUTD1S 2
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x3F:
	__GETD1S 2
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x40:
	RCALL SUBOPT_0x7
	RJMP _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x41:
	RCALL SUBOPT_0x7
	__GETD1N 0x0
	RCALL __PUTPARD1
	RJMP _proto_request

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x42:
	__CPD1N 0x190
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x43:
	__GETD1N 0x0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x44:
	LDD  R30,Y+6
	LDD  R31,Y+6+1
	RJMP SUBOPT_0x7

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x45:
	__GETD2S 3
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x46:
	RCALL SUBOPT_0x1B
	LD   R30,X
	LDI  R26,LOW(100)
	MUL  R30,R26
	MOVW R30,R0
	RCALL SUBOPT_0x45
	RJMP SUBOPT_0x22

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x47:
	MUL  R30,R26
	MOVW R30,R0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 8 TIMES, CODE SIZE REDUCTION:19 WORDS
SUBOPT_0x48:
	__GETD2S 2
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x49:
	LDI  R30,LOW(4)
	LDI  R31,HIGH(4)
	RCALL SUBOPT_0x7
	RJMP _memcpyf

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x4A:
	LDD  R26,Y+6
	LDD  R27,Y+6+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x4B:
	RCALL SUBOPT_0x4A
	LD   R30,X
	LDI  R26,LOW(100)
	RCALL SUBOPT_0x47
	MOV  R26,R30
	MOV  R30,R17
	SUB  R30,R26
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x4C:
	__CPD2N 0x2
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x4D:
	__CPD2N 0x5
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x4E:
	__CPD2N 0x4
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x4F:
	__CPD2N 0x3
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:18 WORDS
SUBOPT_0x50:
	RCALL __PUTD1S0
	LDI  R30,LOW(253)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x51:
	LDI  R30,LOW(0)
	__CLRD1S 4
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x52:
	__GETD1N 0x1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x53:
	__PUTD1S 4
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:13 WORDS
SUBOPT_0x54:
	__GETD1S 6
	RCALL __PUTPARD1
	RJMP _set_data

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x55:
	LDI  R30,LOW(25)
	LDI  R31,HIGH(25)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 22 TIMES, CODE SIZE REDUCTION:61 WORDS
SUBOPT_0x56:
	RCALL __PUTD1S0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x57:
	RCALL __PUTPARD1
	RJMP _set_data

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x58:
	__CPD1N 0x1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x59:
	RCALL SUBOPT_0x7
	__GETD1N 0x2
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x5A:
	__CPD1N 0x2
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x5B:
	__GETD1N 0x3
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x5C:
	SBIW R28,4
	RCALL _menu_get_item_value
	RJMP SUBOPT_0x56

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x5D:
	RCALL SUBOPT_0x7
	RJMP SUBOPT_0x30

;OPTIMIZER ADDED SUBROUTINE, CALLED 13 TIMES, CODE SIZE REDUCTION:22 WORDS
SUBOPT_0x5E:
	RCALL SUBOPT_0x7
	RCALL _data
	RJMP SUBOPT_0x56

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x5F:
	MOV  R30,R17
	RCALL SUBOPT_0xB
	SUBI R30,LOW(-_poll_list)
	SBCI R31,HIGH(-_poll_list)
	LD   R30,Z
	RJMP SUBOPT_0xB

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:13 WORDS
SUBOPT_0x60:
	RCALL SUBOPT_0x35
	RCALL __PUTPARD1
	LDD  R30,Y+8
	LDD  R31,Y+8+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x61:
	RCALL SUBOPT_0x7
	RJMP _convert_percent_to_str

;OPTIMIZER ADDED SUBROUTINE, CALLED 9 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x62:
	LDI  R30,LOW(23)
	LDI  R31,HIGH(23)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x63:
	RCALL _lcd_backlight
	LDI  R30,LOW(200)
	LDI  R31,HIGH(200)
	RJMP SUBOPT_0x40

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x64:
	MOVW R30,R28
	ADIW R30,4
	RJMP SUBOPT_0x7

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x65:
	RCALL SUBOPT_0x62
	RJMP SUBOPT_0x7

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x66:
	RCALL __GETD2S0
	RJMP SUBOPT_0x3A

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x67:
	RCALL __GETD2S0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x68:
	ST   -Y,R30
	RJMP __lcd_write_nibble_G100

;OPTIMIZER ADDED SUBROUTINE, CALLED 9 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x69:
	ST   -Y,R30
	RJMP __lcd_write_data

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x6A:
	__DELAY_USW 400
	LDI  R30,LOW(48)
	RJMP SUBOPT_0x68

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x6B:
	__DELAY_USW 400
	RET


	.CSEG
_delay_ms:
	ld   r30,y+
	ld   r31,y+
	adiw r30,0
	breq __delay_ms1
__delay_ms0:
	__DELAY_USW 0xFA0
	wdr
	sbiw r30,1
	brne __delay_ms0
__delay_ms1:
	ret

__ROUND_REPACK:
	TST  R21
	BRPL __REPACK
	CPI  R21,0x80
	BRNE __ROUND_REPACK0
	SBRS R30,0
	RJMP __REPACK
__ROUND_REPACK0:
	ADIW R30,1
	ADC  R22,R25
	ADC  R23,R25
	BRVS __REPACK1

__REPACK:
	LDI  R21,0x80
	EOR  R21,R23
	BRNE __REPACK0
	PUSH R21
	RJMP __ZERORES
__REPACK0:
	CPI  R21,0xFF
	BREQ __REPACK1
	LSL  R22
	LSL  R0
	ROR  R21
	ROR  R22
	MOV  R23,R21
	RET
__REPACK1:
	PUSH R21
	TST  R0
	BRMI __REPACK2
	RJMP __MAXRES
__REPACK2:
	RJMP __MINRES

__UNPACK:
	LDI  R21,0x80
	MOV  R1,R25
	AND  R1,R21
	LSL  R24
	ROL  R25
	EOR  R25,R21
	LSL  R21
	ROR  R24

__UNPACK1:
	LDI  R21,0x80
	MOV  R0,R23
	AND  R0,R21
	LSL  R22
	ROL  R23
	EOR  R23,R21
	LSL  R21
	ROR  R22
	RET

__CFD1U:
	SET
	RJMP __CFD1U0
__CFD1:
	CLT
__CFD1U0:
	PUSH R21
	RCALL __UNPACK1
	CPI  R23,0x80
	BRLO __CFD10
	CPI  R23,0xFF
	BRCC __CFD10
	RJMP __ZERORES
__CFD10:
	LDI  R21,22
	SUB  R21,R23
	BRPL __CFD11
	NEG  R21
	CPI  R21,8
	BRTC __CFD19
	CPI  R21,9
__CFD19:
	BRLO __CFD17
	SER  R30
	SER  R31
	SER  R22
	LDI  R23,0x7F
	BLD  R23,7
	RJMP __CFD15
__CFD17:
	CLR  R23
	TST  R21
	BREQ __CFD15
__CFD18:
	LSL  R30
	ROL  R31
	ROL  R22
	ROL  R23
	DEC  R21
	BRNE __CFD18
	RJMP __CFD15
__CFD11:
	CLR  R23
__CFD12:
	CPI  R21,8
	BRLO __CFD13
	MOV  R30,R31
	MOV  R31,R22
	MOV  R22,R23
	SUBI R21,8
	RJMP __CFD12
__CFD13:
	TST  R21
	BREQ __CFD15
__CFD14:
	LSR  R23
	ROR  R22
	ROR  R31
	ROR  R30
	DEC  R21
	BRNE __CFD14
__CFD15:
	TST  R0
	BRPL __CFD16
	RCALL __ANEGD1
__CFD16:
	POP  R21
	RET

__CDF1U:
	SET
	RJMP __CDF1U0
__CDF1:
	CLT
__CDF1U0:
	SBIW R30,0
	SBCI R22,0
	SBCI R23,0
	BREQ __CDF10
	CLR  R0
	BRTS __CDF11
	TST  R23
	BRPL __CDF11
	COM  R0
	RCALL __ANEGD1
__CDF11:
	MOV  R1,R23
	LDI  R23,30
	TST  R1
__CDF12:
	BRMI __CDF13
	DEC  R23
	LSL  R30
	ROL  R31
	ROL  R22
	ROL  R1
	RJMP __CDF12
__CDF13:
	MOV  R30,R31
	MOV  R31,R22
	MOV  R22,R1
	PUSH R21
	RCALL __REPACK
	POP  R21
__CDF10:
	RET

__ZERORES:
	CLR  R30
	CLR  R31
	CLR  R22
	CLR  R23
	POP  R21
	RET

__MINRES:
	SER  R30
	SER  R31
	LDI  R22,0x7F
	SER  R23
	POP  R21
	RET

__MAXRES:
	SER  R30
	SER  R31
	LDI  R22,0x7F
	LDI  R23,0x7F
	POP  R21
	RET

__MULF12:
	PUSH R21
	RCALL __UNPACK
	CPI  R23,0x80
	BREQ __ZERORES
	CPI  R25,0x80
	BREQ __ZERORES
	EOR  R0,R1
	SEC
	ADC  R23,R25
	BRVC __MULF124
	BRLT __ZERORES
__MULF125:
	TST  R0
	BRMI __MINRES
	RJMP __MAXRES
__MULF124:
	PUSH R0
	PUSH R17
	PUSH R18
	PUSH R19
	PUSH R20
	CLR  R17
	CLR  R18
	CLR  R25
	MUL  R22,R24
	MOVW R20,R0
	MUL  R24,R31
	MOV  R19,R0
	ADD  R20,R1
	ADC  R21,R25
	MUL  R22,R27
	ADD  R19,R0
	ADC  R20,R1
	ADC  R21,R25
	MUL  R24,R30
	RCALL __MULF126
	MUL  R27,R31
	RCALL __MULF126
	MUL  R22,R26
	RCALL __MULF126
	MUL  R27,R30
	RCALL __MULF127
	MUL  R26,R31
	RCALL __MULF127
	MUL  R26,R30
	ADD  R17,R1
	ADC  R18,R25
	ADC  R19,R25
	ADC  R20,R25
	ADC  R21,R25
	MOV  R30,R19
	MOV  R31,R20
	MOV  R22,R21
	MOV  R21,R18
	POP  R20
	POP  R19
	POP  R18
	POP  R17
	POP  R0
	TST  R22
	BRMI __MULF122
	LSL  R21
	ROL  R30
	ROL  R31
	ROL  R22
	RJMP __MULF123
__MULF122:
	INC  R23
	BRVS __MULF125
__MULF123:
	RCALL __ROUND_REPACK
	POP  R21
	RET

__MULF127:
	ADD  R17,R0
	ADC  R18,R1
	ADC  R19,R25
	RJMP __MULF128
__MULF126:
	ADD  R18,R0
	ADC  R19,R1
__MULF128:
	ADC  R20,R25
	ADC  R21,R25
	RET

__DIVF21:
	PUSH R21
	RCALL __UNPACK
	CPI  R23,0x80
	BRNE __DIVF210
	TST  R1
__DIVF211:
	BRPL __DIVF219
	RJMP __MINRES
__DIVF219:
	RJMP __MAXRES
__DIVF210:
	CPI  R25,0x80
	BRNE __DIVF218
__DIVF217:
	RJMP __ZERORES
__DIVF218:
	EOR  R0,R1
	SEC
	SBC  R25,R23
	BRVC __DIVF216
	BRLT __DIVF217
	TST  R0
	RJMP __DIVF211
__DIVF216:
	MOV  R23,R25
	PUSH R17
	PUSH R18
	PUSH R19
	PUSH R20
	CLR  R1
	CLR  R17
	CLR  R18
	CLR  R19
	CLR  R20
	CLR  R21
	LDI  R25,32
__DIVF212:
	CP   R26,R30
	CPC  R27,R31
	CPC  R24,R22
	CPC  R20,R17
	BRLO __DIVF213
	SUB  R26,R30
	SBC  R27,R31
	SBC  R24,R22
	SBC  R20,R17
	SEC
	RJMP __DIVF214
__DIVF213:
	CLC
__DIVF214:
	ROL  R21
	ROL  R18
	ROL  R19
	ROL  R1
	ROL  R26
	ROL  R27
	ROL  R24
	ROL  R20
	DEC  R25
	BRNE __DIVF212
	MOVW R30,R18
	MOV  R22,R1
	POP  R20
	POP  R19
	POP  R18
	POP  R17
	TST  R22
	BRMI __DIVF215
	LSL  R21
	ROL  R30
	ROL  R31
	ROL  R22
	DEC  R23
	BRVS __DIVF217
__DIVF215:
	RCALL __ROUND_REPACK
	POP  R21
	RET

__CMPF12:
	TST  R25
	BRMI __CMPF120
	TST  R23
	BRMI __CMPF121
	CP   R25,R23
	BRLO __CMPF122
	BRNE __CMPF121
	CP   R26,R30
	CPC  R27,R31
	CPC  R24,R22
	BRLO __CMPF122
	BREQ __CMPF123
__CMPF121:
	CLZ
	CLC
	RET
__CMPF122:
	CLZ
	SEC
	RET
__CMPF123:
	SEZ
	CLC
	RET
__CMPF120:
	TST  R23
	BRPL __CMPF122
	CP   R25,R23
	BRLO __CMPF121
	BRNE __CMPF122
	CP   R30,R26
	CPC  R31,R27
	CPC  R22,R24
	BRLO __CMPF122
	BREQ __CMPF123
	RJMP __CMPF121

__SUBD12:
	SUB  R30,R26
	SBC  R31,R27
	SBC  R22,R24
	SBC  R23,R25
	RET

__SUBD21:
	SUB  R26,R30
	SBC  R27,R31
	SBC  R24,R22
	SBC  R25,R23
	RET

__ANDD12:
	AND  R30,R26
	AND  R31,R27
	AND  R22,R24
	AND  R23,R25
	RET

__ANEGD1:
	COM  R31
	COM  R22
	COM  R23
	NEG  R30
	SBCI R31,-1
	SBCI R22,-1
	SBCI R23,-1
	RET

__LSLW2:
	LSL  R30
	ROL  R31
	LSL  R30
	ROL  R31
	RET

__EQB12:
	CP   R30,R26
	LDI  R30,1
	BREQ __EQB12T
	CLR  R30
__EQB12T:
	RET

__MULB1W2U:
	MOV  R22,R30
	MUL  R22,R26
	MOVW R30,R0
	MUL  R22,R27
	ADD  R31,R0
	RET

__DIVB21U:
	CLR  R0
	LDI  R25,8
__DIVB21U1:
	LSL  R26
	ROL  R0
	SUB  R0,R30
	BRCC __DIVB21U2
	ADD  R0,R30
	RJMP __DIVB21U3
__DIVB21U2:
	SBR  R26,1
__DIVB21U3:
	DEC  R25
	BRNE __DIVB21U1
	MOV  R30,R26
	MOV  R26,R0
	RET

__DIVD21U:
	PUSH R19
	PUSH R20
	PUSH R21
	CLR  R0
	CLR  R1
	CLR  R20
	CLR  R21
	LDI  R19,32
__DIVD21U1:
	LSL  R26
	ROL  R27
	ROL  R24
	ROL  R25
	ROL  R0
	ROL  R1
	ROL  R20
	ROL  R21
	SUB  R0,R30
	SBC  R1,R31
	SBC  R20,R22
	SBC  R21,R23
	BRCC __DIVD21U2
	ADD  R0,R30
	ADC  R1,R31
	ADC  R20,R22
	ADC  R21,R23
	RJMP __DIVD21U3
__DIVD21U2:
	SBR  R26,1
__DIVD21U3:
	DEC  R19
	BRNE __DIVD21U1
	MOVW R30,R26
	MOVW R22,R24
	MOVW R26,R0
	MOVW R24,R20
	POP  R21
	POP  R20
	POP  R19
	RET

__MODB21U:
	RCALL __DIVB21U
	MOV  R30,R26
	RET

__GETD1P:
	LD   R30,X+
	LD   R31,X+
	LD   R22,X+
	LD   R23,X
	SBIW R26,3
	RET

__GETW1PF:
	LPM  R0,Z+
	LPM  R31,Z
	MOV  R30,R0
	RET

__GETD1S0:
	LD   R30,Y
	LDD  R31,Y+1
	LDD  R22,Y+2
	LDD  R23,Y+3
	RET

__GETD2S0:
	LD   R26,Y
	LDD  R27,Y+1
	LDD  R24,Y+2
	LDD  R25,Y+3
	RET

__PUTD1S0:
	ST   Y,R30
	STD  Y+1,R31
	STD  Y+2,R22
	STD  Y+3,R23
	RET

__PUTDZ20:
	ST   Z,R26
	STD  Z+1,R27
	STD  Z+2,R24
	STD  Z+3,R25
	RET

__PUTPARD1:
	ST   -Y,R23
	ST   -Y,R22
	ST   -Y,R31
	ST   -Y,R30
	RET

__PUTPARD2:
	ST   -Y,R25
	ST   -Y,R24
	ST   -Y,R27
	ST   -Y,R26
	RET

__SWAPD12:
	MOV  R1,R24
	MOV  R24,R22
	MOV  R22,R1
	MOV  R1,R25
	MOV  R25,R23
	MOV  R23,R1

__SWAPW12:
	MOV  R1,R27
	MOV  R27,R31
	MOV  R31,R1

__SWAPB12:
	MOV  R1,R26
	MOV  R26,R30
	MOV  R30,R1
	RET

__CPD10:
	SBIW R30,0
	SBCI R22,0
	SBCI R23,0
	RET

__CPD02:
	CLR  R0
	CP   R0,R26
	CPC  R0,R27
	CPC  R0,R24
	CPC  R0,R25
	RET

__SAVELOCR6:
	ST   -Y,R21
__SAVELOCR5:
	ST   -Y,R20
__SAVELOCR4:
	ST   -Y,R19
__SAVELOCR3:
	ST   -Y,R18
__SAVELOCR2:
	ST   -Y,R17
	ST   -Y,R16
	RET

__LOADLOCR6:
	LDD  R21,Y+5
__LOADLOCR5:
	LDD  R20,Y+4
__LOADLOCR4:
	LDD  R19,Y+3
__LOADLOCR3:
	LDD  R18,Y+2
__LOADLOCR2:
	LDD  R17,Y+1
	LD   R16,Y
	RET

__LOADLOCR2P:
	LD   R16,Y+
	LD   R17,Y+
	RET

;END OF CODE MARKER
__END_OF_CODE:
