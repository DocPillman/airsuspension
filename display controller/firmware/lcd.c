#include "lcd.h"



void lcd_show(char *frame)
{
    uint8_t i;
    lcd_clear();
    lcd_gotoxy(0,0);
    for(i = 0; i < LCD_SIZE ; i++ )
    {
        lcd_putchar(frame[i]);
    }
}


void lcd_backlight(ctrl_t ctrl)
{
    if(ctrl == on)
        LCD_BACKLIGHT_PORT |= (1 << LCD_BACKLIGHT_CTRL_PIN);
    else if (ctrl == off)
        LCD_BACKLIGHT_PORT &= ~(1 << LCD_BACKLIGHT_CTRL_PIN);

}


void lcd_off(void)
{
    lcd_clear();
    lcd_backlight(off);
}



