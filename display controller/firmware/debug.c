#include "debug.h"


void _portb3(ctrl_t cmd)
{
    if(cmd == on)
        PORTB |= (1 << PORTB3);
    else
        PORTB &= ~(1 << PORTB3);
}

void _portb4(ctrl_t cmd)
{
    if(cmd == on)
        PORTB |= (1 << PORTB4);
    else
        PORTB &= ~(1 << PORTB4);
}

void _portb5(ctrl_t cmd)
{
    if(cmd == on)
        PORTB |= (1 << PORTB5);
    else
        PORTB &= ~(1 << PORTB5);

}