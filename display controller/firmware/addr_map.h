#ifndef __ADDR_MAP_H_
#define __ADDR_MAP_H_


//------------------------------------------------------------------------------//
//-------------------ADDRESS----------------------------------------------------//
//------------------------------------------------------------------------------//


//**********************************************************************
// -valve manual control
#define  ADDR_CONTR_FL_VALVE  2   // WO+Action
#define  ADDR_CONTR_FR_VALVE  3   // WO+Action
#define  ADDR_CONTR_RL_VALVE  4   // WO+Action
#define  ADDR_CONTR_RR_VALVE  5   // WO+Action
#define  ADDR_CONTR_F_VALVE   6   // WO+Action
#define  ADDR_CONTR_R_VALVE   7   // WO+Action
#define  ADDR_CONTR_ALL_VALVE 8   // WO+Action

/* if sended 1 it mens IN  if 0 it means EX*/
#define  ACTION_VALVE_EX 0
#define  ACTION_VALVE_IN 1




//**********************************************************************
//- time in or ex in manual control mode
#define  ADDR_PARAM_TIME_MANUAL_EX_IN 9  // R/W
//in 0.05 second default 2 = 100ms



//**********************************************************************
//-compressor

#define  ADDR_PARAM_IS_COMPR_EMPTY 10  // RO
#define  ADDR_PARAM_COMPR_CONTROL 11  // R/W + save in Flash

        #define VALUE_COMP_OFF 0x00 //becomes OFF power too
        #define VALUE_COMP_ON 0x01
/*    TypePressureSensor
   0x01 pull up both wire , when receiver empty
   0x02 without pull up - classic curcuit when sensor conncet to plus and to
   board pin   default 0x02  */



//**********************************************************************
// - setting dumps control  SET_XX_damper

/* Target Value for set in dumps or current sets
 0-MAX_ADC  in persent !!!!!!*/

#define  ADDR_PARAM_SET_FL_DAMP 12  //R/W
#define  ADDR_PARAM_SET_FR_DAMP 13  //R/W
#define  ADDR_PARAM_SET_RR_DAMP 14  //R/W
#define  ADDR_PARAM_SET_RL_DAMP 15  //R/W

/*    ADDR_persent_mesured_XX
  Measured value of dupms in persent
  0 - 100 persent */

#define ADDR_PARAM_MEASURED_IN_PERS_FL 16 //RO
#define ADDR_PARAM_MEASURED_IN_PERS_FR 17 //RO
#define ADDR_PARAM_MEASURED_IN_PERS_RR 18 //RO
#define ADDR_PARAM_MEASURED_IN_PERS_RL 19 //RO
        #define VALUE_PERSENT_UNKOWN 0xFFFFFFFF


#define ADDR_PARAM_VALVE_TIME_ON 21 //R/W + save in Flash
        #define  VALUE_VAL_ONTIME0 0x00
        #define  VALUE_VAL_ONTIME5 0x05
        #define  VALUE_VAL_ONTIME7 0x07
        #define  VALUE_VAL_ONTIME10 0x0A
        #define  VALUE_VAL_ONTIME12 0x0C

#define ADDR_PARAM_VALVE_TIME_OFF 22 //R/W + save in Flash
        #define  VALUE_VAL_OFFTIME0 0x00
        #define  VALUE_VAL_OFFTIME17 0x11
        #define  VALUE_VAL_OFFTIME50 0x32
        #define  VALUE_VAL_OFFTIME100 0x64

/*************************************************************************************/

#define ADDR_PARAM_DEPTH_MODE 23 //R/W
// controller start in VALUE_DEPTH_ALL mode
        #define VALUE_DEPTH_MIN 0x00
        #define VALUE_DEPTH_MED 0x01
        #define VALUE_DEPTH_MAX 0x02
        #define VALUE_DEPTH_MF 0x03
        #define VALUE_DEPTH_MR 0x04
        #define VALUE_DEPTH_MALL 0x05


#define ADDR_CONTROL_DEPTH 24 //WO
        #define VALUE_CTRL_SET_OR_UP 0x01 // its set current depth or up if man mode
        #define VALUE_CTRL_DOWN 0x02 // its down car in man mode
/*******************************************************************************************/

#define ADDR_PARAM_SAVE_DAMPS_VALUE 25 // RW + Action
        #define VALUE_DONT_SAVE_DAMPS 0
        #define VALUE_SAVE_MIN_DAMPS 1
        #define VALUE_SAVE_MED_DAMPS 2
        #define VALUE_SAVE_MAX_DAMPS 3

//it means wrote countinuasly  3 measumented value
// 1 low positition
// 2 middle or auto position
// 3 high position

/*
 Measure  value in moment calibrate
*/
#define ADDR_PARAM_MIN_SET_FL 26    //RW + save in Flash
#define ADDR_PARAM_MIN_SET_FR 27
#define ADDR_PARAM_MIN_SET_RR 28
#define ADDR_PARAM_MIN_SET_RL 29

#define ADDR_PARAM_MAX_SET_FL 30    //RW + save in Flash
#define ADDR_PARAM_MAX_SET_FR 31
#define ADDR_PARAM_MAX_SET_RR 32
#define ADDR_PARAM_MAX_SET_RL 33

#define ADDR_PARAM_MED_SET_FL 34   //RW + save in Flash
#define ADDR_PARAM_MED_SET_FR 35
#define ADDR_PARAM_MED_SET_RR 36
#define ADDR_PARAM_MED_SET_RL 37

#define ADDR_PARAM_DIRCHG_FL 38   //RO + save in Flash
#define ADDR_PARAM_DIRCHG_FR 39
#define ADDR_PARAM_DIRCHG_RR 40
#define ADDR_PARAM_DIRCHG_RL 41
        #define VALUE_DIRCHG_MAXUP_MINDOWN 1
        #define VALUE_DIRCHG_MAXDOWN_MINUP 0


#define ADDR_PARAM_MEASURE_FL 43   //RO
#define ADDR_PARAM_MEASURE_FR 44
#define ADDR_PARAM_MEASURE_RR 45
#define ADDR_PARAM_MEASURE_RL 46



//**********************************************************************
// - VOLTAGE CONTROLLER
#define ADDR_PARAM_VOLTAGE 47 // RO int to float will need

#define ADDR_PARAM_VOLT_SYS_THRS 49 // R/W  + save in Flash
        #define VALUE_TRSH_VOL_DISABLE 0
        #define VALUE_TRSH_VOL_10V 1
        #define VALUE_TRSH_VOL_11V 2
        #define VALUE_TRSH_VOL_12V 3



//*************************************************************************
// - DOOR CONTROLLER
#define ADDR_PARAM_IS_DOOR_OPEN 50 // RO   open or close

#define ADDR_PARAM_CONTROL_DOOR 51  // R/W + save in Flash
        #define  VALUE_DOOR_CONTR_OFF 0x00
        #define  VALUE_DOOR_CONTR_ON 0x01

// 0x01 if you use pullup (dip switch 1-2 ON ) using other type sensor
// when door is open signal on pin = 1
// config pin = 1
// 0x01 if you use without pullup-pulldown ( dip switch 1 OFF 2 ON )
// when door is open signal on pin = 1
// config pin  = 1
// 0x01 if use lamp in car then you need set (dip-switch 1 , 2 in OFF)
// config pin = 0
// door is open pin = 1






//*************************************************************************
//- STOP SIGNAL CONTROLLER

#define ADDR_PARAM_STOP_SIG_STATE 52 // RO      default ON if we have 12V it means do not
        #define VALUE_STOP_IS_PUSHED 1
        #define VALUE_STOP_DONOT_PUSH 0

#define ADDR_PARAM_STOP_SIG_CONTR 53 // RW + save in Flash
        #define VALUE_STOP_ENABLE 0x01
        #define VALUE_STOP_DISABLE 0x00




//*************************************************************************
// -Power

#define ADDR_PARAM_POWER_STATE 54 // RW
        #define VALUE_POWER_SLEEP 0x01
        #define VALUE_POWER_ON 0x02


//*************************************************************************
// - Status

#define ADDR_PARAM_STATUS  55  //RO + Action

        #define VALUE_STATUS_ERROR_SHORT_CUR_SENS 0x01
        #define VALUE_STATUS_ERROR_COMMUNICATION  0x02
        #define VALUE_STATUS_ERROR_CALIBRATE 0x04
        #define VALUE_STATUS_ERROR_SAVE_USER_DATA 0x40
        #define VALUE_STATUS_ERROR_LOW_POWER 0x08
        #define VALUE_STATUS_COMPRESSOR_TIMEOUT 0x10
        #define VALUE_STATUS_CLEARANCE_SET_TIMEOUT 0x20


//*************************************************************************
// - SAVE RESET

#define ADDR_CONTROL_SAVERESET 56  // WO+Action
        #define VALUE_SAVE_SETTING  0x01
        #define VALUE_RESET_SETTING 0x00
        


#endif

