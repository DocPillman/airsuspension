#include "proto.h"

proto_t proto;

void proto_init(void)
{
    proto.state = idle;
    proto.counter = 0;
    buffer_init(&proto._request_buffer,REQUEST_BUFFER_SIZE,proto.request_buffer,
                                               PAYLOAD_SIZE,REQUEST_BUFFER_SIZE/2);
    buffer_init(&proto._response_buffer,RESPONSE_BUFFER_SIZE,proto.response_buffer,
                                              PAYLOAD_SIZE,RESPONSE_BUFFER_SIZE/2);
    buffer_flash_queue(&proto._request_buffer);
    buffer_flash_queue(&proto._response_buffer);
}

void proto_responce(payload_t *payload)
{
    #asm("cli")
    buffer_push_queue(&proto._response_buffer,(uint8_t * )payload);
    #asm("sei")
}


void proto_request( uint8_t type_command, uint16_t address, uint32_t data )
{
    payload_t payload;
    payload.value = data;
    payload.address = address;
    payload.type_command = type_command;

    #asm("cli")
    buffer_push_queue(&proto._request_buffer,(uint8_t*)&payload);
    #asm("sei")
}


void proto_pesponse_parser(payload_t * payload)
{
    uint32_t value;
    switch (payload->type_command)
    {
        case CMD_TYPE_READ_OK:
            data_download_cb(payload->address,payload->value);
            break;
        case CMD_TYPE_WRITE_OK:
            data_upload_cb(payload->address,payload->value);
            break;
        case CMD_TYPE_READ_ERROR:
            value = data(payload->address);
            data_download_cb(payload->address,value);
            break;
        case CMD_TYPE_WRITE_ERROR:
            value = data(payload->address);
            data_upload_cb(payload->address,value);
            break;
        default:
            break;
    }
}


void proto_buf_worker(void)
{
    payload_t payload;
    if(!buffer_is_empty(&proto._response_buffer))
    {
        #asm("cli")
        buffer_pop_queue(&proto._response_buffer,(uint8_t * )&payload);
        #asm("sei")
        proto_pesponse_parser(&payload);
    }
    if(!(buffer_is_empty(&proto._request_buffer)) && (idle == uart_get_tx_state()) )
    {
        #asm("cli")
        buffer_pop_queue(&proto._request_buffer,(uint8_t*)&payload);
        #asm("sei")
        uart_transmit(&payload);
    }
}

void proto_tim_handler(void)
{
    if(busy == uart_get_rx_state())
    {
        proto.counter++;
        if(proto.counter >= TIME_GETTING_PACKET)
        {
            proto.state = error;
        }
    } else if(proto.state == idle)
        proto.counter = 0;

    if(error == uart_get_rx_state())
    {
        proto.state = error;
    }

    if(proto.state == error)
    {
        proto.counter ++ ;
        if(proto.counter >= TIME_PROTO_REBOOT)
        {
            uart_init();
            proto_init();
            proto.counter = 0;
            proto.state = idle;
        }
    }
    proto_buf_worker();
}


