#include "help.h"

#ifndef _DATA_CMD_INCLUDED_
#define _DATA_CMD_INCLUDED_

#include "proto.h"
#include "timer.h"

#define TIME_TRANSACTION  400

void set_data(uint16_t addr, uint32_t data);
uint32_t get_data(uint16_t addr);

#endif
