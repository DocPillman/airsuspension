#include "clearance.h"



mode_type_t clearance_get_mode(void)
{
    uint8_t set = data(ADDR_PARAM_DEPTH_MODE);
    if(set == VALUE_DEPTH_MED || set == VALUE_DEPTH_MIN || set == VALUE_DEPTH_MAX)
        return auto_mode;
    if(set == VALUE_DEPTH_MALL || set == VALUE_DEPTH_MF || set == VALUE_DEPTH_MR)
        return  manual_mode;
}

void clearance_switch_auto_manual(void)
{
    uint8_t mode;
    mode_type_t set = clearance_get_mode();
    if(set == auto_mode)
        mode = VALUE_DEPTH_MALL;
    if(set == manual_mode)
        mode = VALUE_DEPTH_MED;
    set_data(ADDR_PARAM_DEPTH_MODE,mode);
}

void clearance_auto_up(void)
{
    uint32_t mode, set;
    set = data(ADDR_PARAM_DEPTH_MODE);
    if(set == VALUE_DEPTH_MAX)
        return;
    if(set == VALUE_DEPTH_MIN)
        mode = VALUE_DEPTH_MED;
    else if(set == VALUE_DEPTH_MED)
        mode = VALUE_DEPTH_MAX;
    set_data(ADDR_PARAM_DEPTH_MODE,mode);
}

void clearance_auto_down(void)
{
    uint32_t mode, set;
    set = data(ADDR_PARAM_DEPTH_MODE);
    if(set == VALUE_DEPTH_MIN)
        return;
    if(set == VALUE_DEPTH_MED)
        mode = VALUE_DEPTH_MIN;
    else if(set == VALUE_DEPTH_MAX)
        mode = VALUE_DEPTH_MED;
    set_data(ADDR_PARAM_DEPTH_MODE,mode);
}

void clearance_switch_manual_param(void)
{
    uint32_t mode, set;
    set = data(ADDR_PARAM_DEPTH_MODE);
    if(set == VALUE_DEPTH_MALL)
        mode = VALUE_DEPTH_MF;
    else if(set == VALUE_DEPTH_MF)
        mode = VALUE_DEPTH_MR;
    else if(set == VALUE_DEPTH_MR)
        mode = VALUE_DEPTH_MALL;
    set_data(ADDR_PARAM_DEPTH_MODE,mode);
}

void clearance_set_or_manual_up(void)
{
    set_data(ADDR_CONTROL_DEPTH,VALUE_CTRL_SET_OR_UP);
}
void clearance_manual_down(void)
{
    set_data(ADDR_CONTROL_DEPTH,VALUE_CTRL_DOWN);
}

