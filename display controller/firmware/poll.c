#include "poll.h"

#define POLL_LIST_SIZE sizeof(poll_list) / sizeof(poll_list[0])


uint8_t poll_list[] = {ADDR_PARAM_VOLTAGE,
                       ADDR_PARAM_MEASURED_IN_PERS_FL,
                       ADDR_PARAM_MEASURED_IN_PERS_FR,
                       ADDR_PARAM_MEASURED_IN_PERS_RL,
                       ADDR_PARAM_MEASURED_IN_PERS_RR,
                       ADDR_PARAM_DEPTH_MODE};

void polling_start(void)
{
    uint8_t i;
    for(i = 0; i < POLL_LIST_SIZE; i++)
    {
        data_clear(poll_list[i]);
        proto_request(CMD_TYPE_READ,poll_list[i],0);
    }
}

bool poll_is_finished(void)
{
    return data_is_download(poll_list[POLL_LIST_SIZE - 1]);
}