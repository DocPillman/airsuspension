#include "timer.h"



uint16_t count;


void tim_init(void)
{

    ASSR=0x00;
    TCCR2=0x00;
    TCNT2=0x00;
    OCR2=0x00;

    TCCR0=(0<<CS02) | (1<<CS01) | (1<<CS00);
    TCNT0=0x06;

    count = 0;

    ASSR=0<<AS2;
    TCCR2=(0<<PWM2) | (0<<COM21) | (0<<COM20) | (0<<CTC2) | (1<<CS22) | (0<<CS21) | (0<<CS20);
    TCNT2=0x06;
    OCR2=0x00;

    TIMSK=(0<<OCIE2) | (1<<TOIE2) | (0<<TICIE1) | (0<<OCIE1A) | (0<<OCIE1B) | (0<<TOIE1) | (1<<TOIE0);
}

void tim_handler(void)
{
    count++;
}


uint16_t tim_get_tic(void)
{
    return count;
}

uint32_t tim_get_time_mc (uint32_t prev)
{
    uint32_t delta = tim_get_tic() - prev;
    if(delta < 0)
        delta += 0xFFFF;

    return delta;
}
