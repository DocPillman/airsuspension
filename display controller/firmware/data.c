#include "data.h"



data_t data_pool[56];
                                     
                         
uint32_t data(uint16_t addr)
{
    return data_pool[addr].value;
}

bool data_is_download(uint16_t addr)
{
    return data_pool[addr].download == 1;
}

bool data_is_upload(uint16_t addr)
{
    return data_pool[addr].upload == 1;
}

void data_clear(uint16_t addr)
{
    data_pool[addr].upload = 0;
    data_pool[addr].download = 0;
    //data_pool[addr].value = 0;
}

void data_download_cb(uint16_t addr, uint32_t val)
{
    data_pool[addr].download = true;
    data_pool[addr].value = val ;
}

void data_upload_cb( uint16_t addr, uint32_t val)
{
    data_pool[addr].upload = true;
    data_pool[addr].value = val;
}
