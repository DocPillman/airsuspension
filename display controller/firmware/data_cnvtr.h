#ifndef _DATA_CNVTR_INCLUDED_
#define _DATA_CNVTR_INCLUDED_

#include "help.h"
#include "addr_map.h"
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>

void convert_percent_to_str(uint32_t data, uint8_t *str);
void convert_voltage_data_to_str(uint32_t data,uint8_t *str);
void convert_depth_mode_to_str( uint32_t data, uint8_t *str);


#endif