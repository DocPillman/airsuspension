#include "uart.h"
#include "proto.h"




com_t tx;
com_t rx;


void uart_init(void)
{
    memset(&tx.buffer,0,PACKET_SIZE);
    memset(&rx.buffer,0,PACKET_SIZE);
    tx.counter = rx.counter = 0; 
    tx.state = rx.state = idle;
    UCSRA=(0<<RXC) | (0<<TXC) | (0<<UDRE) | (0<<FE) | (0<<DOR) | (0<<UPE) | (0<<U2X) | (0<<MPCM);
    UCSRB=(1<<RXCIE) | (0<<TXCIE) | (0<<UDRIE) | (1<<RXEN) | (1<<TXEN) | (0<<UCSZ2) | (0<<RXB8) | (0<<TXB8);
    UCSRC=(1<<URSEL) | (0<<UMSEL) | (0<<UPM1) | (0<<UPM0) | (0<<USBS) | (1<<UCSZ1) | (1<<UCSZ0) | (0<<UCPOL);
    UBRRH=0x00;
    UBRRL=0x33;

}


void uart_rx_isr_handler(void)
{
    packet_t * packet;
    rx.state = busy;
    rx.buffer[rx.counter++] = UDR;

    if(rx.counter == PACKET_SIZE)
    {
        rx.counter = 0;

        packet = (packet_t *) rx.buffer;

        if(chks_is_ok(rx.buffer) && (packet->id == SLAVE_ID))
        {
            proto_responce(&packet->payload);
            rx.state = idle;

        } else {
            if (!chks_is_ok(rx.buffer))
            {
                rx.state = error;
                UCSRB &= ~(1 << RXCIE);
            }
        }
    }
}


void uart_tx_isr_handler(void)
{
    if(tx.counter < PACKET_SIZE)
        UDR = tx.buffer[tx.counter++];
    else
        {
            tx.state = idle;
            UCSRB &= ~(1 << UDRIE);
        }
}



void uart_transmit(payload_t * payload)
{

    packet_t packet;
    if((tx.state != idle) || (payload == NULL))
        return;

    packet.id = MASTER_ID;
    memcpy(&packet.payload,payload,PAYLOAD_SIZE);
    packet.chks = chks((uint8_t*)&packet);
    memcpy(tx.buffer,&packet,PACKET_SIZE);
    tx.counter = 0;
    tx.state = busy;                             
    UCSRB |= (1 << UDRIE);

}


uint8_t uart_get_tx_state(void)
{
    return tx.state;
}

uint8_t uart_get_rx_state(void)
{
    return rx.state;
}

