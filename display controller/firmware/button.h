#ifndef _BUTTON_INCLUDED_
#define _BUTTON_INCLUDED_

#include <mega8.h>
#include "menu.h"
#include <stdio.h>
#include "timer.h"

#define  BTN_UP_GPIO PINB.0
#define  BTN_DOWN_GPIO PINB.1
#define  BTN_RIGHT_GPIO PIND.7
#define  BTN_MENU_GPIO PIND.6

#define LONG_PRESS_TIME_TIC 700

typedef enum {   
	btn_empty =0,
	btn_up,
	btn_down,
	btn_right,
	btn_menu,
    btn_long_menu
} button_t;


void button_handler(void);
void button_timer_handler (void);


bool button_is_pushed();
button_t button_last_push(void);
void button_clear_info(void);

#endif

