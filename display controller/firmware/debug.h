#ifndef  _DEBUG_INCLUDED_
#define  _DEBUG_INCLUDED_

#include <mega8.h>
#include "help.h"


void _portb3(ctrl_t cmd);
void _portb4(ctrl_t cmd);
void _portb5(ctrl_t cmd);

#endif