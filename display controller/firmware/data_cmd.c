#include "data_cmd.h"
#include "ctrl.h"


uint32_t get_data(uint16_t addr)
{
    uint16_t count = tim_get_tic();
    data_clear(addr);
    proto_request(CMD_TYPE_READ,addr,0);
    while (!data_is_download(addr))
    {
        if(tim_get_time_mc(count) >= TIME_TRANSACTION)
        {
            ctrl_restart();
            return 0 ;
        }
    }
    return  data(addr);

}


void set_data(uint16_t addr, uint32_t data)
{
    uint16_t count = tim_get_tic();
    data_clear(addr);
    proto_request(CMD_TYPE_WRITE,addr,data);
    while (!data_is_upload(addr))
    {
        if(tim_get_time_mc(count) >= TIME_TRANSACTION)
        {
           ctrl_restart();
           return  ;
        }
    }
}