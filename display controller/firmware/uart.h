#ifndef  _UART_INCLUDED_
#define  _UART_INCLUDED_

#include <mega8.h>
#include "buffer.h"
#include "pdata.h"
#include "proto.h"
#include "help.h"
#include "chks.h"

   
                          
typedef struct
{
    uint8_t buffer[PACKET_SIZE];
    uint8_t counter;
    uint8_t state;    
    
}com_t;


void uart_init(void);

void uart_rx_isr_handler(void);
void uart_tx_isr_handler(void);

void uart_transmit(payload_t * payload);

uint8_t uart_get_tx_state(void);
uint8_t uart_get_rx_state(void);

#endif