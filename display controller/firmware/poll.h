#ifndef _POLL_INCLUDED_
#define _POLL_INCLUDED_

#include "help.h"
#include "data_cmd.h"
#include "proto.h"

#define POLLING_TIME 1500  // ms


void polling_start(void);
bool poll_is_finished(void);




#endif